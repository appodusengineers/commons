//package com.appodus.commons.security;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
//
//@Configuration
//public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests()
//                .antMatchers(HttpMethod.POST, "/v1/auth/**").permitAll() // Attempting Authentication
//                .antMatchers(HttpMethod.POST, "/v1/organizations").permitAll() // Creating New Organization
//                .antMatchers(HttpMethod.POST, "/v1/users").permitAll() // Maybe, Creating a first user
//                .antMatchers(HttpMethod.GET, "/v1/roles/active").permitAll() // Maybe, Creating a first user
//                .antMatchers(
//                        "/",
//                        "/index**", "/index/**",
//                        "/home**", "/home/**",
//                        "/**/auth/login**", "/**/auth/login**/**", // Login
//                        "/**.css", "/**.js", "/**.map", "/**.eot", "/**.ttf", "/**.woff", "/**.woff2", "/**.svg", "/**.ico",
//                        "/assets/**",
//                        "/cr-websocket/**", // Allow Websocket
//                        "/cr/**" // Allow direct linking (from external links or browser refresh)
//                ).permitAll()
//                .anyRequest().authenticated();
//    }
//}