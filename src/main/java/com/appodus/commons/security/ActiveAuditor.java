package com.appodus.commons.security;

import com.appodus.commons.domain.contact.email.Email;
import com.appodus.commons.domain.contact.phone.Phone;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import com.appodus.commons.domain.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Arrays;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Component
public class ActiveAuditor implements AuditorAware<String> {
    private Organization organization;
    private User user;
    private User aquila;

    public Authentication getCurrentAuthentication() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();

//        // Create Authentication
//        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(username, jwtToken, authorities);
//        authentication.setDetails(user);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        return authentication;
    }

    public User getUser() {
        if (user != null) {
            return user;
        }

        Authentication authentication = getCurrentAuthentication();
        if (authentication != null && StringUtils.hasText((String) authentication.getPrincipal())) {
            String username = (String) authentication.getPrincipal();

            if (username.equalsIgnoreCase("anonymousUser")){
                return getRawAquila();
            }

            User user = new User();
            user.setUsername(username);

            this.user = user;
            return user;
        }


        return getRawAquila();
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isAuthenticated() {
        Authentication authentication = getCurrentAuthentication();
        if (authentication != null) {
            return authentication.isAuthenticated();
        }

        return false;
    }

    @Override
    public String getCurrentAuditor() {
        User user = getUser();
        if (user != null && StringUtils.hasText(user.getUsername())) {
            return user.getUsername();
        }

        return null;
    }

    public User getRawAquila() {
        User user = new User("aquila@" + "churchrun.com", "Password4001:s?");
        user.getProfile().setLastName("ChurchRun");
        user.getProfile().setFirstName("Aquila");
        user.getContact().setPhoneList(Arrays.asList(new Phone("234", "7039012787", "mobile")));
        user.getContact().setEmailList(Arrays.asList(new Email(user.getUsername(), "primary email")));
        user.setInternalUser(true);

        return user;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public User getAquila() {
        return aquila;
    }

    public void setAquila(User aquila) {
        this.aquila = aquila;
    }
}