package com.appodus.commons.Util;

import java.io.File;

public class AppConstants {
    public static final String FILES_DIR = "data" + File.separator;
    public static final String FILES_TEMP_DIR = FILES_DIR + "temp" + File.separator;
    public static final String FILES_PICS_DIR = FILES_DIR + "pics" + File.separator;
    public static final String FILES_PICS_PROFILE_DIR = FILES_PICS_DIR + "profile" + File.separator;

    public static String getApiVersion() {
        return "v1";
    }
}
