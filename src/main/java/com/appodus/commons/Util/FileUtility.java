package com.appodus.commons.Util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Base64;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class FileUtility {

    private static Logger logger = LoggerFactory.getLogger(FileUtility.class.getName());

    public FileUtility() {
    }


    public static String copyFile(String src, String dest) {
        return copyFile(new File(src), new File(dest));
    }

    public static String copyFile(File src, File dest) {
        try {
            FileUtils.copyFile(src, dest, true);
            return dest.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String moveFile(String src, String dest) {
        return moveFile(new File(src), new File(dest));
    }

    public static String moveFile(File src, File dest) {
        try {
            deleteIfExists(dest);
            FileUtils.moveFile(src, dest);
            return dest.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String saveProfilePics(File file, String firstName, String lastName) {

        String fullPath = file.getAbsolutePath();
        String fileExt = fullPath.substring(fullPath.lastIndexOf("."));

        String destPath = AppConstants.FILES_PICS_PROFILE_DIR +
                RandomStringUtils.randomAlphabetic(6) + "_" +
                firstName + "." + lastName +
                fileExt;

        return moveFile(file, new File(destPath));
    }

    public static String moveToTemp(File file) {
        String fullPath = file.getAbsolutePath();
        String fileName = fullPath.substring(fullPath.lastIndexOf(File.separator) + 1);
        String destPath = AppConstants.FILES_TEMP_DIR + RandomStringUtils.randomAlphabetic(6) + "_" + fileName;

        return moveFile(file, new File(destPath));
    }

    public static String toBase64(File file) {
        try {
            String ext = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf(".") + 1);
            return "data:image/" + ext + "+xml;base64," + Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static boolean deleteFile(String fileFullPath) {
        if (fileFullPath == null) {
            return false;
        }

        if (!deleteIfExists(new File(fileFullPath))) {
            logger.error(String.format("Attempted to delete file: %s, which does not exist.", fileFullPath));
            return false;
        }

        return true;
    }

    public static boolean exists(String path) {
        return new File(path).exists();
    }

    public static boolean deleteIfExists(File file) {
        if (file.exists()) {
            try {
                FileUtils.forceDelete(file);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

//        public static void main(String[] args) {
//            System.out.println(File.separator);
//    }
}
