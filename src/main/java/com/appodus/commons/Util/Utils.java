package com.appodus.commons.Util;

import com.appodus.commons.domain.contact.address.lga.data.CountryStatesLocals;
import com.appodus.commons.domain.contact.address.lga.state.country.Country;
import com.appodus.commons.domain.contact.address.lga.state.data.CountryStates;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.role.Role;
import com.appodus.commons.domain.user.role.permission.Permission;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Utils {
    public static Date dateaTimeStringToDate(String dateTimeString) throws ParseException {
        Date date = null;

        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:m:s").parse(dateTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date dateTimeStringToDate(String dateTimeString) throws ParseException {
        Date date = null;
        try {
            date = new SimpleDateFormat().parse(dateTimeString);
        } catch (ParseException e) {
            try {
                date = new SimpleDateFormat("dd/MM/yyyy").parse(dateTimeString);
            } catch (ParseException e1) {
                try {
                    date = new SimpleDateFormat("dd-MM-yyyy").parse(dateTimeString);
                } catch (ParseException e2) {

                    try {
                        date = new SimpleDateFormat("dd/MM/yy").parse(dateTimeString);
                    } catch (ParseException e3) {
                        try {
                            date = new SimpleDateFormat("yyyy/MM/dd").parse(dateTimeString);
                        } catch (ParseException e4) {
                            try {
                                date = new SimpleDateFormat("yyyy-MM-dd").parse(dateTimeString);
                            } catch (ParseException e5) {

                                try {
                                    date = new SimpleDateFormat("yy/MM/dd").parse(dateTimeString);
                                } catch (ParseException e6) {
                                    e6.printStackTrace();
                                    throw new ParseException(e6.getLocalizedMessage(), e6.getErrorOffset());
                                }
                                e5.printStackTrace();
                            }
                            e4.printStackTrace();
                        }
                        e3.printStackTrace();
                    }
                    e2.printStackTrace();
                }
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

        return date;
    }

    public static boolean dateIsBefore(Date date, Date isBefore) {
        if (date == null || isBefore == null) {
            return false;
        }

        return dateToLocalDateTime(date).isBefore(dateToLocalDateTime(isBefore));
    }


    public static boolean dateIsEqual(Date date, Date isEqual) {
        if (date == null || isEqual == null) {
            return false;
        }

        return dateToLocalDateTime(date).isEqual(dateToLocalDateTime(isEqual));
    }

    public static boolean dateIsAfter(Date date, Date isAfter) {
        if (date == null || isAfter == null) {
            return false;
        }

        return dateToLocalDateTime(date).isAfter(dateToLocalDateTime(isAfter));
    }

    public static LocalDateTime dateToLocalDateTime(Date date) {
        if (date == null) {
            return null;
        }

        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    public static String getRequestUrl(HttpServletRequest request) {
        return request.getRequestURL().toString();
    }

    public static String getAuthorizationToken(HttpServletRequest request) {
        return request.getHeader("Authorization").replace("Bearer ", "");
    }

    public static String getClientIP(HttpServletRequest request) {
        String xffHeader = request.getHeader("X-Forwarded-For");
        if (xffHeader == null) {
            return request.getRemoteAddr();
        }
        return xffHeader.split(",")[0];
    }

    public static File multipartFile2File(MultipartFile file) {
        File convFile = new File(file.getOriginalFilename());

        try {
            convFile.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(convFile);
            outputStream.write(file.getBytes());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return convFile;
    }

    public static String objToJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }


    public static Organization jsonStringToOrganization(final String str) {
        try {
            return new ObjectMapper().readValue(str, Organization.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static List<Country> jsonStringToCountryList(final String str) {
        try {
            return new ObjectMapper().readValue(str, new TypeReference<List<Country>>() {
            });
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static List<CountryStates> jsonStringToCountriesStatesList(final String str) {
        try {
            return new ObjectMapper().readValue(str, new TypeReference<List<CountryStates>>() {
            });
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static List<CountryStatesLocals> jsonStringToCountryStatesLocalsList(final String str) {
        try {
            return new ObjectMapper().readValue(str, new TypeReference<List<CountryStatesLocals>>() {
            });
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String roles2Str(List<Role> roles) {

        StringBuilder stringBuilder = new StringBuilder();

        roles.forEach((role) -> {

            if (stringBuilder.toString().length() < 1) {
                stringBuilder.append(role.getName());
            } else {
                stringBuilder.append(", " + role.getName());
            }
        });

        return stringBuilder.toString();
    }

    public static String permissions2Str(List<Permission> permissions) {
        if (permissions == null) {
            return null;
        }

        return permissions.stream()
                .map((permission) -> {
                    return permission.getName().toUpperCase();
                })
                .collect(Collectors.joining(", "));
    }

    public static String sha256(String input) {
        MessageDigest mDigest = null;
        try {
            mDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    public static String stringToBase64(String str) {
        try {
            return Base64.getEncoder().encodeToString(str.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String buildSystemName(String str){
        return str.replace(" ", "_").toLowerCase();
    }
}
