package com.appodus.commons.events;

import org.springframework.context.ApplicationEvent;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public class OnUserModifiedEvent extends ApplicationEvent {
    private EventObj eventObj;

    public OnUserModifiedEvent(EventObj eventObj, Object source) {
        super(source);
        this.eventObj = eventObj;
    }

    public EventObj getEventObj() {
        return eventObj;
    }
}