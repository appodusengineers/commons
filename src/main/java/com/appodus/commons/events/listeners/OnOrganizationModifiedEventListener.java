package com.appodus.commons.events.listeners;

import com.appodus.commons.domain.designation.DesignationService;
import com.appodus.commons.domain.miscellaneous.agecategory.AgeCategoryService;
import com.appodus.commons.domain.miscellaneous.gender.GenderService;
import com.appodus.commons.domain.miscellaneous.maritalstatus.MaritalStatusService;
import com.appodus.commons.domain.miscellaneous.marriagetype.MarriageTypeService;
import com.appodus.commons.domain.miscellaneous.membershipstatus.MembershipStatusService;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.profile.careerindustry.CareerIndustryService;
import com.appodus.commons.domain.profile.school.educationlevel.EducationLevelService;
import com.appodus.commons.domain.user.role.RoleService;
import com.appodus.commons.events.OnOrganizationModifiedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class OnOrganizationModifiedEventListener implements ApplicationListener<OnOrganizationModifiedEvent> {

    private final SimpMessagingTemplate simpMessagingTemplate;
    private final DesignationService designationService;
    private final RoleService roleService;
    private final GenderService genderService;
    private final MaritalStatusService maritalStatusService;
    private final AgeCategoryService ageCategoryService;
    private final MembershipStatusService membershipStatusService;
    private final MarriageTypeService marriageTypeService;
    private final EducationLevelService educationLevelService;
    private final CareerIndustryService careerIndustryService;

    @Autowired
    public OnOrganizationModifiedEventListener(SimpMessagingTemplate simpMessagingTemplate, DesignationService designationService, RoleService roleService, GenderService genderService, MaritalStatusService maritalStatusService, AgeCategoryService ageCategoryService, MembershipStatusService membershipStatusService, MarriageTypeService marriageTypeService, EducationLevelService educationLevelService, CareerIndustryService careerIndustryService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.designationService = designationService;
        this.roleService = roleService;
        this.genderService = genderService;
        this.maritalStatusService = maritalStatusService;
        this.ageCategoryService = ageCategoryService;
        this.membershipStatusService = membershipStatusService;
        this.marriageTypeService = marriageTypeService;
        this.educationLevelService = educationLevelService;
        this.careerIndustryService = careerIndustryService;
    }

    @Override
    public void onApplicationEvent(OnOrganizationModifiedEvent e) {

        initOrganizationEntities(e);

        // Always report last, since reporting annuls Obj
        reportEvent(e);
    }

    private void initOrganizationEntities(final OnOrganizationModifiedEvent e) {
        Organization organization = (Organization) e.getEventObj().getObject();

        designationService.initOrganizationDesignations(organization);
        roleService.initOrganizationRoles(organization);
        genderService.initOrganizationGenders(organization);
        maritalStatusService.initOrganizationMaritalStatuses(organization);
        ageCategoryService.initOrganizationAgeCategories(organization);
        membershipStatusService.initOrganizationMembershipStatuses(organization);
        marriageTypeService.initOrganizationMarriageTypes(organization);
        educationLevelService.initOrganizationEducationLevels(organization);
        careerIndustryService.initOrganizationCareerIndustries(organization);
    }

    private void reportEvent(final OnOrganizationModifiedEvent e) {
//        EventObj eventObj = e.getEventObj().annulObj(); // DON'T SEND OBJ, SET TO NULL

        simpMessagingTemplate.convertAndSend("/topic/organization/modified", e.getEventObj());
    }
}