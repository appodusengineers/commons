package com.appodus.commons.events.listeners;

import com.appodus.commons.events.EventObj;
import com.appodus.commons.events.OnDesignationModifiedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class OnDesignationModifiedEventListener implements ApplicationListener<OnDesignationModifiedEvent> {

    private final SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public OnDesignationModifiedEventListener(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    public void onApplicationEvent(OnDesignationModifiedEvent e) {
        reportEvent(e);
    }

    private void reportEvent(final OnDesignationModifiedEvent e) {
        EventObj eventObj = e.getEventObj().annulObj(); // DON'T SEND OBJ, SET TO NULL

        simpMessagingTemplate.convertAndSend("/topic/designation/modified", eventObj);
    }
}