package com.appodus.commons.events.listeners;

import com.appodus.commons.events.EventObj;
import com.appodus.commons.events.OnPermissionModifiedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class OnPermissionModifiedEventListener implements ApplicationListener<OnPermissionModifiedEvent> {

    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public OnPermissionModifiedEventListener(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    public void onApplicationEvent(OnPermissionModifiedEvent e) {
        reportEvent(e);
    }

    private void reportEvent(final OnPermissionModifiedEvent e) {
        EventObj eventObj = e.getEventObj().annulObj(); // DON'T SEND OBJ, SET TO NULL

        simpMessagingTemplate.convertAndSend("/topic/permission/modified", eventObj);
    }
}