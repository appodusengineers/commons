package com.appodus.commons.events.listeners;

import com.appodus.commons.events.EventObj;
import com.appodus.commons.events.OnUserModifiedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class OnUserModifiedEventListener implements ApplicationListener<OnUserModifiedEvent> {

    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public OnUserModifiedEventListener(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    public void onApplicationEvent(OnUserModifiedEvent e) {
        reportEvent(e);
    }

    private void reportEvent(final OnUserModifiedEvent e) {
        EventObj eventObj = e.getEventObj().annulObj(); // DON'T SEND OBJ, SET TO NULL

        simpMessagingTemplate.convertAndSend("/topic/user/modified", eventObj);
    }
}