package com.appodus.commons.events;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public class EventObj {
    private String entity;
    private String eventType;
    private Object object;

    public EventObj(String eventType, Object object) {
        this.entity = object != null ? object.getClass().getSimpleName() : "";
        this.eventType = eventType;
        this.object = object;
    }

    public String getEntity() {
        return entity;
    }

    public String getEventType() {
        return eventType;
    }

    public Object getObject() {
        return object;
    }

    public EventObj annulObj() {
        this.object = null;

        return this;
    }

    @Override
    public String toString() {
        return "EventObj{" +
                "entity='" + entity + '\'' +
                ", eventType='" + eventType + '\'' +
                '}';
    }

    public enum EventType {
        CREATE("CREATE"),
        READ("READ"),
        UPDATE("UPDATE"),
        ACTIVATE("ACTIVATE"),
        IN_ACTIVATE("IN_ACTIVATE"),
        ARCHIVE("ARCHIVE"),
        UNARCHIVE("UNARCHIVE"),
        DELETE("DELETE"),
        HARD_DELETE("HARD_DELETE"),

        MESSAGE_SENT("MESSAGE_SENT"),
        MESSAGE_DRAFTED("MESSAGE_DRAFTED"),
        MESSAGE_SCHEDULED("MESSAGE_SCHEDULED"),

        ATTENDANCE_PRESENT("ATTENDANCE_PRESENT"),
        ATTENDANCE_ABSENT("ATTENDANCE_ABSENT");

        private String type;

        EventType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }
}