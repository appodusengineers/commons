package com.appodus.commons.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author NanoSoft Solutions
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InputParseException extends RuntimeException {

    public InputParseException(String message) {
        super(message);
    }
}
