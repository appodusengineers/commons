/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appodus.commons.exception;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.dao.DataAccessException;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

/**
 *
 * @author NanoSoft Solutions
 */
@ControllerAdvice()
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private ServerResponse sResponse;

    @ExceptionHandler(value = {
            UnknownException.class,
            InputParseException.class,
//            SQLException.class,
//            LicenseParseException.class,
            NotYetImplementedException.class,
            DataAccessException.class,
            RuntimeException.class
    })
    protected ResponseEntity<ServerResponse> handleServerException(RuntimeException ex) {
        Error error = new Error(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());

        sResponse = new ServerResponse(error);
        return new ResponseEntity<>(sResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {
            EntityConflictException.class
    })
    protected ResponseEntity<ServerResponse> handleConflictException(RuntimeException ex) {
        Error error = new Error(HttpStatus.CONFLICT.value(), ex.getMessage());

        sResponse = new ServerResponse(error);
        return new ResponseEntity<>(sResponse, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = {
            UsernameNotFoundException.class
    })
    protected ResponseEntity<ServerResponse> handleNotFoundException(RuntimeException ex) {
        Error error = new Error(HttpStatus.NOT_FOUND.value(), ex.getMessage());

        sResponse = new ServerResponse(error);
        return new ResponseEntity<>(sResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {
            RequestedContentNotFoundException.class
    })
    protected ResponseEntity<ServerResponse> handleNoContentException(RuntimeException ex) {
        Error error = new Error(HttpStatus.NO_CONTENT.value(), ex.getMessage());

        sResponse = new ServerResponse(error);
        return new ResponseEntity<>(sResponse, HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(value = {
            InvalidRequestException.class,
//            PasswordLifecycleException.class,
            BadSqlGrammarException.class
    })
    protected ResponseEntity<ServerResponse> handleBadRequestException(RuntimeException ex) {
        Error error = new Error(HttpStatus.BAD_REQUEST.value(), ex.getMessage());

        sResponse = new ServerResponse(error);
        return new ResponseEntity<>(sResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {
            AccessDeniedException.class,
            InternalAuthenticationServiceException.class,
            OffWorkingHourException.class
    })
    protected ResponseEntity<ServerResponse> handleAccessDeniedException(RuntimeException ex, final HttpServletRequest request) {
        Error error = new Error(HttpStatus.UNAUTHORIZED.value(), ex.getLocalizedMessage());

        sResponse = new ServerResponse(error);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.UNAUTHORIZED);
    }



//    @ExceptionHandler(value = {
//            FailedDependencyException.class
//    })
//    protected ResponseEntity<ServerResponse> handleFailedDependencyException(FailedDependencyException ex) {
//        Error error = null;
//        if (ex.getMessage().equalsIgnoreCase(AppInitHttpStatus.NO_ACTIVE_LICENSE.getReasonPhrase())){
//            error = new Error(AppInitHttpStatus.NO_ACTIVE_LICENSE.getValue(), ex.getMessage());
//        }else if (ex.getMessage().equalsIgnoreCase(AppInitHttpStatus.MINIMUM_USER_NOT_MET.getReasonPhrase())){
//            error = new Error(AppInitHttpStatus.MINIMUM_USER_NOT_MET.getValue(), ex.getMessage());
//        }else if (ex.getMessage().equalsIgnoreCase(AppInitHttpStatus.SETTINGS_NOT_INITIALIZED.getReasonPhrase())){
//            error = new Error(AppInitHttpStatus.SETTINGS_NOT_INITIALIZED.getValue(), ex.getMessage());
//        }else {
//            error = new Error(AppInitHttpStatus.UNKNOWN_CAUSE.getValue(), ex.getMessage());
//        }
//
//        sResponse = new ServerResponse(error);
//        return new ResponseEntity<>(sResponse, HttpStatus.FAILED_DEPENDENCY);
//    }
}
