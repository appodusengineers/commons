package com.appodus.commons.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author NanoSoft Solutions
 */
@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
public class FailedDependencyException extends RuntimeException {

    public FailedDependencyException(String message) {
        super(message);
    }
}
