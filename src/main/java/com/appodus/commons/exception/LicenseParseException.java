package com.appodus.commons.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Kingsley - FinTech on 7/3/2017.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LicenseParseException extends RuntimeException {
    public LicenseParseException(String message) {
        super(message);
    }
}
