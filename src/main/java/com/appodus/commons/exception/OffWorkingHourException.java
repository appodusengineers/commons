package com.appodus.commons.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author NanoSoft Solutions
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class OffWorkingHourException extends RuntimeException {

    public OffWorkingHourException(String message) {
        super(message);
    }
}
