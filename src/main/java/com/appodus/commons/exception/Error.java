package com.appodus.commons.exception;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public class Error {

    private int status;
    private String message;

    public Error() {
    }

    public Error(int code, String message) {
        this.status = code;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
