package com.appodus.commons.domain.designation;

import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.organization.Organization;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"name", "organization_id"})
})
public class Designation extends AbstractEntity {
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String systemName;
    private String description;
    @Column(nullable = false)
    private String category;

    public Designation() {
        super("", "", "");
    }

//    public Designation(String name, String description, String category, Organization organization) {
//        this();
//
//        this.name = name;
//        this.description = description;
//        this.category = category;
//        this.organization = organization;
//    }

    public Designation(String name, String systemName, String description, String category, Organization organization) {
        this();

        this.name = name;
        this.systemName = systemName;
        this.description = description;
        this.category = category;
        this.organization = organization;
    }


    public static enum Categories {
        FAMILY("FAMILY"),
        GROUP("GROUP"),
        SALUTATION("SALUTATION");

        private String catName;

        Categories(String catName) {
            this.catName = catName;
        }

        public String getCatName() {
            return catName;
        }

        @Override
        public String toString() {
            return "Categories{" +
                    "catName='" + catName + '\'' +
                    '}';
        }
    }
}
