package com.appodus.commons.domain.designation;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.role.Role;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface DesignationDao extends AbstractDao<Designation> {

    Designation findByNameAndOrganization(String name, Organization organization);
    Designation findBySystemNameAndOrganization(String systemName, Organization organization);

    List<Designation> findAllByOrganization(Organization organization);
    List<Designation> findAllByOrganizationAndActive(Organization organization, boolean isActive);
    List<Designation> findAllByOrganizationAndDeleted(Organization organization, boolean isDeleted);
    List<Designation> findAllByOrganizationAndCategoryAndActive(Organization organization, String category, boolean isActive);

//    @Query("SELECT d FROM Designation d WHERE d.active = true ORDER BY d.createdDate DESC")
//    List<Designation> finAllActive();
//
//    @Query("SELECT d FROM Designation d WHERE d.active = false ORDER BY d.createdDate DESC")
//    List<Designation> findAllInactive();
}
