package com.appodus.commons.domain.designation;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.events.EventObj;
import com.appodus.commons.events.OnDesignationModifiedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/designations")
public class DesignationController {
    private final DesignationService designationService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public DesignationController(
            DesignationService designationService,
            ApplicationEventPublisher eventPublisher) {
        this.designationService = designationService;
        this.eventPublisher = eventPublisher;
    }
    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody Designation designation, final HttpServletRequest request) {
        System.out.println("designation: " + designation);
        Designation _designation = designationService.create(designation);

        eventPublisher.publishEvent(new OnDesignationModifiedEvent(new EventObj(EventObj.EventType.CREATE.getType(), _designation), this.getClass()));

        sResponse = new ServerResponse(_designation);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }
    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<Designation> designationList = designationService.getAll();

        sResponse = new ServerResponse(designationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{designationId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("designationId") String designationId, final HttpServletRequest request) {
        Designation designation = designationService.getById(designationId);

        sResponse = new ServerResponse(designation);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-organization")
    public ResponseEntity<ServerResponse> getAllByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Designation> designationList = designationService.getAllByOrganization(organization);

        sResponse = new ServerResponse(designationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }    @PostMapping("active/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Designation> designationList = designationService.getAllActiveByOrganization(organization);

        sResponse = new ServerResponse(designationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("inactive/by-organization")
    public ResponseEntity<ServerResponse> getAllInactiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Designation> designationList = designationService.getAllInactiveByOrganization(organization);

        sResponse = new ServerResponse(designationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Designation> designationList = designationService.getAllDeletedByOrganization(organization);

        sResponse = new ServerResponse(designationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("non-deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllNonDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Designation> designationList = designationService.getAllNonDeletedByOrganization(organization);

        sResponse = new ServerResponse(designationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("cat-family/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveCatFamilyByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Designation> designationList = designationService.getAllActiveCatFamilyByOrganization(organization);

        sResponse = new ServerResponse(designationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("cat-group/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveCatGroupByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Designation> designationList = designationService.getAllActiveCatGroupByOrganization(organization);

        sResponse = new ServerResponse(designationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("cat-salutation/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveCatSalutationByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Designation> designationList = designationService.getAllActiveCatSalutationByOrganization(organization);
        System.out.println("designationList: " + designationList);
        sResponse = new ServerResponse(designationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{designationId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("designationId") String designationId,
            @RequestBody Designation designation,
            final HttpServletRequest request) {

        Designation updatedDesignation = designationService.update(designationId, designation);

        eventPublisher.publishEvent(new OnDesignationModifiedEvent(new EventObj(EventObj.EventType.UPDATE.getType(), updatedDesignation), this.getClass()));

        sResponse = new ServerResponse(updatedDesignation);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{designationId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("designationId") String designationId,
            final HttpServletRequest request) {

        Designation updatedDesignation = designationService.activate(designationId);

        eventPublisher.publishEvent(new OnDesignationModifiedEvent(new EventObj(EventObj.EventType.ACTIVATE.getType(), updatedDesignation), this.getClass()));

        sResponse = new ServerResponse(updatedDesignation);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{designationId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("designationId") String designationId,
            final HttpServletRequest request) {

        Designation updatedDesignation = designationService.inactivate(designationId);

        eventPublisher.publishEvent(new OnDesignationModifiedEvent(new EventObj(EventObj.EventType.IN_ACTIVATE.getType(), updatedDesignation), this.getClass()));

        sResponse = new ServerResponse(updatedDesignation);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{designationId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("designationId") String designationId,
            final HttpServletRequest request) {

        Designation updatedDesignation = designationService.delete(designationId);

        eventPublisher.publishEvent(new OnDesignationModifiedEvent(new EventObj(EventObj.EventType.DELETE.getType(), updatedDesignation), this.getClass()));

        sResponse = new ServerResponse(updatedDesignation);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{designationId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("designationId") String designationId,
            final HttpServletRequest request) {

        designationService.hardDelete(designationId);

        eventPublisher.publishEvent(new OnDesignationModifiedEvent(new EventObj(EventObj.EventType.HARD_DELETE.getType(), null), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
