package com.appodus.commons.domain.designation;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class DesignationService extends AbstractService<Designation, DesignationDao> {
    private final DesignationDao designationsDao;
    private final ActiveAuditor activeAuditor;

    @Autowired
    public DesignationService(DesignationDao designationsDao, ActiveAuditor activeAuditor) {
        this.designationsDao = designationsDao;
        this.activeAuditor = activeAuditor;
    }

    public Designation create(Designation designation) {
        designation.setSystemName(designation.getName().replace(" ", "_").toLowerCase());
        return super.create(designation);
    }

    public Designation update(String designationId, Designation designation) {

        Designation _designation = getById(designationId);
        _designation.setName(designation.getName());
        _designation.setDescription(designation.getDescription());
        _designation.setCategory(designation.getCategory());

        return super.update(_designation);
    }


    public Designation getByNameAndOrganization(String name, Organization organization) {
        return designationsDao.findByNameAndOrganization(name, organization);
    }

    public Designation getBySystemNameAndOrganization(String systemName, Organization organization) {
        return designationsDao.findBySystemNameAndOrganization(systemName, organization);
    }

    /**
     * Returns List of All Designations in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Designation> getAllByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return designationsDao.findAllByOrganization(_organization);
    }


    /**
     * Returns List of All Active Designations in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Designation> getAllActiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return designationsDao.findAllByOrganizationAndActive(_organization, true);
    }


    /**
     * Returns List of All Inactive Designations in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Designation> getAllInactiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return designationsDao.findAllByOrganizationAndActive(_organization, false);
    }


    /**
     * Returns List of All Deleted Designations in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Designation> getAllDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return designationsDao.findAllByOrganizationAndDeleted(_organization, true);
    }


    /**
     * Returns List of All Non Deleted Designations in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Designation> getAllNonDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return designationsDao.findAllByOrganizationAndDeleted(_organization, false);
    }

    /**
     * Returns List of All Active Designations of Category 'FAMILY' in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Designation> getAllActiveCatFamilyByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return designationsDao.findAllByOrganizationAndCategoryAndActive(
                _organization,
                Designation.Categories.FAMILY.getCatName().toLowerCase(),
                true
        );
    }


    /**
     * Returns List of All Active Designations of Category 'GROUP' in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Designation> getAllActiveCatGroupByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return designationsDao.findAllByOrganizationAndCategoryAndActive(
                _organization,
                Designation.Categories.GROUP.getCatName().toLowerCase(),
                true
        );
    }


    /**
     * Returns List of All Active Designations of Category 'SALUTATION' in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Designation> getAllActiveCatSalutationByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return designationsDao.findAllByOrganizationAndCategoryAndActive(
                _organization,
                Designation.Categories.SALUTATION.getCatName().toLowerCase(),
                true
        );
    }

    // Necessary per org
    public void initOrganizationDesignations(Organization organization) {
        if (getAllByOrganization(organization).size() < 1) {
            super.createAll(Arrays.asList(
                    // GROUP
                    new Designation("Member", "Member", "Group member", "group", organization),
                    new Designation("Leader", "Leader", "Group leader", "group", organization),
                    new Designation("Supervisor", "Supervisor", "Group supervisor", "group", organization),
                    new Designation("Secretary", "Secretary", "Group secretary", "group", organization),

                    // SALUTATION
                    new Designation("Sis.", "Sis.", "Sister", "salutation", organization),
                    new Designation("Bro.", "Bro.", "Brother", "salutation", organization),
                    new Designation("Pastor", "Pastor", "Pastor", "salutation", organization),
                    new Designation("Mr.", "Mr.", "Mister", "salutation", organization),
                    new Designation("Mrs.", "Mrs.", "Misses", "salutation", organization),
                    new Designation("Miss.", "Miss.", "Miss", "salutation", organization),
                    new Designation("Ms.", "Ms.", "Miss", "salutation", organization),
                    new Designation("Elder", "Elder", "Elder", "salutation", organization),
                    new Designation("Dr.", "Dr.", "Doctor", "salutation", organization),
                    new Designation("Prof.", "Prof.", "Professor", "salutation", organization),
                    new Designation("Rev.", "Rev.", "Reverend", "salutation", organization),
                    new Designation("Gen.", "Gen.", "General", "salutation", organization),
                    new Designation("Col.", "Col.", "Colonel", "salutation", organization),
                    new Designation("Cpt", "Cpt", "Captain", "salutation", organization),
                    new Designation("Hon.", "Hon.", "Honorable", "salutation", organization),

                    // FAMILY
                    new Designation("Son", "Son", "male child", "family", organization),
                    new Designation("Stepson", "Stepson", "son of spouse from previous marriage", "family", organization),
                    new Designation("Son-in-law", "Son-in-law", "daughter's husband", "family", organization),
                    new Designation("Grandson", "Grandson", "child's son", "family", organization),

                    new Designation("Daughter", "Daughter", "female child", "family", organization),
                    new Designation("Stepdaughter", "Stepdaughter", "daughter of spouse from previous marriage", "family", organization),
                    new Designation("Daughter-in-law", "Daughter-in-law", "son's wife", "family", organization),
                    new Designation("Granddaughter", "Granddaughter", "child's daughter", "family", organization),

                    new Designation("Foster child", "Foster child", "A child whom you provided with care and upbringing; but not your biological child.", "family", organization),

                    new Designation("Father", "Father", "Man who is or is acting as a parent", "family", organization),
                    new Designation("Stepfather", "Stepfather", "mother's subsequent husband", "family", organization),
                    new Designation("Father-in-law", "Father-in-law", "spouse's father", "family", organization),
                    new Designation("Grandfather", "Grandfather", "parent's father", "family", organization),

                    new Designation("Mother", "Mother", "Woman who is or is acting as a parent", "family", organization),
                    new Designation("Stepmother", "Stepmother", "father's subsequent wife", "family", organization),
                    new Designation("Mother-in-law", "Mother-in-law", "spouse's mother", "family", organization),
                    new Designation("Grandmother", "Grandmother", "parent's mother", "family", organization),

                    new Designation("Brother", "Brother", "Male sibling", "family", organization),
                    new Designation("Stepbrother", "Stepbrother", "son of stepparent", "family", organization),
                    new Designation("Brother-in-law", "Brother-in-law", "sister's husband or spouse's brother or spouse's sister's husband", "family", organization),

                    new Designation("Sister", "Sister", "Female sibling", "family", organization),
                    new Designation("Stepsister", "Stepsister", "sister of stepparent", "family", organization),
                    new Designation("Sister-in-law", "Sister-in-law", "brother's wife or spouse's sister or spouse's brother's wife", "family", organization),

                    new Designation("Husband", "Husband", "woman's spouse", "family", organization),

                    new Designation("Wife", "Wife", "man's spouse", "family", organization),
                    new Designation("Stepwife", "Stepwife", "husband's ex-wife or subsequent wife", "family", organization),

                    new Designation("Uncle", "Uncle", "parent's brother or brother-in-law", "family", organization),
                    new Designation("Aunt", "Aunt", "father's or mother's sister", "family", organization),
                    new Designation("Nephew", "Nephew", "sibling's or in-law's son", "family", organization),
                    new Designation("Niece", "Niece", "daughter of sibling", "family", organization),
                    new Designation("Boyfriend", "Boyfriend", "Boyfriend", "family", organization),
                    new Designation("Girlfriend", "Girlfriend", "Girlfriend", "family", organization),
                    new Designation("Cousin", "Cousin", "uncle's or aunt's child", "family", organization),
                    new Designation("Friend", "Friend", "Friend", "family", organization),
                    new Designation("Living with", "Living_with", "Whom you live with", "family", organization),
                    new Designation("Lives with me", "Lives_with_me", "Who is living with you", "family", organization)
            ));
        }
    }

}
