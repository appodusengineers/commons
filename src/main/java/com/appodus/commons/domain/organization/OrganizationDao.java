package com.appodus.commons.domain.organization;


import com.appodus.commons.domain.abstractservice.AbstractDao;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface OrganizationDao extends AbstractDao<Organization> {
    List<Organization> findAllByOrganization(Organization organization);
}
