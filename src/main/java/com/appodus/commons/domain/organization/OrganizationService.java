package com.appodus.commons.domain.organization;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.contact.ContactService;
import com.appodus.commons.domain.contact.email.EmailService;
import com.appodus.commons.domain.contact.phone.PhoneService;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class OrganizationService extends AbstractService<Organization, OrganizationDao> {
    private final OrganizationDao organizationDao;
    private final ContactService contactService;
    private final ActiveAuditor activeAuditor;
    private final PhoneService phoneService;
    private final EmailService emailService;

    @Autowired
    public OrganizationService(OrganizationDao organizationDao, ContactService contactService, ActiveAuditor activeAuditor, PhoneService phoneService, EmailService emailService) {
        this.organizationDao = organizationDao;
        this.contactService = contactService;
        this.activeAuditor = activeAuditor;
        this.phoneService = phoneService;
        this.emailService = emailService;
    }

    @Transactional
    public Organization create(Organization organization) {

        // Constraint Validation
        // ...phoneNumber
        String ext = organization.getContact().getPhoneList().get(0).getExt();
        String phone = organization.getContact().getPhoneList().get(0).getValue();
        if (phoneService.getByExtAndValue(ext, phone) != null) {
            throw new RuntimeException(String.format("The phone, %s%s, is already in use.", ext, phone));
        }
        // ...email
        String email = organization.getContact().getEmailList().get(0).getValue();
        if (emailService.getByValue(email) != null) {
            throw new RuntimeException(String.format("The email, %s, is already in use.", email));
        }


        organization.setContact(contactService.create(organization.getContact()));

        organization.setOrgNumber(super.count() + 1000);

        return super.create(organization);
    }

    /**
     * Returns List of All Organizations in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Organization> getAllByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return organizationDao.findAllByOrganization(_organization);
    }

    public Organization update(String organizationId, Organization organization) {

        Organization _organization = getById(organizationId);
        _organization.setName(organization.getName());
        _organization.setContact(organization.getContact());

        return super.update(_organization);
    }
}
