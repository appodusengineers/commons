package com.appodus.commons.domain.organization;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.user.User;
import com.appodus.commons.events.EventObj;
import com.appodus.commons.events.OnOrganizationModifiedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/organizations")
public class OrganizationController {
    private final OrganizationService organizationService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public OrganizationController(
            OrganizationService organizationService,
            ApplicationEventPublisher eventPublisher) {
        this.organizationService = organizationService;
        this.eventPublisher = eventPublisher;
    }
    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody Organization organization, final HttpServletRequest request) {
        System.out.println("organization: " + organization);
        Organization _organization = organizationService.create(organization);

        eventPublisher.publishEvent(new OnOrganizationModifiedEvent(new EventObj(EventObj.EventType.CREATE.getType(), _organization), this.getClass()));

        sResponse = new ServerResponse(_organization);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }
    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<Organization> organizationList = organizationService.getAll();

        sResponse = new ServerResponse(organizationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("{organizationId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("organizationId") String organizationId, final HttpServletRequest request) {
        Organization organization = organizationService.getById(organizationId);

        sResponse = new ServerResponse(organization);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<Organization> organizationList = organizationService.getAllActive();

        sResponse = new ServerResponse(organizationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<Organization> organizationList = organizationService.getAllInactive();

        sResponse = new ServerResponse(organizationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-organization")
    public ResponseEntity<ServerResponse> getAllByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Organization> organizationList = organizationService.getAllByOrganization(organization);

        sResponse = new ServerResponse(organizationList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{organizationId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("organizationId") String organizationId,
            @RequestBody Organization organization,
            final HttpServletRequest request) {

        Organization updatedOrganization = organizationService.update(organizationId, organization);

        eventPublisher.publishEvent(new OnOrganizationModifiedEvent(new EventObj(EventObj.EventType.UPDATE.getType(), updatedOrganization), this.getClass()));

        sResponse = new ServerResponse(updatedOrganization);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{organizationId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("organizationId") String organizationId,
            final HttpServletRequest request) {

        Organization updatedOrganization = organizationService.activate(organizationId);

        eventPublisher.publishEvent(new OnOrganizationModifiedEvent(new EventObj(EventObj.EventType.ACTIVATE.getType(), updatedOrganization), this.getClass()));

        sResponse = new ServerResponse(updatedOrganization);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{organizationId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("organizationId") String organizationId,
            final HttpServletRequest request) {

        Organization updatedOrganization = organizationService.inactivate(organizationId);

        eventPublisher.publishEvent(new OnOrganizationModifiedEvent(new EventObj(EventObj.EventType.IN_ACTIVATE.getType(), updatedOrganization), this.getClass()));

        sResponse = new ServerResponse(updatedOrganization);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{organizationId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("organizationId") String organizationId,
            final HttpServletRequest request) {

        Organization updatedOrganization = organizationService.delete(organizationId);

        eventPublisher.publishEvent(new OnOrganizationModifiedEvent(new EventObj(EventObj.EventType.DELETE.getType(), updatedOrganization), this.getClass()));

        sResponse = new ServerResponse(updatedOrganization);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{organizationId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("organizationId") String organizationId,
            final HttpServletRequest request) {

        organizationService.hardDelete(organizationId);

        eventPublisher.publishEvent(new OnOrganizationModifiedEvent(new EventObj(EventObj.EventType.HARD_DELETE.getType(), null), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
