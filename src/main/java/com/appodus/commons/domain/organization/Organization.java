package com.appodus.commons.domain.organization;


import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.contact.Contact;
import com.appodus.commons.domain.user.User;
import com.appodus.commons.domain.user.role.permission.Permission;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Organization extends AbstractEntity {

    @Column(updatable = false, nullable = false, unique = true)
    private long orgNumber; // Number unique to an org for identification
    @Column(nullable = false, unique = true)
    private String name;
    @ManyToMany
    @JoinTable(
            name = "organization_permissions",
            joinColumns = @JoinColumn(name = "organization_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
    private List<Permission> permissionList;
    @OneToOne
    private Contact contact;
    @OneToMany
    @JoinTable(
            name = "organization_users",
            joinColumns = @JoinColumn(name = "organization_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private List<User> users;

    public Organization() {
        super("", "", "");

        this.contact = new Contact();
    }

    /**
     * Used by Filter(s) to Add Request Organization to ActiveAuditor
     *
     * @param id "Organization unique Id"
     */
    public Organization(String id) {
        super();

        this.id = id;
    }

    @PrePersist
    public void prePersist() {

        if (this.users != null) {
            if (StringUtils.isEmpty(this.users.get(0).getUsername())) {
                this.users = null;
            }
        }

        if (this.permissionList != null) {
            if (StringUtils.isEmpty(this.permissionList.get(0).getId())) {
                this.permissionList = null;
            }
        }
    }
    @PreUpdate
    public void preUpdate()
    {
        prePersist();
    }
}
