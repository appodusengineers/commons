package com.appodus.commons.domain.abstractservice;

import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.organization.Organization;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface AbstractDao<E extends AbstractEntity> extends CrudRepository<E, String> {
//TODO: fIX these queries

//    List<E> findAllByOrganization(Organization organization);
}
