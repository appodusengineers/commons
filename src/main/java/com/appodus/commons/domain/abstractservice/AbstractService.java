package com.appodus.commons.domain.abstractservice;

import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.exception.UnknownException;
import com.appodus.commons.security.ActiveAuditor;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Represents the base Service of any Entity Service.
 * <p>
 * <p>
 * Should be extended by Entity Services.
 * Requires the Service's Entity E, which must extend AbstractEntity and the Entity's Dao D, which
 * must extend CrudRepository<E, Long>.
 * <p>
 *
 * @author Kingsley Ezenwere
 * @see com.appodus.commons.domain.abstractentity.AbstractEntity
 * @see org.springframework.data.repository.CrudRepository
 * @since 0.0.1
 */
public abstract class AbstractService<E extends AbstractEntity, D extends AbstractDao<E>> {

    @Autowired
    private D dao;

    @Autowired
    private ActiveAuditor activeAuditor;
    private Logger logger = LoggerFactory.getLogger(AbstractService.class.getSimpleName());

    public E create(E e) {
        e.setCreatedBy(activeAuditor.getCurrentAuditor());
        // Use Organization from ActiveAuditor only when not already set
        if (e.getOrganization() == null) {
            e.setOrganization(activeAuditor.getOrganization());
        }

//        e.setCreatedByIp(activeAuditor.getUser().getLastLoginIp());
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<E>> constraintViolations = validator.validate(e);

        if (constraintViolations.size() > 0 ) {
            logger.info("VIOLATION OCCURED");
            for (ConstraintViolation<E> contraints : constraintViolations) {
                logger.info(contraints.getRootBeanClass().getSimpleName()+
                        "." + contraints.getPropertyPath() + " " + contraints.getMessage());
            }
        }


        try {
            e = dao.save(e);
        } catch (DataIntegrityViolationException | ConstraintViolationException ex) {
            ex.printStackTrace();
            throw new UnknownException(MessageFormat.format(e.EXISTED_MESSAGE, e.getClass().getSimpleName()));
        }

        return e;
    }

    public List<E> createAll(List<E> eList) {
        List<E> _eList = new ArrayList<>();

        // Avoid null pointer exception
        if (eList == null){
            return _eList;
        }

        eList.forEach((e) -> {

            e.setCreatedBy(activeAuditor.getCurrentAuditor());
            // Use Organization from ActiveAuditor only when not already set
            if (e.getOrganization() == null) {
                e.setOrganization(activeAuditor.getOrganization());
            }
//            e.setCreatedByIp(ActiveAuditor.getUser().getLastLoginIp());
            _eList.add(e);
        });

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<List<E>>> constraintViolations = validator.validate(_eList);

        if (constraintViolations.size() > 0 ) {
            logger.info("VIOLATION OCCURED");
            for (ConstraintViolation<List<E>> contraints : constraintViolations) {
                logger.info(contraints.getRootBeanClass().getSimpleName()+
                        "." + contraints.getPropertyPath() + " " + contraints.getMessage());
            }
        }



        try {
            eList = (List<E>) dao.save(eList);
        } catch (DataIntegrityViolationException | ConstraintViolationException ex) {
            ex.printStackTrace();
            throw new UnknownException(MessageFormat.format(eList.get(0).EXISTED_MESSAGE, eList.get(0).getClass().getSimpleName()));
        }catch(Exception e){
            e.printStackTrace();
        }

        return eList;
    }

    public E update(E e) {
        if (!exists(e)) {
//            throw new RequestedContentNotFoundException(e.NOT_EXISTED_MESSAGE);
        }

        e.setLastModifiedBy(activeAuditor.getCurrentAuditor());
//        e.setLastModifiedByIp(ActiveAuditor.getUser().getLastLoginIp());
        return dao.save(e);
    }

    public List<E> updateAll(List<E> eList) {
        List<E> _eList = new ArrayList<>();

        // Avoid null pointer exception
        if (eList == null){
            return _eList;
        }

        eList.forEach((e) -> {
            if (exists(e)) {
                e.setLastModifiedBy(activeAuditor.getCurrentAuditor());
//                e.setLastModifiedByIp(ActiveAuditor.getUser().getLastLoginIp());
                _eList.add(e);
            }
        });

        return (List<E>) dao.save(_eList);
    }


    public E upSert(E e) {
        if (!exists(e)) {
            return create(e);
        }

        return update(e);
    }

    public E getById(String id) {
        E e = dao.findOne(id);
        if (e == null) {
//            throw new RequestedContentNotFoundException(e.NOT_EXISTED_MESSAGE);
        }

        return e;
    }

    public List<E> getAll() {
        List<E> eList = (List<E>) dao.findAll();

        if (eList.isEmpty()) {
//            throw new RequestedContentNotFoundException(e.LIST_NOT_FOUND_MESSAGE);
        }

        return eList;
    }

    //    public List<E> getAllByOrganization(Organization organization) {
//        List<E> eList = (List<E>) dao.findAllByOrganization(organization);
//
//        if (eList.isEmpty()) {
////            throw new RequestedContentNotFoundException(e.LIST_NOT_FOUND_MESSAGE);
//        }
//
//        return eList;
//    }
    public List<E> getAllActive() {
        List<E> eList = getAll()
                .stream()
                .filter((e) -> (e.isActive() == true))
                .collect(Collectors.toList());

        if (eList.isEmpty()) {
//            throw new RequestedContentNotFoundException(e.LIST_NOT_FOUND_MESSAGE);
        }

        return eList;
    }

    public List<E> getAllInactive() {
        List<E> eList = getAll()
                .stream()
                .filter((e) -> (e.isActive() == false))
                .collect(Collectors.toList());

        if (eList.isEmpty()) {
//            throw new RequestedContentNotFoundException(e.LIST_NOT_FOUND_MESSAGE);
        }

        return eList;
    }

    public E activate(String id) {

        E e = getById(id);

        e.setActive(true);
        e.setActivatedDate(new Date());
        e.setActivatedBy(activeAuditor.getCurrentAuditor());
//        e.setActivatedByIp(ActiveAuditor.getUser().getLastLoginIp());
        return update(e);
    }

    public E inactivate(String id) {

        E e = getById(id);

        e.setActive(false);
        e.setInactivatedDate(new Date());
        e.setInactivatedBy(activeAuditor.getCurrentAuditor());
//        e.setInactivatedByIp(ActiveAuditor.getUser().getLastLoginIp());
        return update(e);
    }

    public E archive(String id) {

        E e = getById(id);

        e.setArchived(true);
        e.setArchivedDate(new Date());
        e.setArchivedBy(activeAuditor.getCurrentAuditor());
//        e.setArchivedByIp(ActiveAuditor.getUser().getLastLoginIp());
        return update(e);
    }

    public E unarchive(String id) {

        E e = getById(id);

        e.setArchived(false);
        e.setUnarchivedDate(new Date());
        e.setUnarchivedBy(activeAuditor.getCurrentAuditor());
//        e.setUnarchivedByIp(ActiveAuditor.getUser().getLastLoginIp());
        return update(e);
    }

    public E delete(E e) {
        return delete(e.getId());
    }

    public E delete(String id) {

        E e = getById(id);

        e.setDeleted(true);
        e.setDeletedDate(new Date());
        e.setDeletedBy(activeAuditor.getCurrentAuditor());
//        e.setDeletedByIp(ActiveAuditor.getUser().getLastLoginIp());
        return update(e);
    }

    public void deleteAll(List<E> eList) {
        List<E> _eList = new ArrayList<>();

        // Avoid null pointer exception
        if (eList == null){
            return;
        }

        eList.forEach((e) -> {
            if (exists(e)) {
                e.setDeleted(true);
                e.setDeletedDate(new Date());
                e.setDeletedBy(activeAuditor.getCurrentAuditor());
//                e.setDeletedByIp(ActiveAuditor.getUser().getLastLoginIp());
                _eList.add(e);
            }
        });

        dao.save(_eList);
    }

    public void hardDelete(E e) {
        dao.delete(e);
    }

    public void hardDelete(String id) {
        dao.delete(id);
    }

    public void hardDeleteAll(List<E> eList) {

        // Avoid null pointer exception
        if (eList == null){
            return;
        }

        dao.delete(eList);
    }

    public long count() {
        return dao.count();
    }

    public boolean exists(String id) {
        if (id == null) {
            return false;
        }

        return dao.exists(id);
    }

    public boolean exists(E e) {
        return exists(e.getId());
    }
}
