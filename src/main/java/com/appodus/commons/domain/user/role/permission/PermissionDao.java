package com.appodus.commons.domain.user.role.permission;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.profile.school.educationlevel.EducationLevel;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface PermissionDao extends AbstractDao<Permission> {

    Permission findByNameAndOrganization(String name, Organization organization);

    List<Permission> findAllByOrganization(Organization organization);
    List<Permission> findAllByOrganizationAndActive(Organization organization, boolean isActive);
    List<Permission> findAllByOrganizationAndDeleted(Organization organization, boolean isDeleted);
}
