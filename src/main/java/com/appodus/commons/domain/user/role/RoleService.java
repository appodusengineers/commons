package com.appodus.commons.domain.user.role;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class RoleService extends AbstractService<Role, RoleDao> {
    private final RoleDao roleDao;
    private final ActiveAuditor activeAuditor;

    @Autowired
    public RoleService(RoleDao roleDao, ActiveAuditor activeAuditor) {
        this.roleDao = roleDao;
        this.activeAuditor = activeAuditor;
    }

    public Role getByNameAndOrganization(String name, Organization organization) {
        return roleDao.findByNameAndOrganization(name, organization);
    }

    /**
     * Returns List of All Roles in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Role> getAllByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return roleDao.findAllByOrganization(_organization);
    }


    /**
     * Returns List of All Active Roles in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Role> getAllActiveByOrganization(Organization organization) {
        System.out.println("organization: " + organization);
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }
        System.out.println("_organization: " + _organization);
        return roleDao.findAllByOrganizationAndActive(_organization, true);
    }


    /**
     * Returns List of All Inactive Roles in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Role> getAllInactiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return roleDao.findAllByOrganizationAndActive(_organization, false);
    }


    /**
     * Returns List of All Deleted Roles in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Role> getAllDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return roleDao.findAllByOrganizationAndDeleted(_organization, true);
    }


    /**
     * Returns List of All Non Deleted Roles in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Role> getAllNonDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return roleDao.findAllByOrganizationAndDeleted(_organization, false);
    }
    public Role update(String roleId, Role role) {
        return super.update(role);
    }

    public void initOrganizationRoles(Organization organization) {

        if(getAllByOrganization(organization).size() < 1) {
            super.createAll(Arrays.asList(
                    new Role("Admin", "Admin", "Access to all permissions", organization),
                    new Role("Member", "Member", "Access to own account", organization),
                    new Role("Staff", "Staff", "Basic access", organization)
            ));
        }
    }
}
