package com.appodus.commons.domain.user.role;

import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import com.appodus.commons.domain.user.role.permission.Permission;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"name", "organization_id"}),
        @UniqueConstraint(columnNames = {"systemName", "organization_id"})
})
public class Role extends AbstractEntity {
    @Column(nullable = false)
    private String name;
    @Column(nullable = false, updatable = false)
    private String systemName;
    private String description;

    protected boolean locked;
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lockedDate;
    protected String lockedBy;
    protected String lockedByIp;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date unlockedDate;
    protected String unlockedBy;
    protected String unlockedByIp;

    @ManyToMany
    @JoinTable(
            name = "roles_permissions",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
    private List<Permission> permissionList;
    @ManyToMany
    @JoinTable(
            name = "roles_users",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private List<User> userList;

    public Role() {
        super("", "", "");
    }

    public Role(String name, String systemName, String description, Organization organization) {
        this();

        this.name = name;
        this.systemName = systemName;
        this.description = description;
        this.organization = organization;
    }

    @PrePersist
    public void prePersist() {

        if (this.permissionList != null) {
            if (StringUtils.isEmpty(this.permissionList.get(0).getId())) {
                this.permissionList = null;
            }
        }

        if (this.userList != null) {
            if (StringUtils.isEmpty(this.userList.get(0).getId())) {
                this.userList = null;
            }
        }

    }
    @PreUpdate
    public void preUpdate()
    {
        prePersist();
    }
}
