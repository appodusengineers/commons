package com.appodus.commons.domain.user;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.events.EventObj;
import com.appodus.commons.events.OnUserModifiedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@RestController
@RequestMapping("v1/users")
public class UserController {
    private final UserService userService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public UserController   (UserService userService, ApplicationEventPublisher eventPublisher) {
        this.userService = userService;
        this.eventPublisher = eventPublisher;
    }


    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody User user, final HttpServletRequest request) {
        System.out.println("User: " + user);
        User _user = userService.create(user);

        eventPublisher.publishEvent(new OnUserModifiedEvent(new EventObj(EventObj.EventType.CREATE.getType(), _user), this.getClass()));

        sResponse = new ServerResponse(_user);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }

    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<User> userList = userService.getAll();

        sResponse = new ServerResponse(userList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{userId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("userId") String userId, final HttpServletRequest request) {
        User user = userService.getById(userId);

        sResponse = new ServerResponse(user);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<User> userList = userService.getAllActive();

        sResponse = new ServerResponse(userList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<User> userList = userService.getAllInactive();

        sResponse = new ServerResponse(userList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-organization")
    public ResponseEntity<ServerResponse> getAllByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<User> userList = userService.getAllByOrganization(organization);

        sResponse = new ServerResponse(userList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("active/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<User> userList = userService.getAllActiveByOrganization(organization);

        sResponse = new ServerResponse(userList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("inactive/by-organization")
    public ResponseEntity<ServerResponse> getAllInactiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<User> userList = userService.getAllInactiveByOrganization(organization);

        sResponse = new ServerResponse(userList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<User> userList = userService.getAllDeletedByOrganization(organization);

        sResponse = new ServerResponse(userList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("non-deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllNonDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<User> userList = userService.getAllNonDeletedByOrganization(organization);

        sResponse = new ServerResponse(userList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{userId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("userId") String userId,
            @RequestBody User user,
            final HttpServletRequest request) {

        System.out.println("user: " + user);

        User updatedUser = userService.update(userId, user);

        eventPublisher.publishEvent(new OnUserModifiedEvent(new EventObj(EventObj.EventType.UPDATE.getType(), updatedUser), this.getClass()));

        sResponse = new ServerResponse(updatedUser);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{userId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("userId") String userId,
            final HttpServletRequest request) {

        User activatedUser = userService.activate(userId);

        eventPublisher.publishEvent(new OnUserModifiedEvent(new EventObj(EventObj.EventType.ACTIVATE.getType(), activatedUser), this.getClass()));

        sResponse = new ServerResponse(activatedUser);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{userId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("userId") String userId,
            final HttpServletRequest request) {

        User inactivatedUser = userService.inactivate(userId);

        eventPublisher.publishEvent(new OnUserModifiedEvent(new EventObj(EventObj.EventType.IN_ACTIVATE.getType(), inactivatedUser), this.getClass()));

        sResponse = new ServerResponse(inactivatedUser);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{userId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("userId") String userId,
            final HttpServletRequest request) {

        User deletedUser = userService.delete(userId);

        eventPublisher.publishEvent(new OnUserModifiedEvent(new EventObj(EventObj.EventType.DELETE.getType(), deletedUser), this.getClass()));

        sResponse = new ServerResponse(deletedUser);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{userId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("userId") String userId,
            final HttpServletRequest request) {

        userService.hardDelete(userId);

        eventPublisher.publishEvent(new OnUserModifiedEvent(new EventObj(EventObj.EventType.HARD_DELETE.getType(), null), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
