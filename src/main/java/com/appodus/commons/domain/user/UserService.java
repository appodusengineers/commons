package com.appodus.commons.domain.user;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.contact.Contact;
import com.appodus.commons.domain.contact.ContactService;
import com.appodus.commons.domain.contact.address.Address;
import com.appodus.commons.domain.contact.email.Email;
import com.appodus.commons.domain.contact.email.EmailService;
import com.appodus.commons.domain.contact.phone.PhoneService;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.organization.OrganizationService;
import com.appodus.commons.domain.profile.Profile;
import com.appodus.commons.domain.profile.ProfileService;
import com.appodus.commons.domain.user.role.RoleService;
import com.appodus.commons.events.EventObj;
import com.appodus.commons.events.OnOrganizationModifiedEvent;
import com.appodus.commons.events.OnUserModifiedEvent;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class UserService extends AbstractService<User, UserDao> {
    private final UserDao userDao;
    private final ContactService contactService;
    private final ProfileService profileService;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;
    private final ActiveAuditor activeAuditor;
    private final OrganizationService organizationService;
    private final ApplicationEventPublisher eventPublisher;
    private final PhoneService phoneService;
    private final EmailService emailService;

    @Autowired
    public UserService(UserDao userDao, ContactService contactService, ProfileService profileService, RoleService roleService, PasswordEncoder passwordEncoder, ActiveAuditor activeAuditor, OrganizationService organizationService, ApplicationEventPublisher eventPublisher, PhoneService phoneService, EmailService emailService) {
        this.userDao = userDao;
        this.contactService = contactService;
        this.profileService = profileService;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
        this.activeAuditor = activeAuditor;
        this.organizationService = organizationService;
        this.eventPublisher = eventPublisher;
        this.phoneService = phoneService;
        this.emailService = emailService;
    }

    @Transactional
    public User create(User user) {
        // Constraint Validation
        // ...phoneNumber
        String ext = user.getContact().getPhoneList().get(0).getExt();
        String phone = user.getContact().getPhoneList().get(0).getValue();
        if (phoneService.getByExtAndValue(ext, phone) != null) {
            throw new RuntimeException(String.format("The phone, %s%s, is already in use.", ext, phone));
        }
        // ...email
        String email = user.getContact().getEmailList().get(0).getValue();
        if (emailService.getByValue(email) != null) {
            throw new RuntimeException(String.format("The email, %s, is already in use.", email));
        }

        user.setProfile(profileService.create(user.getProfile()));
        user.setContact(contactService.create(user.getContact()));

        user.setPassword(passwordEncoder.encode(user.getPassword())); // Encode password
        return super.create(user);
    }

    public User getByUsername(String username) {
        return userDao.findByUsername(username);
    }
    
    public List<User> getAll() { // Don't get Internal Users
        return super.getAll().stream().filter((user) -> user.isInternalUser() != true).collect(Collectors.toList());
    }

    /**
     * Returns List of All Users in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<User> getAllByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return userDao.findAllByOrganizationAndInternalUser(_organization, false);
    }


    /**
     * Returns List of All Active Users in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<User> getAllActiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return userDao.findAllByOrganizationAndActiveAndInternalUser(_organization, true, false);
    }


    /**
     * Returns List of All Inactive Users in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<User> getAllInactiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return userDao.findAllByOrganizationAndActiveAndInternalUser(_organization, false, false);
    }


    /**
     * Returns List of All Deleted Users in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<User> getAllDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return userDao.findAllByOrganizationAndDeletedAndInternalUser(_organization, true, false);
    }


    /**
     * Returns List of All Non Deleted Users in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<User> getAllNonDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return userDao.findAllByOrganizationAndDeletedAndInternalUser(_organization, false, false);
    }
    public User update(String userId, User user) {
        return super.update(user);
    }

    @PostConstruct
    public void init() {

        if (count() < 1){
            // Create Aquila
            Organization _organization = new Organization();
            _organization.setName("Appodus Data Systems ");
            _organization.getContact().setEmailList(Arrays.asList(new Email("churchrun@gmail.com")));
            _organization.getContact().setAddressList(Arrays.asList(new Address("23 Parkview Estate, Ikoyi, Lagos state, Nigeria")));
            Organization organization = organizationService.create(_organization);

            eventPublisher.publishEvent(new OnOrganizationModifiedEvent(new EventObj(EventObj.EventType.CREATE.getType(), organization), this.getClass()));

            User aquila = activeAuditor.getRawAquila();
            aquila.setRoles(roleService.getAll());
            aquila.setOrganization(organization);

            create(aquila);
        }
    }
}
