package com.appodus.commons.domain.user.role;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import com.appodus.commons.events.EventObj;
import com.appodus.commons.events.OnRoleModifiedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@RestController
@RequestMapping("v1/roles")
public class RoleController {
    private final RoleService roleService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public RoleController(RoleService roleService, ApplicationEventPublisher eventPublisher) {
        this.roleService = roleService;
        this.eventPublisher = eventPublisher;
    }


    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody Role role, final HttpServletRequest request) {
        System.out.println("Role: " + role);
//        role.setGroupMembershipList(null); // Very Important; A fresh role has no member
        Role _role = roleService.create(role);

        eventPublisher.publishEvent(new OnRoleModifiedEvent(new EventObj(EventObj.EventType.CREATE.getType(), _role), this.getClass()));

        sResponse = new ServerResponse(_role);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }

    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<Role> roleList = roleService.getAll();

        sResponse = new ServerResponse(roleList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{roleId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("roleId") String roleId, final HttpServletRequest request) {
        Role role = roleService.getById(roleId);

        sResponse = new ServerResponse(role);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<Role> roleList = roleService.getAllActive();

        sResponse = new ServerResponse(roleList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<Role> roleList = roleService.getAllInactive();

        sResponse = new ServerResponse(roleList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-organization")
    public ResponseEntity<ServerResponse> getAllByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Role> roleList = roleService.getAllByOrganization(organization);

        sResponse = new ServerResponse(roleList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("active/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Role> roleList = roleService.getAllActiveByOrganization(organization);

        sResponse = new ServerResponse(roleList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("inactive/by-organization")
    public ResponseEntity<ServerResponse> getAllInactiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Role> roleList = roleService.getAllInactiveByOrganization(organization);

        sResponse = new ServerResponse(roleList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Role> roleList = roleService.getAllDeletedByOrganization(organization);

        sResponse = new ServerResponse(roleList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("non-deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllNonDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Role> roleList = roleService.getAllNonDeletedByOrganization(organization);

        sResponse = new ServerResponse(roleList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{roleId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("roleId") String roleId,
            @RequestBody Role role,
            final HttpServletRequest request) {

        System.out.println("role: " + role);

        Role updatedRole = roleService.update(roleId, role);

        eventPublisher.publishEvent(new OnRoleModifiedEvent(new EventObj(EventObj.EventType.UPDATE.getType(), updatedRole), this.getClass()));

        sResponse = new ServerResponse(updatedRole);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{roleId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("roleId") String roleId,
            final HttpServletRequest request) {

        Role activatedRole = roleService.activate(roleId);

        eventPublisher.publishEvent(new OnRoleModifiedEvent(new EventObj(EventObj.EventType.ACTIVATE.getType(), activatedRole), this.getClass()));

        sResponse = new ServerResponse(activatedRole);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{roleId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("roleId") String roleId,
            final HttpServletRequest request) {

        Role inactivatedRole = roleService.inactivate(roleId);

        eventPublisher.publishEvent(new OnRoleModifiedEvent(new EventObj(EventObj.EventType.IN_ACTIVATE.getType(), inactivatedRole), this.getClass()));

        sResponse = new ServerResponse(inactivatedRole);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{roleId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("roleId") String roleId,
            final HttpServletRequest request) {

        Role deletedRole = roleService.delete(roleId);

        eventPublisher.publishEvent(new OnRoleModifiedEvent(new EventObj(EventObj.EventType.DELETE.getType(), deletedRole), this.getClass()));

        sResponse = new ServerResponse(deletedRole);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{roleId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("roleId") String roleId,
            final HttpServletRequest request) {

        roleService.hardDelete(roleId);

        eventPublisher.publishEvent(new OnRoleModifiedEvent(new EventObj(EventObj.EventType.HARD_DELETE.getType(), null), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
