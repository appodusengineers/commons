package com.appodus.commons.domain.user.role.permission;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import com.appodus.commons.events.EventObj;
import com.appodus.commons.events.OnPermissionModifiedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@RestController
@RequestMapping("v1/permissions")
public class PermissionController {
    private final PermissionService permissionService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public PermissionController(PermissionService permissionService, ApplicationEventPublisher eventPublisher) {
        this.permissionService = permissionService;
        this.eventPublisher = eventPublisher;
    }

    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<Permission> permissionList = permissionService.getAll();

        sResponse = new ServerResponse(permissionList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{permissionId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("permissionId") String permissionId, final HttpServletRequest request) {
        Permission permission = permissionService.getById(permissionId);

        sResponse = new ServerResponse(permission);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<Permission> permissionList = permissionService.getAllActive();

        sResponse = new ServerResponse(permissionList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<Permission> permissionList = permissionService.getAllInactive();

        sResponse = new ServerResponse(permissionList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-organization")
    public ResponseEntity<ServerResponse> getAllByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Permission> permissionList = permissionService.getAllByOrganization(organization);

        sResponse = new ServerResponse(permissionList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("active/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Permission> permissionList = permissionService.getAllActiveByOrganization(organization);

        sResponse = new ServerResponse(permissionList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("inactive/by-organization")
    public ResponseEntity<ServerResponse> getAllInactiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Permission> permissionList = permissionService.getAllInactiveByOrganization(organization);

        sResponse = new ServerResponse(permissionList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Permission> permissionList = permissionService.getAllDeletedByOrganization(organization);

        sResponse = new ServerResponse(permissionList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("non-deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllNonDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Permission> permissionList = permissionService.getAllNonDeletedByOrganization(organization);

        sResponse = new ServerResponse(permissionList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{permissionId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("permissionId") String permissionId,
            @RequestBody Permission permission,
            final HttpServletRequest request) {

        System.out.println("permission: " + permission);

        Permission updatedPermission = permissionService.update(permissionId, permission);

        eventPublisher.publishEvent(new OnPermissionModifiedEvent(new EventObj(EventObj.EventType.UPDATE.getType(), updatedPermission), this.getClass()));

        sResponse = new ServerResponse(updatedPermission);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{permissionId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("permissionId") String permissionId,
            final HttpServletRequest request) {

        Permission activatedPermission = permissionService.activate(permissionId);

        eventPublisher.publishEvent(new OnPermissionModifiedEvent(new EventObj(EventObj.EventType.ACTIVATE.getType(), activatedPermission), this.getClass()));

        sResponse = new ServerResponse(activatedPermission);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{permissionId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("permissionId") String permissionId,
            final HttpServletRequest request) {

        Permission inactivatedPermission = permissionService.inactivate(permissionId);

        eventPublisher.publishEvent(new OnPermissionModifiedEvent(new EventObj(EventObj.EventType.IN_ACTIVATE.getType(), inactivatedPermission), this.getClass()));

        sResponse = new ServerResponse(inactivatedPermission);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

}
