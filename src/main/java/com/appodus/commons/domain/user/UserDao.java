package com.appodus.commons.domain.user;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.role.Role;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface UserDao extends AbstractDao<User> {
    User findByUsername(String username);

    List<User> findAllByOrganizationAndInternalUser(Organization organization, boolean isInternalUser);
    List<User> findAllByOrganizationAndActiveAndInternalUser(Organization organization, boolean isActive, boolean isInternalUser);
    List<User> findAllByOrganizationAndDeletedAndInternalUser(Organization organization, boolean isDeleted, boolean isInternalUser);
}
