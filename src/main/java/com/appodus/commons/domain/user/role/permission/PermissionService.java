package com.appodus.commons.domain.user.role.permission;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class PermissionService extends AbstractService<Permission, PermissionDao> {

    private final PermissionDao permissionDao;
    private final ActiveAuditor activeAuditor;

    @Autowired
    public PermissionService(PermissionDao permissionDao, ActiveAuditor activeAuditor) {
        this.permissionDao = permissionDao;
        this.activeAuditor = activeAuditor;
    }
    public Permission getByNameAndOrganization(String name, Organization organization) {
        return permissionDao.findByNameAndOrganization(name, organization);
    }

    /**
     * Returns List of All Permissions in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Permission> getAllByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return permissionDao.findAllByOrganization(_organization);
    }


    /**
     * Returns List of All Active Permissions in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Permission> getAllActiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return permissionDao.findAllByOrganizationAndActive(_organization, true);
    }


    /**
     * Returns List of All Inactive Permissions in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Permission> getAllInactiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return permissionDao.findAllByOrganizationAndActive(_organization, false);
    }


    /**
     * Returns List of All Deleted Permissions in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Permission> getAllDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return permissionDao.findAllByOrganizationAndDeleted(_organization, true);
    }


    /**
     * Returns List of All Non Deleted Permissions in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Permission> getAllNonDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return permissionDao.findAllByOrganizationAndDeleted(_organization, false);
    }
    public Permission update(String permissionId, Permission permission) {
        return super.update(permission);
    }
}
