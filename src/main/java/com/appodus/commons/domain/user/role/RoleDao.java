package com.appodus.commons.domain.user.role;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.role.permission.Permission;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface RoleDao extends AbstractDao<Role> {

    Role findByNameAndOrganization(String name, Organization organization);

    List<Role> findAllByOrganization(Organization organization);
    List<Role> findAllByOrganizationAndActive(Organization organization, boolean isActive);
    List<Role> findAllByOrganizationAndDeleted(Organization organization, boolean isDeleted);
}
