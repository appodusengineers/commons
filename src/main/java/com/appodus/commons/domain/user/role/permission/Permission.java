package com.appodus.commons.domain.user.role.permission;


import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.user.role.Role;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.*;
import java.util.Date;
import java.util.List;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Permission extends AbstractEntity {
    @Column(nullable = false, unique = true)
    private String name;
    @Column(nullable = false, unique = true)
    private String systemName;
    private String description;

    protected boolean locked;
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lockedDate;
    protected String lockedBy;
    protected String lockedByIp;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date unlockedDate;
    protected String unlockedBy;
    protected String unlockedByIp;
    @ManyToMany
    @JoinTable(
            name = "permissions_roles",
            joinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private List<Role> roleList;

    public Permission() {
        super("", "", "");
    }
}
