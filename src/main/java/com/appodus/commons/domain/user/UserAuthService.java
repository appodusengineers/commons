package com.appodus.commons.domain.user;

import com.appodus.commons.domain.user.role.Role;
import com.appodus.commons.domain.user.role.permission.Permission;
import com.appodus.commons.exception.RequestedContentNotFoundException;
import com.appodus.commons.exception.UserNotActivatedException;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by NanoSoft Solutions on 2/2/2017.
 */
@Service
public class UserAuthService implements UserDetailsService {

    private final UserService userService;
    private final ActiveAuditor activeAuditor;

    @Autowired
    public UserAuthService(UserService userService, ActiveAuditor activeAuditor) {
        this.userService = userService;
        this.activeAuditor = activeAuditor;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("Username: " + username);

        if (username.isEmpty()) {
            throw new UsernameNotFoundException("Username is empty!");
        }

        User user = userService.getByUsername(username);
        System.out.println("user: " + user);

        if (user == null){
            throw new RequestedContentNotFoundException("No such user.");
        }
        // Not withing working hour
//        if (!isWorkingHour(user.getWorkingHour())) {
//            throw new OffWorkingHourException("Wrong working hour");
//        }

        // Is Deleted
        if (user.isDeleted()) {
            throw new UserNotActivatedException("User " + username + " had been deleted!");
        }

        // Is Disabled
        if (!user.isAccountNonExpired()) {
            throw new UserNotActivatedException("This account has expired! ");
        }
        if (!user.isEnabled()) {// Account is disabled
            throw new UserNotActivatedException("User " + username + " is not active!");
        }

        // Is locked
//        if (user.isDormant()) {
//            throw new UserNotActivatedException("This account is dormant and has been locked! You would need to have it unlocked.");
//        }
        if (!user.isAccountNonLocked()) {// Account is locked
            throw new UserNotActivatedException("This account has been locked! ");
        }

        // SET ACTIVE AUDITOR
        activeAuditor.setUser(user);
        activeAuditor.setOrganization(user.getOrganization());

        // RETURN USER
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(), user.getPassword(), user.isEnabled(),
                user.isAccountNonExpired(), user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                getAuthorities(user.getRoles()) //Collections.emptyList()
        );
    }


    private Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> roles) {
        return getGrantedAuthorities(getPermissions(roles));
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> permissions) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        permissions.forEach((permission) -> {
            authorities.add(new SimpleGrantedAuthority(permission));
        });

        return authorities;
    }

    private List<String> getPermissions(Collection<Role> roles) {
        List<String> permissionsNameList = new ArrayList<>();
        List<Permission> permissions = new ArrayList<>();
        roles.forEach((role) -> {
             permissions.addAll(role.getPermissionList());
        });

        permissions.forEach((permission) -> {
            permissionsNameList.add(permission.getSystemName());
        });

        permissionsNameList.add("TESTING");
        return permissionsNameList;
    }

}
