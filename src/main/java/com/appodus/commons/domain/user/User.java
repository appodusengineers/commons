package com.appodus.commons.domain.user;

import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.contact.Contact;
import com.appodus.commons.domain.profile.Profile;
import com.appodus.commons.domain.user.role.Role;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static javax.persistence.CascadeType.*;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class User extends AbstractEntity implements UserDetails, CredentialsContainer {

    @Column(nullable = false, unique = true)
    private String username;
    @Column(nullable = false)
    private String password;
    private boolean internalUser;

    @Transient
    private Collection<? extends GrantedAuthority> authorities;

    private boolean accountNonExpired;
    private String accountNonExpiredBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date accountNonExpiredDate;

    private String accountExpiredBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date accountExpiredDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date accountExpiresDate;
    private String accountExpiresDateSetBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date accountExpiresDateSetOn;

    private boolean accountNonLocked;
    private String accountNonLockedBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date accountNonLockedDate;

    private String accountLockedBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date accountLockedDate;

    private boolean credentialsNonExpired;
    private String credentialsNonExpiredBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date credentialsNonExpiredDate;

    private String credentialsExpiredBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date credentialsExpiredDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date credentialsExpiresDate;
    private String credentialsExpiresDateSetBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date credentialsExpiresDateSetOn;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private List<Role> roles;

    @OneToOne(cascade = {PERSIST, MERGE, REMOVE, REFRESH, DETACH})
    private Profile profile;

    @OneToOne(cascade = {PERSIST, MERGE, REMOVE, REFRESH, DETACH})
    private Contact contact;
    private String lastLoginIp;


// ~ Constructors
    // ===================================================================================================

    public User() {
        super("A user with this email(username) already exists.", "Username/Password mis-match!", "No users yet.");

        // Init Object Compositions
//        this.roles = Arrays.asList(new Role());
        this.profile = new Profile();
        this.contact = new Contact();
    }

    public User(String username, String password) {
        this(username, password, true, true, true, true);
    }

    /**
     * Calls the more complex constructor with all boolean arguments set to {@code true}.
     */
    public User(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        this(username, password);
        this.authorities = authorities;
    }

    /**
     * Construct the <code>User</code> with the details required by
     * {@link org.springframework.security.authentication.dao.DaoAuthenticationProvider}.
     *
     * @param username              the username presented to the
     *                              <code>DaoAuthenticationProvider</code>
     * @param password              the password that should be presented to the
     *                              <code>DaoAuthenticationProvider</code>
     * @param active                set to <code>true</code> if the user is active (enabled)
     * @param accountNonExpired     set to <code>true</code> if the account has not expired
     * @param credentialsNonExpired set to <code>true</code> if the credentials has not expired
     * @param accountNonLocked      set to <code>true</code> if the account is not locked
     *                              //     * @param authorities           the authorities that should be granted to the caller if they
     *                              presented the correct username and password and the user is enabled. Not null.
     * @throws IllegalArgumentException if a <code>null</code> value was passed either as
     *                                  a parameter or as an element in the <code>GrantedAuthority</code> collection
     */
    public User(String username, String password, boolean active,
                boolean accountNonExpired, boolean credentialsNonExpired,
                boolean accountNonLocked
//            , Collection<? extends GrantedAuthority> authorities
    ) {
        this();

        if (((username == null) || "".equals(username)) || (password == null)) {
            throw new IllegalArgumentException("Cannot pass null or empty values to constructor");
        }

        this.username = username;
        this.password = password;
        super.active = active;
        this.accountNonExpired = accountNonExpired;
        this.credentialsNonExpired = credentialsNonExpired;
        this.accountNonLocked = accountNonLocked;
//        this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));
    }


    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isInternalUser() {
        return internalUser;
    }

    public void setInternalUser(boolean internalUser) {
        this.internalUser = internalUser;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public String getAccountNonExpiredBy() {
        return accountNonExpiredBy;
    }

    public void setAccountNonExpiredBy(String accountNonExpiredBy) {
        this.accountNonExpiredBy = accountNonExpiredBy;
    }

    public Date getAccountNonExpiredDate() {
        return accountNonExpiredDate;
    }

    public void setAccountNonExpiredDate(Date accountNonExpiredDate) {
        this.accountNonExpiredDate = accountNonExpiredDate;
    }

    public String getAccountExpiredBy() {
        return accountExpiredBy;
    }

    public void setAccountExpiredBy(String accountExpiredBy) {
        this.accountExpiredBy = accountExpiredBy;
    }

    public Date getAccountExpiredDate() {
        return accountExpiredDate;
    }

    public void setAccountExpiredDate(Date accountExpiredDate) {
        this.accountExpiredDate = accountExpiredDate;
    }

    public Date getAccountExpiresDate() {
        return accountExpiresDate;
    }

    public void setAccountExpiresDate(Date accountExpiresDate) {
        this.accountExpiresDate = accountExpiresDate;
    }

    public String getAccountExpiresDateSetBy() {
        return accountExpiresDateSetBy;
    }

    public void setAccountExpiresDateSetBy(String accountExpiresDateSetBy) {
        this.accountExpiresDateSetBy = accountExpiresDateSetBy;
    }

    public Date getAccountExpiresDateSetOn() {
        return accountExpiresDateSetOn;
    }

    public void setAccountExpiresDateSetOn(Date accountExpiresDateSetOn) {
        this.accountExpiresDateSetOn = accountExpiresDateSetOn;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public String getAccountNonLockedBy() {
        return accountNonLockedBy;
    }

    public void setAccountNonLockedBy(String accountNonLockedBy) {
        this.accountNonLockedBy = accountNonLockedBy;
    }

    public Date getAccountNonLockedDate() {
        return accountNonLockedDate;
    }

    public void setAccountNonLockedDate(Date accountNonLockedDate) {
        this.accountNonLockedDate = accountNonLockedDate;
    }

    public String getAccountLockedBy() {
        return accountLockedBy;
    }

    public void setAccountLockedBy(String accountLockedBy) {
        this.accountLockedBy = accountLockedBy;
    }

    public Date getAccountLockedDate() {
        return accountLockedDate;
    }

    public void setAccountLockedDate(Date accountLockedDate) {
        this.accountLockedDate = accountLockedDate;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public String getCredentialsNonExpiredBy() {
        return credentialsNonExpiredBy;
    }

    public void setCredentialsNonExpiredBy(String credentialsNonExpiredBy) {
        this.credentialsNonExpiredBy = credentialsNonExpiredBy;
    }

    public Date getCredentialsNonExpiredDate() {
        return credentialsNonExpiredDate;
    }

    public void setCredentialsNonExpiredDate(Date credentialsNonExpiredDate) {
        this.credentialsNonExpiredDate = credentialsNonExpiredDate;
    }

    public String getCredentialsExpiredBy() {
        return credentialsExpiredBy;
    }

    public void setCredentialsExpiredBy(String credentialsExpiredBy) {
        this.credentialsExpiredBy = credentialsExpiredBy;
    }

    public Date getCredentialsExpiredDate() {
        return credentialsExpiredDate;
    }

    public void setCredentialsExpiredDate(Date credentialsExpiredDate) {
        this.credentialsExpiredDate = credentialsExpiredDate;
    }

    public Date getCredentialsExpiresDate() {
        return credentialsExpiresDate;
    }

    public void setCredentialsExpiresDate(Date credentialsExpiresDate) {
        this.credentialsExpiresDate = credentialsExpiresDate;
    }

    public String getCredentialsExpiresDateSetBy() {
        return credentialsExpiresDateSetBy;
    }

    public void setCredentialsExpiresDateSetBy(String credentialsExpiresDateSetBy) {
        this.credentialsExpiresDateSetBy = credentialsExpiresDateSetBy;
    }

    public Date getCredentialsExpiresDateSetOn() {
        return credentialsExpiresDateSetOn;
    }

    public void setCredentialsExpiresDateSetOn(Date credentialsExpiresDateSetOn) {
        this.credentialsExpiresDateSetOn = credentialsExpiresDateSetOn;
    }

    @Override
    public boolean isEnabled() {
        return super.active;
    }


    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    @PrePersist
    public void prePersist() {

//        if (this.roles != null) {
//            if (StringUtils.isEmpty(this.roles.get(0).getId())) {
//                this.roles = null;
//            }
//        }

    }
    @PreUpdate
    public void preUpdate()
    {
        prePersist();
    }

    @PostLoad
    public void postLoad() {

        // Set Status
        if (isDeleted()) {
            this.status.put("message", User.Status.DELETED.statusMessage);
            this.status.put("color", User.Status.DELETED.statusColorCode);

        } else if (!isAccountNonExpired()) {
            this.status.put("message", User.Status.EXPIRED.statusMessage);
            this.status.put("color", User.Status.EXPIRED.statusColorCode);

        } else if (!isCredentialsNonExpired()) {
            this.status.put("message", User.Status.CREDENTIALS_EXPIRED.statusMessage);
            this.status.put("color", User.Status.CREDENTIALS_EXPIRED.statusColorCode);

        } else if (!isAccountNonLocked()) {
            this.status.put("message", User.Status.LOCKED.statusMessage);
            this.status.put("color", User.Status.LOCKED.statusColorCode);

        } else if (!isActive()) {
            this.status.put("message", User.Status.IN_ACTIVE.statusMessage);
            this.status.put("color", User.Status.IN_ACTIVE.statusColorCode);

        } else if (isActive()) {
            this.status.put("message", User.Status.ACTIVE.statusMessage);
            this.status.put("color", User.Status.ACTIVE.statusColorCode);

        } else {
            this.status.put("message", User.Status.UNKNOWN.statusMessage);
            this.status.put("color", User.Status.UNKNOWN.statusColorCode);

        }

    }

    public void eraseCredentials() {
        password = null;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", authorities=" + authorities +
                ", accountNonExpired=" + accountNonExpired +
                ", accountNonExpiredBy=" + accountNonExpiredBy +
                ", accountNonExpiredDate=" + accountNonExpiredDate +
                ", accountExpiredBy=" + accountExpiredBy +
                ", accountExpiredDate=" + accountExpiredDate +
                ", accountExpiresDate=" + accountExpiresDate +
                ", accountExpiresDateSetBy=" + accountExpiresDateSetBy +
                ", accountExpiresDateSetOn=" + accountExpiresDateSetOn +
                ", accountNonLocked=" + accountNonLocked +
                ", accountNonLockedBy=" + accountNonLockedBy +
                ", accountNonLockedDate=" + accountNonLockedDate +
                ", accountLockedBy=" + accountLockedBy +
                ", accountLockedDate=" + accountLockedDate +
                ", credentialsNonExpired=" + credentialsNonExpired +
                ", credentialsNonExpiredBy=" + credentialsNonExpiredBy +
                ", credentialsNonExpiredDate=" + credentialsNonExpiredDate +
                ", credentialsExpiredBy=" + credentialsExpiredBy +
                ", credentialsExpiredDate=" + credentialsExpiredDate +
                ", credentialsExpiresDate=" + credentialsExpiresDate +
                ", credentialsExpiresDateSetBy=" + credentialsExpiresDateSetBy +
                ", credentialsExpiresDateSetOn=" + credentialsExpiresDateSetOn +
                ", roles=" + roles +
                ", profile=" + profile +
                ", contact=" + contact +
                '}';
    }

    /**
     * @author Kingsley Ezenwere
     * @since 0.0.1
     */
    public enum Status {
        ACTIVE("ACTIVE", "success"),
        IN_ACTIVE("IN-ACTIVE", "danger"),
        LOCKED("LOCKED", "danger"),
        DELETED("DELETED", "danger"),
        EXPIRED("ACCOUNT EXPIRED", "danger"),
        PENDING_ACTIVATION("PENDING ACTIVATION", "info"),
        CREDENTIALS_EXPIRED("CREDENTIALS EXPIRED", "danger"),
        MODIFICATION_APPROVED("MODIFICATION APPROVED", "success"),
        MODIFICATION_REJECTED("MODIFICATION REJECTED", "info"),
        MODIFICATION_PENDING_APPROVAL("MODIFICATION PENDING APPROVAL", "info"),
        UNKNOWN("STATUS UNKNOWN", "danger");

        private String statusMessage;
        private String statusColorCode;

        Status(String statusMessage, String statusColorCode) {
            this.statusMessage = statusMessage;
            this.statusColorCode = statusColorCode;
        }

        public String getStatusMessage() {
            return statusMessage;
        }

        public String getStatusColorCode() {
            return statusColorCode;
        }

        @Override
        public String toString() {
            return "Status{" +
                    "statusMessage='" + statusMessage + '\'' +
                    ", statusColorCode='" + statusColorCode + '\'' +
                    '}';
        }
    }
}
