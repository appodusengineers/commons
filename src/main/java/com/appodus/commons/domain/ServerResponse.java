package com.appodus.commons.domain;

import org.springframework.hateoas.ResourceSupport;

/**
 * Created by NanoSoft Solutions on 3/3/2017.
 */
public class ServerResponse extends ResourceSupport {
    private final Object data;

    public ServerResponse(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }
}
