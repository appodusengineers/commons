package com.appodus.commons.domain.miscellaneous.membershipstatus;


import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.organization.Organization;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(name = "membershipstatus")
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"name", "organization_id"})
})
public class MembershipStatus extends AbstractEntity {
    @Column(nullable = false)
    private String name;
    private String description;
    @Column(nullable = false)
    private int ordering;

    public MembershipStatus() {
        super("", "", "");
    }

    public MembershipStatus(String name, String description, int ordering, Organization organization) {
        this();

        this.name = name;
        this.description = description;
        this.ordering = ordering;
        this.organization = organization;
    }
}
