package com.appodus.commons.domain.miscellaneous.gender;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/genders")
public class GenderController {
    private final GenderService genderService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public GenderController(
            GenderService genderService,
            ApplicationEventPublisher eventPublisher) {
        this.genderService = genderService;
        this.eventPublisher = eventPublisher;
    }
    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody Gender gender, final HttpServletRequest request) {
        System.out.println("gender: " + gender);
        Gender _gender = genderService.create(gender);

//        eventPublisher.publishEvent(new OnGenderModifiedEvent(new EventObj("CREATE"), this.getClass()));

        sResponse = new ServerResponse(_gender);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }
    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<Gender> genderList = genderService.getAll();

        sResponse = new ServerResponse(genderList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{genderId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("genderId") String genderId, final HttpServletRequest request) {
        Gender gender = genderService.getById(genderId);

        sResponse = new ServerResponse(gender);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<Gender> genderList = genderService.getAllActive();

        sResponse = new ServerResponse(genderList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<Gender> genderList = genderService.getAllInactive();

        sResponse = new ServerResponse(genderList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-organization")
    public ResponseEntity<ServerResponse> getAllByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Gender> genderList = genderService.getAllByOrganization(organization);

        sResponse = new ServerResponse(genderList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }    @PostMapping("active/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Gender> genderList = genderService.getAllActiveByOrganization(organization);

        sResponse = new ServerResponse(genderList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("inactive/by-organization")
    public ResponseEntity<ServerResponse> getAllInactiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Gender> genderList = genderService.getAllInactiveByOrganization(organization);

        sResponse = new ServerResponse(genderList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Gender> genderList = genderService.getAllDeletedByOrganization(organization);

        sResponse = new ServerResponse(genderList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("non-deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllNonDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<Gender> genderList = genderService.getAllNonDeletedByOrganization(organization);

        sResponse = new ServerResponse(genderList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{genderId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("genderId") String genderId,
            @RequestBody Gender gender,
            final HttpServletRequest request) {

        Gender updatedGender = genderService.update(genderId, gender);

//        eventPublisher.publishEvent(new OnGenderModifiedEvent(new EventObj("UPDATE"), this.getClass()));

        sResponse = new ServerResponse(updatedGender);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{genderId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("genderId") String genderId,
            final HttpServletRequest request) {

        Gender updatedGender = genderService.activate(genderId);

//        eventPublisher.publishEvent(new OnGenderModifiedEvent(new EventObj("ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedGender);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{genderId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("genderId") String genderId,
            final HttpServletRequest request) {

        Gender updatedGender = genderService.inactivate(genderId);

//        eventPublisher.publishEvent(new OnGenderModifiedEvent(new EventObj("IN_ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedGender);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{genderId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("genderId") String genderId,
            final HttpServletRequest request) {

        Gender updatedGender = genderService.delete(genderId);

//        eventPublisher.publishEvent(new OnGenderModifiedEvent(new EventObj("DELETE"), this.getClass()));

        sResponse = new ServerResponse(updatedGender);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{genderId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("genderId") String genderId,
            final HttpServletRequest request) {

        genderService.hardDelete(genderId);

//        eventPublisher.publishEvent(new OnGenderModifiedEvent(new EventObj("HARD_DELETE"), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
