package com.appodus.commons.domain.miscellaneous.marriagetype;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class MarriageTypeService extends AbstractService<MarriageType, MarriageTypeDao> {
    private final MarriageTypeDao marriageTypeDao;
    private final ActiveAuditor activeAuditor;

    @Autowired
    public MarriageTypeService(MarriageTypeDao marriageTypeDao, ActiveAuditor activeAuditor) {
        this.marriageTypeDao = marriageTypeDao;
        this.activeAuditor = activeAuditor;
    }

    public MarriageType update(String marriageTypeId, MarriageType marriageType) {

        MarriageType _marriageType = getById(marriageTypeId);
        _marriageType.setName(marriageType.getName());
        _marriageType.setDescription(marriageType.getDescription());

        return super.update(_marriageType);
    }


    public MarriageType getByNameAndOrganization(String name, Organization organization) {
        return marriageTypeDao.findByNameAndOrganization(name, organization);
    }

    /**
     * Returns List of All MarriageTypes in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MarriageType> getAllByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return marriageTypeDao.findAllByOrganization(_organization);
    }


    /**
     * Returns List of All Active MarriageTypes in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MarriageType> getAllActiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return marriageTypeDao.findAllByOrganizationAndActive(_organization, true);
    }


    /**
     * Returns List of All Inactive MarriageTypes in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MarriageType> getAllInactiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return marriageTypeDao.findAllByOrganizationAndActive(_organization, false);
    }


    /**
     * Returns List of All Deleted MarriageTypes in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MarriageType> getAllDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return marriageTypeDao.findAllByOrganizationAndDeleted(_organization, true);
    }


    /**
     * Returns List of All Non Deleted MarriageTypes in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MarriageType> getAllNonDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return marriageTypeDao.findAllByOrganizationAndDeleted(_organization, false);
    }

    public void initOrganizationMarriageTypes(Organization organization) {
        if (getAllByOrganization(organization).size() < 1) {
            super.createAll(Arrays.asList(
                    new MarriageType("Civil marriage", "", organization),
                    new MarriageType("Customary marriage", "", organization),
                    new MarriageType("Traditional Marriage", "", organization),
                    new MarriageType("Religious Marriage", "", organization),
                    new MarriageType("Remarriage", "", organization)
            ));
        }
    }





}
