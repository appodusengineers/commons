package com.appodus.commons.domain.miscellaneous.membershipstatus;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class MembershipStatusService extends AbstractService<MembershipStatus, MembershipStatusDao> {
    private final MembershipStatusDao membershipStatusDao;
    private final ActiveAuditor activeAuditor;

    @Autowired
    public MembershipStatusService(MembershipStatusDao membershipStatusDao, ActiveAuditor activeAuditor) {
        this.membershipStatusDao = membershipStatusDao;
        this.activeAuditor = activeAuditor;
    }

    public MembershipStatus update(String genderId, MembershipStatus membershipStatus) {

        MembershipStatus _membershipStatus = getById(genderId);
        _membershipStatus.setName(membershipStatus.getName());
        _membershipStatus.setDescription(membershipStatus.getDescription());

        return super.update(_membershipStatus);
    }

    public MembershipStatus getByNameAndOrganization(String name, Organization organization) {
        return membershipStatusDao.findByNameAndOrganization(name, organization);
    }

    /**
     * Returns List of All MembershipStatuses in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MembershipStatus> getAllByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return membershipStatusDao.findAllByOrganization(_organization);
    }


    /**
     * Returns List of All Active MembershipStatuses in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MembershipStatus> getAllActiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return membershipStatusDao.findAllByOrganizationAndActive(_organization, true);
    }


    /**
     * Returns List of All Inactive MembershipStatuses in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MembershipStatus> getAllInactiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return membershipStatusDao.findAllByOrganizationAndActive(_organization, false);
    }


    /**
     * Returns List of All Deleted MembershipStatuses in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MembershipStatus> getAllDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return membershipStatusDao.findAllByOrganizationAndDeleted(_organization, true);
    }


    /**
     * Returns List of All Non Deleted MembershipStatuses in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MembershipStatus> getAllNonDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return membershipStatusDao.findAllByOrganizationAndDeleted(_organization, false);
    }

    public void initOrganizationMembershipStatuses(Organization organization) {
        if (getAllByOrganization(organization).size() < 1) {
            super.createAll(Arrays.asList(
                    new MembershipStatus("Member", "JANUARY", 1, organization),
                    new MembershipStatus("Worshipper","FEBRUARY", 2, organization),
                    new MembershipStatus("Visitor","MATCH", 3, organization)
            ));
        }
    }
}
