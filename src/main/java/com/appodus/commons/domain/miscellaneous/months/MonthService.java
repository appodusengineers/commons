package com.appodus.commons.domain.miscellaneous.months;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.organization.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class MonthService extends AbstractService<Month, MonthDao> {
    private final MonthDao monthDao;

    @Autowired
    public MonthService(MonthDao monthDao) {
        this.monthDao = monthDao;
    }

    public Month update(String genderId, Month month) {

        Month _month = getById(genderId);
        _month.setName(month.getName());
        _month.setNumber(month.getNumber());

        return super.update(_month);
    }

    public Month getByName(String name) {
        return monthDao.findByName(name);
    }

    public Month getByNumber(int number) {
        return monthDao.findByNumber(number);
    }

    public Month getByShortName(String shortName) {
        return monthDao.findByShortName(shortName);
    }
//
//    public Month getByNameAndOrganization(String name, Organization organization) {
//        return monthDao.findByNameAndOrganization(name, organization);
//    }
//
//    public Month getByNumberAndOrganization(int number, Organization organization) {
//        return monthDao.findByNumberAndOrganization(number, organization);
//    }
//
//    public Month getByShortNameAndOrganization(String shortName, Organization organization) {
//        return monthDao.findByShortNameAndOrganization(shortName, organization);
//    }
//
//    public List<Month> getAllActiveByOrganization(Organization organization) {
//        return monthDao.findByOrganization(organization)
//                .stream()
//                .filter(month -> month.isActive()).collect(Collectors.toList());
//    }


    @PostConstruct
    public void init() {
        if (super.count() < 1) {
            super.createAll(Arrays.asList(

                    new Month(1, "JAN", "JANUARY"),
                    new Month(2, "FEB", "FEBRUARY"),
                    new Month(3, "MATCH", "MATCH"),
                    new Month(4, "APR", "APRIL"),
                    new Month(5, "MAY", "MAY"),
                    new Month(6, "JUN", "JUNE"),
                    new Month(7, "JUL", "JULY"),
                    new Month(8, "AUG", "AUGUST"),
                    new Month(9, "SEP", "SEPTEMBER"),
                    new Month(10, "OCT", "OCTOBER"),
                    new Month(11, "NOV", "NOVEMBER"),
                    new Month(12, "DEC", "DECEMBER")
            ));
        }
    }
}
