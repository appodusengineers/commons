package com.appodus.commons.domain.miscellaneous.marriagetype;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/marriageTypes")
public class MarriageTypeController {
    private final MarriageTypeService marriageTypeService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public MarriageTypeController(
            MarriageTypeService marriageTypeService,
            ApplicationEventPublisher eventPublisher) {
        this.marriageTypeService = marriageTypeService;
        this.eventPublisher = eventPublisher;
    }
    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody MarriageType marriageType, final HttpServletRequest request) {
        System.out.println("marriageType: " + marriageType);
        MarriageType _marriageType = marriageTypeService.create(marriageType);

//        eventPublisher.publishEvent(new OnMarriageTypeModifiedEvent(new EventObj("CREATE"), this.getClass()));

        sResponse = new ServerResponse(_marriageType);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }
    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<MarriageType> marriageTypeList = marriageTypeService.getAll();

        sResponse = new ServerResponse(marriageTypeList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
        @GetMapping("{marriageTypeId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("marriageTypeId") String marriageTypeId, final HttpServletRequest request) {
        MarriageType marriageType = marriageTypeService.getById(marriageTypeId);

        sResponse = new ServerResponse(marriageType);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<MarriageType> marriageTypeList = marriageTypeService.getAllActive();

        sResponse = new ServerResponse(marriageTypeList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<MarriageType> marriageTypeList = marriageTypeService.getAllInactive();

        sResponse = new ServerResponse(marriageTypeList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-organization")
    public ResponseEntity<ServerResponse> getAllByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MarriageType> marriageTypeList = marriageTypeService.getAllByOrganization(organization);

        sResponse = new ServerResponse(marriageTypeList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }    @PostMapping("active/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MarriageType> marriageTypeList = marriageTypeService.getAllActiveByOrganization(organization);

        sResponse = new ServerResponse(marriageTypeList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("inactive/by-organization")
    public ResponseEntity<ServerResponse> getAllInactiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MarriageType> marriageTypeList = marriageTypeService.getAllInactiveByOrganization(organization);

        sResponse = new ServerResponse(marriageTypeList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MarriageType> marriageTypeList = marriageTypeService.getAllDeletedByOrganization(organization);

        sResponse = new ServerResponse(marriageTypeList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("non-deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllNonDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MarriageType> marriageTypeList = marriageTypeService.getAllNonDeletedByOrganization(organization);

        sResponse = new ServerResponse(marriageTypeList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{marriageTypeId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("marriageTypeId") String marriageTypeId,
            @RequestBody MarriageType marriageType,
            final HttpServletRequest request) {

        MarriageType updatedMarriageType = marriageTypeService.update(marriageTypeId, marriageType);

//        eventPublisher.publishEvent(new OnMarriageTypeModifiedEvent(new EventObj("UPDATE"), this.getClass()));

        sResponse = new ServerResponse(updatedMarriageType);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{marriageTypeId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("marriageTypeId") String marriageTypeId,
            final HttpServletRequest request) {

        MarriageType updatedMarriageType = marriageTypeService.activate(marriageTypeId);

//        eventPublisher.publishEvent(new OnMarriageTypeModifiedEvent(new EventObj("ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedMarriageType);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{marriageTypeId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("marriageTypeId") String marriageTypeId,
            final HttpServletRequest request) {

        MarriageType updatedMarriageType = marriageTypeService.inactivate(marriageTypeId);

//        eventPublisher.publishEvent(new OnMarriageTypeModifiedEvent(new EventObj("IN_ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedMarriageType);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{marriageTypeId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("marriageTypeId") String marriageTypeId,
            final HttpServletRequest request) {

        MarriageType updatedMarriageType = marriageTypeService.delete(marriageTypeId);

//        eventPublisher.publishEvent(new OnMarriageTypeModifiedEvent(new EventObj("DELETE"), this.getClass()));

        sResponse = new ServerResponse(updatedMarriageType);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{marriageTypeId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("marriageTypeId") String marriageTypeId,
            final HttpServletRequest request) {

        marriageTypeService.hardDelete(marriageTypeId);

//        eventPublisher.publishEvent(new OnMarriageTypeModifiedEvent(new EventObj("HARD_DELETE"), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
