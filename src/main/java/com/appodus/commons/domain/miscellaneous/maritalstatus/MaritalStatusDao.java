package com.appodus.commons.domain.miscellaneous.maritalstatus;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.miscellaneous.gender.Gender;
import com.appodus.commons.domain.organization.Organization;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface MaritalStatusDao extends AbstractDao<MaritalStatus> {
    MaritalStatus findByNameAndOrganization(String name, Organization organization);

    List<MaritalStatus> findAllByOrganization(Organization organization);
    List<MaritalStatus> findAllByOrganizationAndActive(Organization organization, boolean isActive);
    List<MaritalStatus> findAllByOrganizationAndDeleted(Organization organization, boolean isDeleted);
//    @Query("SELECT d FROM MaritalStatus d WHERE d.active = true ORDER BY d.createdDate DESC")
//    List<MaritalStatus> finAllActive();
//
//    @Query("SELECT d FROM MaritalStatus d WHERE d.active = false ORDER BY d.createdDate DESC")
//    List<MaritalStatus> findAllInactive();
}
