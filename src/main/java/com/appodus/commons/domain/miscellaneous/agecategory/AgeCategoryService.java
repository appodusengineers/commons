package com.appodus.commons.domain.miscellaneous.agecategory;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class AgeCategoryService extends AbstractService<AgeCategory, AgeCategoryDao> {
    private final AgeCategoryDao ageCategoryDao;
    private final ActiveAuditor activeAuditor;

    @Autowired
    public AgeCategoryService(AgeCategoryDao ageCategoryDao, ActiveAuditor activeAuditor) {
        this.ageCategoryDao = ageCategoryDao;
        this.activeAuditor = activeAuditor;
    }

    public AgeCategory update(String designationId, AgeCategory ageCategory) {

        AgeCategory _ageCategory = getById(designationId);
        _ageCategory.setName(ageCategory.getName());
        _ageCategory.setDescription(ageCategory.getDescription());

        return super.update(_ageCategory);
    }


    public AgeCategory getByNameAndOrganization(String name, Organization organization) {
        return ageCategoryDao.findByNameAndOrganization(name, organization);
    }


    public AgeCategory getBySystemNameAndOrganization(String SystemName, Organization organization) {
        return ageCategoryDao.findBySystemNameAndOrganization(SystemName, organization);
    }

    /**
     * Returns List of All AgeCategories in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<AgeCategory> getAllByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return ageCategoryDao.findAllByOrganization(_organization);
    }


    /**
     * Returns List of All Active AgeCategories in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<AgeCategory> getAllActiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return ageCategoryDao.findAllByOrganizationAndActive(_organization, true);
    }


    /**
     * Returns List of All Inactive AgeCategories in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<AgeCategory> getAllInactiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return ageCategoryDao.findAllByOrganizationAndActive(_organization, false);
    }


    /**
     * Returns List of All Deleted AgeCategories in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<AgeCategory> getAllDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return ageCategoryDao.findAllByOrganizationAndDeleted(_organization, true);
    }


    /**
     * Returns List of All Non Deleted AgeCategories in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<AgeCategory> getAllNonDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return ageCategoryDao.findAllByOrganizationAndDeleted(_organization, false);
    }

    public void initOrganizationAgeCategories(Organization organization) {
        if (getAllByOrganization(organization).size() < 1) {
            super.createAll(Arrays.asList(
                    new AgeCategory("Infant", "Between zero to two years old", 0, 1, "(0-1)", organization),
                    new AgeCategory("Toddler", "Between two to three years old", 2, 3, "(2-3)", organization),
                    new AgeCategory("Early school age", "Between four to six years old", 4, 6, "(4-6)", organization),
                    new AgeCategory("Middle childhood", "Between six to twelve years old", 6, 12, "(6-12)", organization),
                    new AgeCategory("Teenager", "Between thirteen to nineteen years old", 13, 19, "(13-19)", organization),
                    new AgeCategory("Youth", "Between nineteen to thirty three years old", 19, 35, "(19-35)", organization),
                    new AgeCategory("Adult", "Between thirty six to fifty five years old", 36, 55, "(36-55)", organization),
                    new AgeCategory("Older adult", "Between fifty six to seventy five years old", 56, 75, "(56-75)", organization),
                    new AgeCategory("Very old age", "Above seventy five years old", 75, 120, "(75+)", organization)
            ));
        }
    }
}







