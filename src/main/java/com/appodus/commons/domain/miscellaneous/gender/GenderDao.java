package com.appodus.commons.domain.miscellaneous.gender;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.miscellaneous.agecategory.AgeCategory;
import com.appodus.commons.domain.organization.Organization;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface GenderDao extends AbstractDao<Gender> {
    Gender findByNameAndOrganization(String name, Organization organization);

    List<Gender> findAllByOrganization(Organization organization);
    List<Gender> findAllByOrganizationAndActive(Organization organization, boolean isActive);
    List<Gender> findAllByOrganizationAndDeleted(Organization organization, boolean isDeleted);
//    @Query("SELECT d FROM Gender d WHERE d.active = true ORDER BY d.createdDate DESC")
//    List<Gender> finAllActive();
//
//    @Query("SELECT d FROM Gender d WHERE d.active = false ORDER BY d.createdDate DESC")
//    List<Gender> findAllInactive();

}
