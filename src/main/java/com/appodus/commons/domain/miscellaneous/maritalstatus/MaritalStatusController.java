package com.appodus.commons.domain.miscellaneous.maritalstatus;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/maritalStatuses")
public class MaritalStatusController {
    private final MaritalStatusService maritalStatusService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public MaritalStatusController(
            MaritalStatusService maritalStatusService,
            ApplicationEventPublisher eventPublisher) {
        this.maritalStatusService = maritalStatusService;
        this.eventPublisher = eventPublisher;
    }
    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody MaritalStatus maritalStatus, final HttpServletRequest request) {
        System.out.println("maritalStatus: " + maritalStatus);
        MaritalStatus _maritalStatus = maritalStatusService.create(maritalStatus);

//        eventPublisher.publishEvent(new OnMaritalStatusModifiedEvent(new EventObj("CREATE"), this.getClass()));

        sResponse = new ServerResponse(_maritalStatus);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }
    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<MaritalStatus> maritalStatusList = maritalStatusService.getAll();

        sResponse = new ServerResponse(maritalStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{maritalStatusId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("maritalStatusId") String maritalStatusId, final HttpServletRequest request) {
        MaritalStatus maritalStatus = maritalStatusService.getById(maritalStatusId);

        sResponse = new ServerResponse(maritalStatus);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<MaritalStatus> maritalStatusList = maritalStatusService.getAllActive();

        sResponse = new ServerResponse(maritalStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<MaritalStatus> maritalStatusList = maritalStatusService.getAllInactive();

        sResponse = new ServerResponse(maritalStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-organization")
    public ResponseEntity<ServerResponse> getAllByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MaritalStatus> maritalStatusList = maritalStatusService.getAllByOrganization(organization);

        sResponse = new ServerResponse(maritalStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }    @PostMapping("active/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MaritalStatus> maritalStatusList = maritalStatusService.getAllActiveByOrganization(organization);

        sResponse = new ServerResponse(maritalStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("inactive/by-organization")
    public ResponseEntity<ServerResponse> getAllInactiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MaritalStatus> maritalStatusList = maritalStatusService.getAllInactiveByOrganization(organization);

        sResponse = new ServerResponse(maritalStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MaritalStatus> maritalStatusList = maritalStatusService.getAllDeletedByOrganization(organization);

        sResponse = new ServerResponse(maritalStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("non-deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllNonDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MaritalStatus> maritalStatusList = maritalStatusService.getAllNonDeletedByOrganization(organization);

        sResponse = new ServerResponse(maritalStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{maritalStatusId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("maritalStatusId") String maritalStatusId,
            @RequestBody MaritalStatus maritalStatus,
            final HttpServletRequest request) {

        MaritalStatus updatedMaritalStatus = maritalStatusService.update(maritalStatusId, maritalStatus);

//        eventPublisher.publishEvent(new OnMaritalStatusModifiedEvent(new EventObj("UPDATE"), this.getClass()));

        sResponse = new ServerResponse(updatedMaritalStatus);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{maritalStatusId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("maritalStatusId") String maritalStatusId,
            final HttpServletRequest request) {

        MaritalStatus updatedMaritalStatus = maritalStatusService.activate(maritalStatusId);

//        eventPublisher.publishEvent(new OnMaritalStatusModifiedEvent(new EventObj("ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedMaritalStatus);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{maritalStatusId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("maritalStatusId") String maritalStatusId,
            final HttpServletRequest request) {

        MaritalStatus updatedMaritalStatus = maritalStatusService.inactivate(maritalStatusId);

//        eventPublisher.publishEvent(new OnMaritalStatusModifiedEvent(new EventObj("IN_ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedMaritalStatus);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{maritalStatusId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("maritalStatusId") String maritalStatusId,
            final HttpServletRequest request) {

        MaritalStatus updatedMaritalStatus = maritalStatusService.delete(maritalStatusId);

//        eventPublisher.publishEvent(new OnMaritalStatusModifiedEvent(new EventObj("DELETE"), this.getClass()));

        sResponse = new ServerResponse(updatedMaritalStatus);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{maritalStatusId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("maritalStatusId") String maritalStatusId,
            final HttpServletRequest request) {

        maritalStatusService.hardDelete(maritalStatusId);

//        eventPublisher.publishEvent(new OnMaritalStatusModifiedEvent(new EventObj("HARD_DELETE"), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
