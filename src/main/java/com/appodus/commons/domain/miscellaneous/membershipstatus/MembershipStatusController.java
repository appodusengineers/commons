package com.appodus.commons.domain.miscellaneous.membershipstatus;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/membershipStatuses")
public class MembershipStatusController {
    private final MembershipStatusService membershipStatusService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public MembershipStatusController(
            MembershipStatusService membershipStatusService,
            ApplicationEventPublisher eventPublisher) {
        this.membershipStatusService = membershipStatusService;
        this.eventPublisher = eventPublisher;
    }
    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody MembershipStatus membershipStatus, final HttpServletRequest request) {
        System.out.println("membershipStatus: " + membershipStatus);
        MembershipStatus _membershipStatus = membershipStatusService.create(membershipStatus);

//        eventPublisher.publishEvent(new OnMembershipStatusModifiedEvent(new EventObj("CREATE"), this.getClass()));

        sResponse = new ServerResponse(_membershipStatus);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }
    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<MembershipStatus> membershipStatusList = membershipStatusService.getAll();

        sResponse = new ServerResponse(membershipStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{membershipStatusId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("membershipStatusId") String membershipStatusId, final HttpServletRequest request) {
        MembershipStatus membershipStatus = membershipStatusService.getById(membershipStatusId);

        sResponse = new ServerResponse(membershipStatus);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<MembershipStatus> membershipStatusList = membershipStatusService.getAllActive();

        sResponse = new ServerResponse(membershipStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<MembershipStatus> membershipStatusList = membershipStatusService.getAllInactive();

        sResponse = new ServerResponse(membershipStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-organization")
    public ResponseEntity<ServerResponse> getAllByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MembershipStatus> membershipStatusList = membershipStatusService.getAllByOrganization(organization);

        sResponse = new ServerResponse(membershipStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }    @PostMapping("active/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MembershipStatus> membershipStatusList = membershipStatusService.getAllActiveByOrganization(organization);

        sResponse = new ServerResponse(membershipStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("inactive/by-organization")
    public ResponseEntity<ServerResponse> getAllInactiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MembershipStatus> membershipStatusList = membershipStatusService.getAllInactiveByOrganization(organization);

        sResponse = new ServerResponse(membershipStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MembershipStatus> membershipStatusList = membershipStatusService.getAllDeletedByOrganization(organization);

        sResponse = new ServerResponse(membershipStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("non-deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllNonDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<MembershipStatus> membershipStatusList = membershipStatusService.getAllNonDeletedByOrganization(organization);

        sResponse = new ServerResponse(membershipStatusList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{membershipStatusId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("membershipStatusId") String membershipStatusId,
            @RequestBody MembershipStatus membershipStatus,
            final HttpServletRequest request) {

        MembershipStatus updatedMembershipStatus = membershipStatusService.update(membershipStatusId, membershipStatus);

//        eventPublisher.publishEvent(new OnMembershipStatusModifiedEvent(new EventObj("UPDATE"), this.getClass()));

        sResponse = new ServerResponse(updatedMembershipStatus);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{membershipStatusId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("membershipStatusId") String membershipStatusId,
            final HttpServletRequest request) {

        MembershipStatus updatedMembershipStatus = membershipStatusService.activate(membershipStatusId);

//        eventPublisher.publishEvent(new OnMembershipStatusModifiedEvent(new EventObj("ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedMembershipStatus);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{membershipStatusId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("membershipStatusId") String membershipStatusId,
            final HttpServletRequest request) {

        MembershipStatus updatedMembershipStatus = membershipStatusService.inactivate(membershipStatusId);

//        eventPublisher.publishEvent(new OnMembershipStatusModifiedEvent(new EventObj("IN_ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedMembershipStatus);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{membershipStatusId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("membershipStatusId") String membershipStatusId,
            final HttpServletRequest request) {

        MembershipStatus updatedMembershipStatus = membershipStatusService.delete(membershipStatusId);

//        eventPublisher.publishEvent(new OnMembershipStatusModifiedEvent(new EventObj("DELETE"), this.getClass()));

        sResponse = new ServerResponse(updatedMembershipStatus);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{membershipStatusId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("membershipStatusId") String membershipStatusId,
            final HttpServletRequest request) {

        membershipStatusService.hardDelete(membershipStatusId);

//        eventPublisher.publishEvent(new OnMembershipStatusModifiedEvent(new EventObj("HARD_DELETE"), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
