package com.appodus.commons.domain.miscellaneous.months;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/months")
public class MonthController {
    private final MonthService monthService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public MonthController(
            MonthService monthService,
            ApplicationEventPublisher eventPublisher) {
        this.monthService = monthService;
        this.eventPublisher = eventPublisher;
    }
    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody Month month, final HttpServletRequest request) {
        System.out.println("month: " + month);
        Month _month = monthService.create(month);

//        eventPublisher.publishEvent(new OnMonthModifiedEvent(new EventObj("CREATE"), this.getClass()));

        sResponse = new ServerResponse(_month);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }
    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<Month> monthList = monthService.getAll();

        sResponse = new ServerResponse(monthList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{monthId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("monthId") String monthId, final HttpServletRequest request) {
        Month month = monthService.getById(monthId);

        sResponse = new ServerResponse(month);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{monthId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("monthId") String monthId,
            @RequestBody Month month,
            final HttpServletRequest request) {

        Month updatedMonth = monthService.update(monthId, month);

//        eventPublisher.publishEvent(new OnMonthModifiedEvent(new EventObj("UPDATE"), this.getClass()));

        sResponse = new ServerResponse(updatedMonth);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

}
