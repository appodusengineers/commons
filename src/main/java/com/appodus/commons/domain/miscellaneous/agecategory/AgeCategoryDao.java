package com.appodus.commons.domain.miscellaneous.agecategory;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.organization.Organization;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface AgeCategoryDao extends AbstractDao<AgeCategory> {

    AgeCategory findByNameAndOrganization(String name, Organization organization);
    AgeCategory findBySystemNameAndOrganization(String systemName, Organization organization);

    List<AgeCategory> findAllByOrganization(Organization organization);
    List<AgeCategory> findAllByOrganizationAndActive(Organization organization, boolean isActive);
    List<AgeCategory> findAllByOrganizationAndDeleted(Organization organization, boolean isDeleted);
//    @Query("SELECT d FROM Designation d WHERE d.active = true ORDER BY d.createdDate DESC")
//    List<Designation> finAllActive();
//
//    @Query("SELECT d FROM Designation d WHERE d.active = false ORDER BY d.createdDate DESC")
//    List<Designation> findAllInactive();
}
