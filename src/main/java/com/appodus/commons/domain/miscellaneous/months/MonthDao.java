package com.appodus.commons.domain.miscellaneous.months;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.organization.Organization;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface MonthDao extends AbstractDao<Month> {
    Month findByNumber(int number);

    Month findByName(String name);

    Month findByShortName(String shortName);
//    Month findByNumberAndOrganization(int number, Organization organization);
//
//    Month findByNameAndOrganization(String name, Organization organization);
//
//    Month findByShortNameAndOrganization(String shortName, Organization organization);
//
//    List<Month> findByOrganization(Organization organization);
//    @Query("SELECT d FROM Gender d WHERE d.active = true ORDER BY d.createdDate DESC")
//    List<Gender> finAllActive();
//
//    @Query("SELECT d FROM Gender d WHERE d.active = false ORDER BY d.createdDate DESC")
//    List<Gender> findAllInactive();
}
