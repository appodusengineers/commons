package com.appodus.commons.domain.miscellaneous.agecategory;


import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.organization.Organization;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(name = "agecategory")
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"name", "organization_id"})
})
public class AgeCategory extends AbstractEntity {
    @Column(nullable = false)
    private String name;
    private String description;
    @Column(nullable = false)
    private int min;
    @Column(nullable = false)
    private int max;
    private String systemName;

    public AgeCategory() {
        super("", "", "");
    }

    public AgeCategory(String name, String description, int min, int max, String systemName, Organization organization) {
        this();

        this.name = name;
        this.description = description;
        this.min = min;
        this.max = max;
        this.organization = organization;
        this.systemName = systemName;
    }
}
