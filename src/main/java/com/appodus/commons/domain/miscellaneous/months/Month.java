package com.appodus.commons.domain.miscellaneous.months;


import com.appodus.commons.domain.abstractentity.AbstractEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Month extends AbstractEntity {
    @Column(nullable = false, unique = true)
    private int number;
    @Column(nullable = false, unique = true)
    private String shortName;
    @Column(nullable = false, unique = true)
    private String name;

    public Month() {
        super();
    }

    public Month(int number, String name) {
        this();

        this.number = number;
        this.name = name;
    }

    public Month(int number, String shortName, String name) {
        this(number, name);

        this.shortName = shortName;
    }
}
