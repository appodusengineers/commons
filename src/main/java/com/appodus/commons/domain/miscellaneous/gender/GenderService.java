package com.appodus.commons.domain.miscellaneous.gender;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class GenderService extends AbstractService<Gender, GenderDao> {
    private final GenderDao genderDao;
    private final ActiveAuditor activeAuditor;

    @Autowired
    public GenderService(GenderDao genderDao, ActiveAuditor activeAuditor) {
        this.genderDao = genderDao;
        this.activeAuditor = activeAuditor;
    }

    public Gender update(String genderId, Gender gender) {

        Gender _gender = getById(genderId);
        _gender.setName(gender.getName());
        _gender.setDescription(gender.getDescription());

        return super.update(_gender);
    }

    public Gender getByNameAndOrganization(String name, Organization organization) {
        return genderDao.findByNameAndOrganization(name, organization);
    }

    /**
     * Returns List of All Genders in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Gender> getAllByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return genderDao.findAllByOrganization(_organization);
    }


    /**
     * Returns List of All Active Genders in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Gender> getAllActiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return genderDao.findAllByOrganizationAndActive(_organization, true);
    }


    /**
     * Returns List of All Inactive Genders in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Gender> getAllInactiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return genderDao.findAllByOrganizationAndActive(_organization, false);
    }


    /**
     * Returns List of All Deleted Genders in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Gender> getAllDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return genderDao.findAllByOrganizationAndDeleted(_organization, true);
    }


    /**
     * Returns List of All Non Deleted Genders in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<Gender> getAllNonDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return genderDao.findAllByOrganizationAndDeleted(_organization, false);
    }

    public void initOrganizationGenders(Organization organization) {
        if (getAllByOrganization(organization).size() < 1) {
            super.createAll(Arrays.asList(
                    new Gender("Female", "Woman", organization),
                    new Gender("Male", "Man", organization)
            ));
        }
    }
}
