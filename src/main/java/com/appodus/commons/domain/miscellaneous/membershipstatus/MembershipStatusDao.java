package com.appodus.commons.domain.miscellaneous.membershipstatus;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.miscellaneous.marriagetype.MarriageType;
import com.appodus.commons.domain.organization.Organization;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface MembershipStatusDao extends AbstractDao<MembershipStatus> {
    MembershipStatus findByNameAndOrganization(String name, Organization organization);

    List<MembershipStatus> findAllByOrganization(Organization organization);
    List<MembershipStatus> findAllByOrganizationAndActive(Organization organization, boolean isActive);
    List<MembershipStatus> findAllByOrganizationAndDeleted(Organization organization, boolean isDeleted);
//    @Query("SELECT d FROM Gender d WHERE d.active = true ORDER BY d.createdDate DESC")
//    List<Gender> finAllActive();
//
//    @Query("SELECT d FROM Gender d WHERE d.active = false ORDER BY d.createdDate DESC")
//    List<Gender> findAllInactive();
}
