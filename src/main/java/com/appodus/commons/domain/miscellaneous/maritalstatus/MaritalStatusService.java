package com.appodus.commons.domain.miscellaneous.maritalstatus;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class MaritalStatusService extends AbstractService<MaritalStatus, MaritalStatusDao> {
    private final MaritalStatusDao maritalStatusDao;
    private final ActiveAuditor activeAuditor;

    @Autowired
    public MaritalStatusService(MaritalStatusDao maritalStatusDao, ActiveAuditor activeAuditor) {
        this.maritalStatusDao = maritalStatusDao;
        this.activeAuditor = activeAuditor;
    }

    public MaritalStatus update(String maritalStatusId, MaritalStatus maritalStatus) {

        MaritalStatus _maritalStatus = getById(maritalStatusId);
        _maritalStatus.setName(maritalStatus.getName());
        _maritalStatus.setDescription(maritalStatus.getDescription());

        return super.update(_maritalStatus);
    }

    public MaritalStatus getByNameAndOrganization(String name, Organization organization) {
        return maritalStatusDao.findByNameAndOrganization(name, organization);
    }

    /**
     * Returns List of All MaritalStatuses in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MaritalStatus> getAllByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return maritalStatusDao.findAllByOrganization(_organization);
    }


    /**
     * Returns List of All Active MaritalStatuses in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MaritalStatus> getAllActiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return maritalStatusDao.findAllByOrganizationAndActive(_organization, true);
    }


    /**
     * Returns List of All Inactive MaritalStatuses in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MaritalStatus> getAllInactiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return maritalStatusDao.findAllByOrganizationAndActive(_organization, false);
    }


    /**
     * Returns List of All Deleted MaritalStatuses in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MaritalStatus> getAllDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return maritalStatusDao.findAllByOrganizationAndDeleted(_organization, true);
    }


    /**
     * Returns List of All Non Deleted MaritalStatuses in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<MaritalStatus> getAllNonDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return maritalStatusDao.findAllByOrganizationAndDeleted(_organization, false);
    }

    public void initOrganizationMaritalStatuses(Organization organization) {
        if (getAllByOrganization(organization).size() < 1) {
            super.createAll(Arrays.asList(
                    new MaritalStatus("Single", "Never married", organization),
                    new MaritalStatus("Married", "In a marriage", organization),
                    new MaritalStatus("Divorced", "Married, but separated", organization),
                    new MaritalStatus("Widowed", "Married, but spouse died", organization)
            ));
        }
    }

}
