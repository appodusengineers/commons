package com.appodus.commons.domain.miscellaneous.maritalstatus;


import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.organization.Organization;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.*;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(name = "maritalstatus")
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"name", "organization_id"})
})
public class MaritalStatus extends AbstractEntity {
    @Column(nullable = false)
    private String name;
    private String description;

    public MaritalStatus() {
        super("", "", "");
    }

    public MaritalStatus(String name, String description, Organization organization) {
        this();

        this.name = name;
        this.description = description;
        this.organization = organization;
    }
}
