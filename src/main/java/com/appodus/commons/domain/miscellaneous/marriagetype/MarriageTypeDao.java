package com.appodus.commons.domain.miscellaneous.marriagetype;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.miscellaneous.maritalstatus.MaritalStatus;
import com.appodus.commons.domain.organization.Organization;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface MarriageTypeDao extends AbstractDao<MarriageType> {
    MarriageType findByNameAndOrganization(String name, Organization organization);

    List<MarriageType> findAllByOrganization(Organization organization);
    List<MarriageType> findAllByOrganizationAndActive(Organization organization, boolean isActive);
    List<MarriageType> findAllByOrganizationAndDeleted(Organization organization, boolean isDeleted);
//    @Query("SELECT d FROM MarriageType d WHERE d.active = true ORDER BY d.createdDate DESC")
//    List<MarriageType> finAllActive();
//
//    @Query("SELECT d FROM MarriageType d WHERE d.active = false ORDER BY d.createdDate DESC")
//    List<MarriageType> findAllInactive();
}
