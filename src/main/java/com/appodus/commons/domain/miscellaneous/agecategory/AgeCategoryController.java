package com.appodus.commons.domain.miscellaneous.agecategory;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/ageCategories")
public class AgeCategoryController {
    private final AgeCategoryService ageCategoryService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public AgeCategoryController(
            AgeCategoryService ageCategoryService,
            ApplicationEventPublisher eventPublisher) {
        this.ageCategoryService = ageCategoryService;
        this.eventPublisher = eventPublisher;
    }
    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody AgeCategory ageCategory, final HttpServletRequest request) {
        System.out.println("ageCategory: " + ageCategory);
        AgeCategory _ageCategory = ageCategoryService.create(ageCategory);

//        eventPublisher.publishEvent(new OnAgeCategoryModifiedEvent(new EventObj("CREATE"), this.getClass()));

        sResponse = new ServerResponse(_ageCategory);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }
    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<AgeCategory> ageCategoryList = ageCategoryService.getAll();

        sResponse = new ServerResponse(ageCategoryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<AgeCategory> ageCategoryList = ageCategoryService.getAllActive();

        sResponse = new ServerResponse(ageCategoryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<AgeCategory> ageCategoryList = ageCategoryService.getAllInactive();

        sResponse = new ServerResponse(ageCategoryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("{ageCategoryId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("ageCategoryId") String ageCategoryId, final HttpServletRequest request) {
        AgeCategory ageCategory = ageCategoryService.getById(ageCategoryId);

        sResponse = new ServerResponse(ageCategory);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-organization")
    public ResponseEntity<ServerResponse> getAllByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<AgeCategory> ageCategoryList = ageCategoryService.getAllByOrganization(organization);

        sResponse = new ServerResponse(ageCategoryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }    @PostMapping("active/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<AgeCategory> ageCategoryList = ageCategoryService.getAllActiveByOrganization(organization);

        sResponse = new ServerResponse(ageCategoryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("inactive/by-organization")
    public ResponseEntity<ServerResponse> getAllInactiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<AgeCategory> ageCategoryList = ageCategoryService.getAllInactiveByOrganization(organization);

        sResponse = new ServerResponse(ageCategoryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<AgeCategory> ageCategoryList = ageCategoryService.getAllDeletedByOrganization(organization);

        sResponse = new ServerResponse(ageCategoryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("non-deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllNonDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<AgeCategory> ageCategoryList = ageCategoryService.getAllNonDeletedByOrganization(organization);

        sResponse = new ServerResponse(ageCategoryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{ageCategoryId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("ageCategoryId") String ageCategoryId,
            @RequestBody AgeCategory ageCategory,
            final HttpServletRequest request) {

        AgeCategory updatedAgeCategory = ageCategoryService.update(ageCategoryId, ageCategory);

//        eventPublisher.publishEvent(new OnAgeCategoryModifiedEvent(new EventObj("UPDATE"), this.getClass()));

        sResponse = new ServerResponse(updatedAgeCategory);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{ageCategoryId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("ageCategoryId") String ageCategoryId,
            final HttpServletRequest request) {

        AgeCategory updatedAgeCategory = ageCategoryService.activate(ageCategoryId);

//        eventPublisher.publishEvent(new OnAgeCategoryModifiedEvent(new EventObj("ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedAgeCategory);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{ageCategoryId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("ageCategoryId") String ageCategoryId,
            final HttpServletRequest request) {

        AgeCategory updatedAgeCategory = ageCategoryService.inactivate(ageCategoryId);

//        eventPublisher.publishEvent(new OnAgeCategoryModifiedEvent(new EventObj("IN_ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedAgeCategory);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{ageCategoryId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("ageCategoryId") String ageCategoryId,
            final HttpServletRequest request) {

        AgeCategory updatedAgeCategory = ageCategoryService.delete(ageCategoryId);

//        eventPublisher.publishEvent(new OnAgeCategoryModifiedEvent(new EventObj("DELETE"), this.getClass()));

        sResponse = new ServerResponse(updatedAgeCategory);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{ageCategoryId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("ageCategoryId") String ageCategoryId,
            final HttpServletRequest request) {

        ageCategoryService.hardDelete(ageCategoryId);

//        eventPublisher.publishEvent(new OnAgeCategoryModifiedEvent(new EventObj("HARD_DELETE"), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
