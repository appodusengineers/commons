package com.appodus.commons.domain.contact.email;

import com.appodus.commons.domain.abstractservice.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class EmailService extends AbstractService<Email, EmailDao> {

    private final EmailDao emailDao;

    @Autowired
    public EmailService(EmailDao emailDao) {
        this.emailDao = emailDao;
    }

    public Email getByValue(String value){
        return emailDao.findByValue(value);
    }
}
