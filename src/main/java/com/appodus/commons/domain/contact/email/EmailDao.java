package com.appodus.commons.domain.contact.email;


import com.appodus.commons.domain.abstractservice.AbstractDao;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface EmailDao extends AbstractDao<Email> {
    Email findByValue(String value);
}
