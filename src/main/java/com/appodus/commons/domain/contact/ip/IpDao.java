package com.appodus.commons.domain.contact.ip;


import com.appodus.commons.domain.abstractservice.AbstractDao;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface IpDao extends AbstractDao<Ip> {
}
