package com.appodus.commons.domain.contact.address.lga;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.contact.address.lga.data.CountryStatesLocals;
import com.appodus.commons.domain.contact.address.lga.data.DataLocalGov;
import com.appodus.commons.domain.contact.address.lga.data.DataStateGov;
import com.appodus.commons.domain.contact.address.lga.state.State;
import com.appodus.commons.domain.contact.address.lga.state.StateService;
import com.appodus.commons.domain.contact.address.lga.state.country.Country;
import com.appodus.commons.domain.contact.address.lga.state.country.CountryService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class LgaService extends AbstractService<Lga, LgaDao> {

    private final LgaDao lgaDao;
    private final CountryService countryService;
    private final StateService stateService;
    @Value("classpath:countryStatesLocals.json")
    private Resource stateLocalsJsonResource;


    @Autowired
    public LgaService(LgaDao lgaDao, CountryService countryService, StateService stateService) {
        this.lgaDao = lgaDao;
        this.countryService = countryService;
        this.stateService = stateService;
    }

    public List<Lga> getLgasByState(State state) {
        return lgaDao.findByState(state);
    }
    public Lga update(String lgaId, Lga lga) {

        throw new NotImplementedException();

//        return super.update(_lga);
    }
    public Lga getByName(String name) {
        return lgaDao.findByName(name);
    }

    @PostConstruct
    public void init() throws IOException {
        System.out.println("Started LgaService.init()");

        if (super.count() < 1) {
//            Data from: https://gist.github.com/ebaranov/41bf38fdb1a2cb19a781#file-gistfile1-json
            String stateLocals = FileUtils.readFileToString(Paths.get(stateLocalsJsonResource.getURI()).toFile(), "ISO-8859-1");
            System.out.println("stateLocals: " + stateLocals);
            List<CountryStatesLocals> stateLocalsList = Utils.jsonStringToCountryStatesLocalsList(stateLocals);
            System.out.println("stateLocalsList: " + stateLocalsList);
            List<Lga> lgaList = null;


            for (CountryStatesLocals _stateLocals: stateLocalsList) {

                Country country = countryService.getByName(_stateLocals.getCountry());
                if (country != null) {
                    List<DataStateGov> dataStateGovs = _stateLocals.getStates();
                    for(DataStateGov _Data_stateGov : dataStateGovs) {
                        State state = stateService.getByNameAndCountry(_Data_stateGov.getName(), country);
                        if (state != null) {
                            lgaList = new ArrayList<>();
                            for(DataLocalGov local : _Data_stateGov.getLocals()) {

                                Lga lga = new Lga(local.getName());
                                lga.setState(state);
                                lga.setCountry(country);
                                lgaList.add(lga);
                            }

                            List<Lga> _lgaList = super.createAll(lgaList);

                            // Set Lgas to state
//                            state.setLgaList(_lgaList);
//                            stateService.update(state);
                        } else {
                            System.out.println();
                            System.out.printf("DataStateGov: %s, not found", _Data_stateGov.getName());
                            System.out.printf("country: %s, not found", country);
                        }
                    }
                } else {
                    System.out.println();
                    System.out.printf("Country: %s, not found", _stateLocals.getCountry());
                }
            }
        }

        System.out.println("Ended LgaService.init()");
    }

}
