package com.appodus.commons.domain.contact.address.lga.state.country;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.contact.address.lga.state.State;
import com.appodus.commons.domain.contact.address.lga.state.country.region.SubRegion;
import com.appodus.commons.domain.contact.address.lga.state.country.region.continent.Continent;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.io.IOException;
import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Country extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String name;
    private String capital;
    private String callingCodesStr;
    @Transient
    private List<String> callingCodes;
    private String flag;
    private String alpha2Code;
    private String alpha3Code;
    private String timezonesStr;
    @Transient
    private List<String> timezones;
    private String numericCode;
    @OneToMany
    @JoinTable(
            name = "country_states",
            joinColumns = @JoinColumn(name = "country_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "state_id", referencedColumnName = "id"))
    private List<State> stateList;
    @ManyToOne
    private SubRegion subRegion;
    @ManyToOne
    private Continent continent;
    @Transient
    private String subRegionStr;
    @Transient
    private String continentStr;

    public Country() {
        super("", "", "");
    }

    public Country(String name) {
        this();

        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public List<String> getCallingCodes() {
        return callingCodes;
    }

    public void setCallingCodes(List<String> callingCodes) {
        this.callingCodes = callingCodes;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAlpha2Code() {
        return alpha2Code;
    }

    public void setAlpha2Code(String alpha2Code) {
        this.alpha2Code = alpha2Code;
    }

    public String getAlpha3Code() {
        return alpha3Code;
    }

    public void setAlpha3Code(String alpha3Code) {
        this.alpha3Code = alpha3Code;
    }

    public List<String> getTimezones() {
        return timezones;
    }

    public void setTimezones(List<String> timezones) {
        this.timezones = timezones;
    }

    public String getNumericCode() {
        return numericCode;
    }

    public void setNumericCode(String numericCode) {
        this.numericCode = numericCode;
    }

    public List<State> getStateList() {
        return stateList;
    }

    public void setStateList(List<State> stateList) {
        this.stateList = stateList;
    }

    public SubRegion getSubRegion() {
        return subRegion;
    }

    public void setSubRegion(SubRegion subRegion) {
        this.subRegion = subRegion;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }

    public String getSubRegionStr() {
        return subRegionStr;
    }

    public void setSubRegionStr(String subRegionStr) {
        this.subRegionStr = subRegionStr;
    }

    public String getContinentStr() {
        return continentStr;
    }

    public void setContinentStr(String continentStr) {
        this.continentStr = continentStr;
    }

    @PrePersist
    public void prePersist() {

        if (this.stateList != null) {
            if (StringUtils.isEmpty(this.stateList.get(0).getId())) {
                this.stateList = null;
            }
        }

        if (this.subRegion != null) {
            if (StringUtils.isEmpty(this.subRegion.getId())) {
                this.subRegion = null;
            }
        }

        if (this.continent != null) {
            if (StringUtils.isEmpty(this.continent.getId())) {
                this.continent = null;
            }
        }

        // Convert TimeZones & CallingCodes
        timezonesStr = Utils.objToJsonString(timezones);
        callingCodesStr = Utils.objToJsonString(callingCodes);

    }
    @PreUpdate
    public void preUpdate()
    {
        prePersist();
    }

    @PostLoad
    public void postLoad() {
        // Convert TimeZones
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            timezones = objectMapper.readValue(timezonesStr, new TypeReference<List<String>>() {
            });
            callingCodes = objectMapper.readValue(callingCodesStr, new TypeReference<List<String>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", capital='" + capital + '\'' +
                ", callingCodesStr='" + callingCodesStr + '\'' +
                ", callingCodes=" + callingCodes +
                ", flag='" + flag + '\'' +
                ", alpha2Code='" + alpha2Code + '\'' +
                ", alpha3Code='" + alpha3Code + '\'' +
                ", timezonesStr='" + timezonesStr + '\'' +
                ", timezones=" + timezones +
                ", numericCode='" + numericCode + '\'' +
                ", stateList=" + stateList +
                ", subRegion=" + subRegion +
                ", continent=" + continent +
                ", subRegionStr='" + subRegionStr + '\'' +
                ", continentStr='" + continentStr + '\'' +
                '}';
    }
}
