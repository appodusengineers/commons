package com.appodus.commons.domain.contact.address.lga.state.country;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.contact.address.lga.state.State;
import com.appodus.commons.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/countries")
public class CountryController {
    private final CountryService countryService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public CountryController(
            CountryService countryService,
            ApplicationEventPublisher eventPublisher) {
        this.countryService = countryService;
        this.eventPublisher = eventPublisher;
    }
    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody Country country, final HttpServletRequest request) {
        System.out.println("country: " + country);
        Country _country = countryService.create(country);

//        eventPublisher.publishEvent(new OnCountryModifiedEvent(new EventObj("CREATE"), this.getClass()));

        sResponse = new ServerResponse(_country);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }
    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<Country> countryList = countryService.getAll();

        sResponse = new ServerResponse(countryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{countryId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("countryId") String countryId, final HttpServletRequest request) {
        Country country = countryService.getById(countryId);

        sResponse = new ServerResponse(country);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<Country> countryList = countryService.getAllActive();

        sResponse = new ServerResponse(countryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<Country> countryList = countryService.getAllInactive();

        sResponse = new ServerResponse(countryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{countryId}/states")
    public ResponseEntity<ServerResponse> getCountryStates(@PathVariable("countryId") String countryId, final HttpServletRequest request) {
        List<State> stateList = countryService.getCountryStates(countryId);

        sResponse = new ServerResponse(stateList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{countryId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("countryId") String countryId,
            @RequestBody Country country,
            final HttpServletRequest request) {

        Country updatedCountry = countryService.update(countryId, country);

//        eventPublisher.publishEvent(new OnCountryModifiedEvent(new EventObj("UPDATE"), this.getClass()));

        sResponse = new ServerResponse(updatedCountry);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{countryId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("countryId") String countryId,
            final HttpServletRequest request) {

        Country updatedCountry = countryService.activate(countryId);

//        eventPublisher.publishEvent(new OnCountryModifiedEvent(new EventObj("ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedCountry);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{countryId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("countryId") String countryId,
            final HttpServletRequest request) {

        Country updatedCountry = countryService.inactivate(countryId);

//        eventPublisher.publishEvent(new OnCountryModifiedEvent(new EventObj("IN_ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedCountry);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{countryId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("countryId") String countryId,
            final HttpServletRequest request) {

        Country updatedCountry = countryService.delete(countryId);

//        eventPublisher.publishEvent(new OnCountryModifiedEvent(new EventObj("DELETE"), this.getClass()));

        sResponse = new ServerResponse(updatedCountry);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{countryId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("countryId") String countryId,
            final HttpServletRequest request) {

        countryService.hardDelete(countryId);

//        eventPublisher.publishEvent(new OnCountryModifiedEvent(new EventObj("HARD_DELETE"), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
