package com.appodus.commons.domain.contact;


import com.appodus.commons.domain.abstractservice.AbstractDao;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface ContactDao extends AbstractDao<Contact> {
}
