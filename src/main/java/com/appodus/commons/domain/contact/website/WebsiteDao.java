package com.appodus.commons.domain.contact.website;


import com.appodus.commons.domain.abstractservice.AbstractDao;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface WebsiteDao extends AbstractDao<Website> {
}
