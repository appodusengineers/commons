package com.appodus.commons.domain.contact.website;

import com.appodus.commons.domain.abstractservice.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class WebsiteService extends AbstractService<Website, WebsiteDao> {

    private final WebsiteDao websiteDao;

    @Autowired
    public WebsiteService(WebsiteDao websiteDao) {
        this.websiteDao = websiteDao;
    }
}
