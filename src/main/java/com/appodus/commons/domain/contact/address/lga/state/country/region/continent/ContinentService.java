package com.appodus.commons.domain.contact.address.lga.state.country.region.continent;

import com.appodus.commons.domain.abstractservice.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class ContinentService extends AbstractService<Continent, ContinentDao> {

    private final ContinentDao continentDao;

    @Autowired
    public ContinentService(ContinentDao continentDao) {
        this.continentDao = continentDao;
    }

    public Continent getByName(String name) {
        return continentDao.findByName(name);
    }

    @PostConstruct
    public void init() {
        if (super.count() < 1) {
            List<Continent> continentList = Arrays.asList(
                    new Continent("ASIA"),
                    new Continent("AFRICA"),
                    new Continent("NORTH AMERICA"),
                    new Continent("SOUTH AMERICA"),
                    new Continent("ANTARCTICA"),
                    new Continent("EUROPE"),
                    new Continent("AUSTRALIA"),

                    new Continent("OCEANIA"),
                    new Continent("AMERICAS"),
                    new Continent("POLAR")
            );

            super.createAll(continentList);
        }
    }
}

//new Continent("ASIA"),
//        new Continent("AFRICA"),
//        new Continent("NORTH AMERICA"),
//        new Continent("CENTRAL AMERICA"),
//        new Continent("SOUTH AMERICA"),
//        new Continent("THE CARIBBEAN"),
//        new Continent("EUROPE"),
//        new Continent("OCEANIA")