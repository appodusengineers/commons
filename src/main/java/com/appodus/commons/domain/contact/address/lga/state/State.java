package com.appodus.commons.domain.contact.address.lga.state;

import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.contact.address.lga.Lga;
import com.appodus.commons.domain.contact.address.lga.state.country.Country;
import com.appodus.commons.domain.contact.address.lga.state.country.region.SubRegion;
import com.appodus.commons.domain.contact.address.lga.state.country.region.continent.Continent;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"name", "country_id"})
})
public class State extends AbstractEntity {
    @Column(nullable = false)
    private String name;
    @OneToMany
    @JoinTable(
            name = "state_lgas",
            joinColumns = @JoinColumn(name = "state_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "lga_id", referencedColumnName = "id"))
    private List<Lga> lgaList;
    @ManyToOne
    private Country country;
    @ManyToOne
    private SubRegion subRegion;
    @ManyToOne
    private Continent continent;

    public State() {
        super("", "", "");
    }

    public State(String name) {
        this();

        this.name = name;
    }

    @PrePersist
    public void prePersist() {

        if (this.lgaList != null) {
            if (StringUtils.isEmpty(this.lgaList.get(0).getId())) {
                this.lgaList = null;
            }
        }

        if (this.country != null) {
            if (StringUtils.isEmpty(this.country.getId())) {
                this.country = null;
            }
        }

        if (this.continent != null) {
            if (StringUtils.isEmpty(this.continent.getId())) {
                this.continent = null;
            }
        }

        if (this.subRegion != null) {
            if (StringUtils.isEmpty(this.subRegion.getId())) {
                this.subRegion = null;
            }
        }

    }
    @PreUpdate
    public void preUpdate()
    {
        prePersist();
    }
}
