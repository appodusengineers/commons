package com.appodus.commons.domain.contact.email;


import com.appodus.commons.domain.abstractentity.AbstractEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Email extends AbstractEntity {
    @Column(nullable = false)
    private String value;
    private String description;

    public Email() {
        super("", "", "");
    }

    public Email(String value) {
        this(value, "Primary email");
    }

    public Email(String value, String description) {
        this();

        this.value = value;
        this.description = description;
    }
}
