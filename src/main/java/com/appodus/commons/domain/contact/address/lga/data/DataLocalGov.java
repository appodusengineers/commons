package com.appodus.commons.domain.contact.address.lga.data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public class DataLocalGov {
    private String name;
    private long id;

    public DataLocalGov() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "DataLocalGov{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
