package com.appodus.commons.domain.contact.address.lga;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.contact.address.lga.state.State;
import com.appodus.commons.domain.contact.address.lga.state.country.Country;
import com.appodus.commons.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/lgas")
public class LgaController {
    private final LgaService lgaService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public LgaController(
            LgaService lgaService,
            ApplicationEventPublisher eventPublisher) {
        this.lgaService = lgaService;
        this.eventPublisher = eventPublisher;
    }
    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody Lga lga, final HttpServletRequest request) {
        System.out.println("lga: " + lga);
        Lga _lga = lgaService.create(lga);

//        eventPublisher.publishEvent(new OnLgaModifiedEvent(new EventObj("CREATE"), this.getClass()));

        sResponse = new ServerResponse(_lga);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }
    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<Lga> lgaList = lgaService.getAll();

        sResponse = new ServerResponse(lgaList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{lgaId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("lgaId") String lgaId, final HttpServletRequest request) {
        Lga lga = lgaService.getById(lgaId);

        sResponse = new ServerResponse(lga);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<Lga> lgaList = lgaService.getAllActive();

        sResponse = new ServerResponse(lgaList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<Lga> lgaList = lgaService.getAllInactive();

        sResponse = new ServerResponse(lgaList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-state")
    public ResponseEntity<ServerResponse> getLgasByState(@RequestBody State state, final HttpServletRequest request) {
        List<Lga> lgaList = lgaService.getLgasByState(state);

        sResponse = new ServerResponse(lgaList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{lgaId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("lgaId") String lgaId,
            @RequestBody Lga lga,
            final HttpServletRequest request) {

        Lga updatedLga = lgaService.update(lgaId, lga);

//        eventPublisher.publishEvent(new OnLgaModifiedEvent(new EventObj("UPDATE"), this.getClass()));

        sResponse = new ServerResponse(updatedLga);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{lgaId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("lgaId") String lgaId,
            final HttpServletRequest request) {

        Lga updatedLga = lgaService.activate(lgaId);

//        eventPublisher.publishEvent(new OnLgaModifiedEvent(new EventObj("ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedLga);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{lgaId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("lgaId") String lgaId,
            final HttpServletRequest request) {

        Lga updatedLga = lgaService.inactivate(lgaId);

//        eventPublisher.publishEvent(new OnLgaModifiedEvent(new EventObj("IN_ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedLga);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{lgaId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("lgaId") String lgaId,
            final HttpServletRequest request) {

        Lga updatedLga = lgaService.delete(lgaId);

//        eventPublisher.publishEvent(new OnLgaModifiedEvent(new EventObj("DELETE"), this.getClass()));

        sResponse = new ServerResponse(updatedLga);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{lgaId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("lgaId") String lgaId,
            final HttpServletRequest request) {

        lgaService.hardDelete(lgaId);

//        eventPublisher.publishEvent(new OnLgaModifiedEvent(new EventObj("HARD_DELETE"), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
