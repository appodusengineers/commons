package com.appodus.commons.domain.contact;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.contact.address.AddressService;
import com.appodus.commons.domain.contact.email.EmailService;
import com.appodus.commons.domain.contact.ip.IpService;
import com.appodus.commons.domain.contact.loci.LociService;
import com.appodus.commons.domain.contact.phone.PhoneService;
import com.appodus.commons.domain.contact.website.WebsiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class ContactService extends AbstractService<Contact, ContactDao> {

    private final ContactDao contactDao;
    private final AddressService addressService;
    private final EmailService emailService;
    private final IpService ipService;
    private final LociService lociService;
    private final PhoneService phoneService;
    private final WebsiteService websiteService;

    @Autowired
    public ContactService(ContactDao contactDao, AddressService addressService, EmailService emailService, IpService ipService, LociService lociService, PhoneService phoneService, WebsiteService websiteService) {
        this.contactDao = contactDao;
        this.addressService = addressService;
        this.emailService = emailService;
        this.ipService = ipService;
        this.lociService = lociService;
        this.phoneService = phoneService;
        this.websiteService = websiteService;
    }

    public Contact create(Contact contact) {
        if (contact.getAddressList() != null) {
            contact.setAddressList(addressService.createAll(contact.getAddressList()));
        }

        if (contact.getEmailList() != null) {
            if (StringUtils.hasText(contact.getEmailList().get(0).getValue())) {
                contact.setEmailList(emailService.createAll(contact.getEmailList()));

            }
        }

        if (contact.getIpList() != null) {
            if (StringUtils.hasText(contact.getIpList().get(0).getValue())) {
                contact.setIpList(ipService.createAll(contact.getIpList()));

            }
        }

        if (contact.getLociList() != null) {
            if (contact.getLociList().get(0).getLongitude() > 0) {
                contact.setLociList(lociService.createAll(contact.getLociList()));

            }
        }

        if (contact.getPhoneList() != null) {
            if (StringUtils.hasText(contact.getPhoneList().get(0).getValue())) {
                contact.setPhoneList(phoneService.createAll(contact.getPhoneList()));

            }
        }

        if (contact.getWebsiteList() != null) {
            if (StringUtils.hasText(contact.getWebsiteList().get(0).getWebsite())) {
                contact.setWebsiteList(websiteService.createAll(contact.getWebsiteList()));

            }
        }

        return super.create(contact);
    }
}
