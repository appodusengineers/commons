package com.appodus.commons.domain.contact.address.lga.state.country.region.continent;


import com.appodus.commons.domain.abstractservice.AbstractDao;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface ContinentDao extends AbstractDao<Continent> {
    Continent findByName(String name);
}
