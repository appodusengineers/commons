package com.appodus.commons.domain.contact.phone;


import com.appodus.commons.domain.abstractentity.AbstractEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(uniqueConstraints = {
//        @UniqueConstraint(columnNames = {"ext", "value"})
})
public class Phone extends AbstractEntity {
    @Column(nullable = false)
    private String ext;
    @Column(nullable = false)
    private String value;
    private String description;

    public Phone() {
        super("", "", "");
    }

    public Phone(String ext, String value, String description) {
        this();

        this.ext = ext;
        this.value = value;
        this.description = description;
    }
}
