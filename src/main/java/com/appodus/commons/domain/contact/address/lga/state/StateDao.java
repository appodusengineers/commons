package com.appodus.commons.domain.contact.address.lga.state;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.contact.address.lga.state.country.Country;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface StateDao extends AbstractDao<State> {
    State findByNameAndCountry(String name, Country country);
    List<State> findByCountry(Country country);

    State findByName(String name);
}
