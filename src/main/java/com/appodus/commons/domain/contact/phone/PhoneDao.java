package com.appodus.commons.domain.contact.phone;


import com.appodus.commons.domain.abstractservice.AbstractDao;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface PhoneDao extends AbstractDao<Phone> {
    Phone findByExtAndValue(String ext, String value);
}
