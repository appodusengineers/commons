package com.appodus.commons.domain.contact;

import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.contact.address.Address;
import com.appodus.commons.domain.contact.email.Email;
import com.appodus.commons.domain.contact.ip.Ip;
import com.appodus.commons.domain.contact.loci.Loci;
import com.appodus.commons.domain.contact.phone.Phone;
import com.appodus.commons.domain.contact.website.Website;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Contact extends AbstractEntity {

    @OneToMany
    @JoinTable(
            name = "contact_addresses",
            joinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "address_id", referencedColumnName = "id"))
    private List<Address> addressList;
    @OneToMany
    @JoinTable(
            name = "contact_emails",
            joinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "email_id", referencedColumnName = "id"))
    private List<Email> emailList;
    @OneToMany
    @JoinTable(
            name = "contact_ips",
            joinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "ip_id", referencedColumnName = "id"))
    private List<Ip> ipList;
    @OneToMany
    @JoinTable(
            name = "contact_locus",
            joinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "loci_id", referencedColumnName = "id"))
    private List<Loci> lociList;
    @OneToMany
    @JoinTable(
            name = "contact_phones",
            joinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "phone_id", referencedColumnName = "id"))
    private List<Phone> phoneList;
    @OneToMany
    @JoinTable(
            name = "contact_websites",
            joinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "website_id", referencedColumnName = "id"))
    private List<Website> websiteList;

    public Contact() {
        super("", "", "");

        this.addressList = Arrays.asList(new Address());
        this.emailList = Arrays.asList(new Email());
        this.ipList = Arrays.asList(new Ip());
        this.lociList = Arrays.asList(new Loci());
        this.phoneList = Arrays.asList(new Phone());
        this.websiteList = Arrays.asList(new Website());
    }

    @PrePersist
    public void prePersist() {

        if (this.addressList != null) {
            if (StringUtils.isEmpty(this.addressList.get(0).getId())) {
                this.addressList = null;
            }
        }

        if (this.emailList != null) {
            if (StringUtils.isEmpty(this.emailList.get(0).getId())) {
                this.emailList = null;
            }
        }

        if (this.ipList != null) {
            if (StringUtils.isEmpty(this.ipList.get(0).getId())) {
                this.ipList = null;
            }
        }

        if (this.lociList != null) {
            if (StringUtils.isEmpty(this.lociList.get(0).getId())) {
                this.lociList = null;
            }
        }

        if (this.phoneList != null) {
            if (StringUtils.isEmpty(this.phoneList.get(0).getId())) {
                this.phoneList = null;
            }
        }

        if (this.websiteList != null) {
            if (StringUtils.isEmpty(this.websiteList.get(0).getId())) {
                this.websiteList = null;
            }
        }
    }
    @PreUpdate
    public void preUpdate()
    {
        prePersist();
    }
}
