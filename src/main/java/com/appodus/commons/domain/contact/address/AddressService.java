package com.appodus.commons.domain.contact.address;

import com.appodus.commons.domain.abstractservice.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class AddressService extends AbstractService<Address, AddressDao> {

    private final AddressDao addressDao;

    @Autowired
    public AddressService(AddressDao addressDao) {
        this.addressDao = addressDao;
    }
}
