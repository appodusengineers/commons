package com.appodus.commons.domain.contact.phone;

import com.appodus.commons.domain.abstractservice.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class PhoneService extends AbstractService<Phone, PhoneDao> {

    private final PhoneDao phoneDao;

    @Autowired
    public PhoneService(PhoneDao phoneDao) {
        this.phoneDao = phoneDao;
    }

    public Phone getByExtAndValue(String ext, String value){
        return phoneDao.findByExtAndValue(ext, value);
    }
}
