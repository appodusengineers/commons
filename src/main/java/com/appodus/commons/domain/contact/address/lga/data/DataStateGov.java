package com.appodus.commons.domain.contact.address.lga.data;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public class DataStateGov {
    private String name;
    private long id;
    private List<DataLocalGov> locals;

    public DataStateGov() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<DataLocalGov> getLocals() {
        return locals;
    }

    public void setLocals(List<DataLocalGov> locals) {
        this.locals = locals;
    }

    @Override
    public String toString() {
        return "DataStateGov{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", locals=" + locals +
                '}';
    }
}
