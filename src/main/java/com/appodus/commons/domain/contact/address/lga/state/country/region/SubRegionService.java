package com.appodus.commons.domain.contact.address.lga.state.country.region;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.contact.address.lga.state.country.region.continent.Continent;
import com.appodus.commons.domain.contact.address.lga.state.country.region.continent.ContinentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class SubRegionService extends AbstractService<SubRegion, SubRegionDao> {

    private final SubRegionDao subRegionDao;
    private final ContinentService continentService;

    @Autowired
    public SubRegionService(SubRegionDao subRegionDao, ContinentService continentService) {
        this.subRegionDao = subRegionDao;
        this.continentService = continentService;
    }


    public SubRegion getByName(String name) {
        return subRegionDao.findByName(name);
    }

    @PostConstruct
    public void init() {
        if (super.count() < 1) {

            // ASIA
            Continent asia = continentService.getByName("ASIA");
            List<SubRegion> asianSubRegionList = Arrays.asList(
                    new SubRegion("SOUTHERN ASIA", asia),
                    new SubRegion("EASTERN ASIA", asia),
                    new SubRegion("WESTERN ASIA", asia),
                    new SubRegion("CENTRAL ASIA", asia),
                    new SubRegion("SOUTHEASTERN ASIA", asia)
            );
            super.createAll(asianSubRegionList);

            // AFRICA
            Continent africa = continentService.getByName("AFRICA");
            List<SubRegion> africanSubRegionList = Arrays.asList(
                    new SubRegion("Northern Africa", africa),
                    new SubRegion("Eastern Africa", africa),
                    new SubRegion("Central Africa", africa),
                    new SubRegion("Western Africa", africa),
                    new SubRegion("Southern Africa", africa)
            );
            super.createAll(africanSubRegionList);

//            // NORTH AMERICA
//            Continent northAmerica = continentService.getByName("NORTH AMERICA");
//            List<SubRegion> northAmericanSubRegionList = Arrays.asList(
//                    new SubRegion("Northern America", northAmerica),
//                    new SubRegion("Caribbean", northAmerica),
//                    new SubRegion("Central America", northAmerica)
//            );
//            super.createAll(northAmericanSubRegionList);
//
//            // SOUTH AMERICA
//            Continent southAmerica = continentService.getByName("SOUTH AMERICA");
//            List<SubRegion> southAmericanSubRegionList = Arrays.asList(
//                    new SubRegion("SOUTH AMERICA", southAmerica)
//            );
//            super.createAll(southAmericanSubRegionList);

            // ANTARCTICA
            Continent antarctica = continentService.getByName("ANTARCTICA");
            List<SubRegion> antarcticaSubRegionList = Arrays.asList(
                    new SubRegion("ANTARCTICA", antarctica)
            );
            super.createAll(antarcticaSubRegionList);

            // EUROPE
            Continent europe = continentService.getByName("EUROPE");
            List<SubRegion> europeanSubRegionList = Arrays.asList(
                    new SubRegion("Eastern Europe", europe),
                    new SubRegion("Northern Europe", europe),
                    new SubRegion("Southern Europe", europe),
                    new SubRegion("Western  Europe", europe)
            );
            super.createAll(europeanSubRegionList);

            // AUSTRALIA
            Continent australia = continentService.getByName("AUSTRALIA");
            List<SubRegion> australianSubRegionList = Arrays.asList(
                    new SubRegion("AUSTRALIA", australia)
            );
            super.createAll(australianSubRegionList);



            // OCEANIA
            Continent oceania = continentService.getByName("OCEANIA");
            List<SubRegion> oceaniaSubRegionList = Arrays.asList(
                    new SubRegion("Polynesia", oceania),
                    new SubRegion("Australia and New Zealand", oceania),
                    new SubRegion("Melanesia", oceania),
                    new SubRegion("Micronesia", oceania)
            );
            super.createAll(oceaniaSubRegionList);

            // AMERICAS
            Continent americas = continentService.getByName("AMERICAS");
            List<SubRegion> americasSubRegionList = Arrays.asList(
                    new SubRegion("Caribbean", americas),
                    new SubRegion("Central America", americas),
                    new SubRegion("Northern America", americas),
                    new SubRegion("South America", americas)
            );
            super.createAll(americasSubRegionList);

//            // POLAR
//            Continent polar = continentService.getByName("POLAR");
//            List<SubRegion> polarSubRegionList = Arrays.asList(
//                    new SubRegion("", polar)
//            );
//            super.createAll(polarSubRegionList);
        }
    }
}
