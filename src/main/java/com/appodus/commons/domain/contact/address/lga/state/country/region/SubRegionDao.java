package com.appodus.commons.domain.contact.address.lga.state.country.region;


import com.appodus.commons.domain.abstractservice.AbstractDao;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface SubRegionDao extends AbstractDao<SubRegion> {
    SubRegion findByName(String name);
}
