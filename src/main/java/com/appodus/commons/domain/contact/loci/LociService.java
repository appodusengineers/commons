package com.appodus.commons.domain.contact.loci;

import com.appodus.commons.domain.abstractservice.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class LociService extends AbstractService<Loci, LociDao> {

    private final LociDao lociDao;

    @Autowired
    public LociService(LociDao lociDao) {
        this.lociDao = lociDao;
    }
}
