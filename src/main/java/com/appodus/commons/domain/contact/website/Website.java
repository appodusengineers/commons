package com.appodus.commons.domain.contact.website;


import com.appodus.commons.domain.abstractentity.AbstractEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Entity;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Website extends AbstractEntity {
    private String website;
    private String description;
    private String username;
    private String password;

    public Website() {
        super();
    }
}
