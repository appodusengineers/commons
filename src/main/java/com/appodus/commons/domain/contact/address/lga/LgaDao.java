package com.appodus.commons.domain.contact.address.lga;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.contact.address.lga.state.State;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface LgaDao extends AbstractDao<Lga> {
    List<Lga> findByState(State state);

    Lga findByName(String name);
}
