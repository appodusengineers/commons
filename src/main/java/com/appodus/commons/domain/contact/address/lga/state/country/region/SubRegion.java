package com.appodus.commons.domain.contact.address.lga.state.country.region;

import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.contact.address.lga.state.country.Country;
import com.appodus.commons.domain.contact.address.lga.state.country.region.continent.Continent;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(name = "subregion")
public class SubRegion extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String name;
    @OneToMany
    @JoinTable(
            name = "region_countries",
            joinColumns = @JoinColumn(name = "region_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "country_id", referencedColumnName = "id"))
    private List<Country> countryList;
    @ManyToOne
    private Continent continent;

    public SubRegion() {
        super("", "", "");
    }

    public SubRegion(String name) {
        this();

        this.name = name;
    }

    public SubRegion(String name, List<Country> countryList) {
        this(name);

        this.countryList = countryList;
    }

    public SubRegion(String name, Continent continent) {
        this(name);

        this.continent = continent;
    }

    public SubRegion(String name, List<Country> countryList, Continent continent) {
        this(name, continent);

        this.countryList = countryList;
    }

    @PrePersist
    public void prePersist() {

        if (this.countryList != null) {
            if (StringUtils.isEmpty(this.countryList.get(0).getId())) {
                this.countryList = null;
            }
        }

        if (this.continent != null) {
            if (StringUtils.isEmpty(this.continent.getId())) {
                this.continent = null;
            }
        }
    }
    @PreUpdate
    public void preUpdate()
    {
        prePersist();
    }
}
