package com.appodus.commons.domain.contact.ip;

import com.appodus.commons.domain.abstractservice.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class IpService extends AbstractService<Ip, IpDao> {

    private final IpDao ipDao;

    @Autowired
    public IpService(IpDao ipDao) {
        this.ipDao = ipDao;
    }
}
