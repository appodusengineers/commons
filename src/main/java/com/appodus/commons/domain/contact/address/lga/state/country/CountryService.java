package com.appodus.commons.domain.contact.address.lga.state.country;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.contact.address.lga.state.State;
import com.appodus.commons.domain.contact.address.lga.state.country.region.SubRegion;
import com.appodus.commons.domain.contact.address.lga.state.country.region.SubRegionService;
import com.appodus.commons.domain.user.User;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class CountryService extends AbstractService<Country, CountryDao> {

    private final CountryDao countryDao;
    private final SubRegionService subRegionService;
    @Value("classpath:countryList.json")
    private Resource countryListJsonResource;

    @Autowired
    public CountryService(CountryDao countryDao, SubRegionService subRegionService) {
        this.countryDao = countryDao;
        this.subRegionService = subRegionService;
    }

    public Country getByName(String name) {
        return countryDao.findByName(name);
    }

    public List<State> getCountryStates(String countryId) {
        return super.getById(countryId).getStateList();
    }

    public Country update(String countryId, Country country) {

        throw new NotImplementedException();

//        return super.update(_country);
    }

    @PostConstruct
    public void init() throws IOException {

        if (super.count() < 1) {
//            Data from: https://restcountries.eu/
            String countryListStr = FileUtils.readFileToString(Paths.get(countryListJsonResource.getURI()).toFile(), "ISO-8859-1");
            System.out.println("countryListStr: " + countryListStr);
            List<Country> countries = Utils.jsonStringToCountryList(countryListStr);
            System.out.println("countries: " + countries);
            List<Country> countryList = new ArrayList<>();

            countries.forEach(country -> {
                SubRegion subRegion = null;
                if (country.getSubRegionStr() != null) {
                    subRegion = subRegionService.getByName(country.getSubRegionStr());
                }
                if (subRegion != null) {
                    country.setSubRegion(subRegion);
                    country.setContinent(subRegion.getContinent());
                    countryList.add(country);
                }
            });

            super.createAll(countryList);


        }
    }
}
