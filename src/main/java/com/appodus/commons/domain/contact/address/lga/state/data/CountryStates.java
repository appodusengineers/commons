package com.appodus.commons.domain.contact.address.lga.state.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryStates {
    private String country;
    private List<String> states;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<String> getStates() {
        return states;
    }

    public void setStates(List<String> states) {
        this.states = states;
    }

    @Override
    public String toString() {
        return "CountryStates{" +
                "country='" + country + '\'' +
                ", states=" + states +
                '}';
    }
}
