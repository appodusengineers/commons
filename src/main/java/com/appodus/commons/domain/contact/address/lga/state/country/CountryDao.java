package com.appodus.commons.domain.contact.address.lga.state.country;


import com.appodus.commons.domain.abstractservice.AbstractDao;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface CountryDao extends AbstractDao<Country> {
    Country findByName(String name);
}
