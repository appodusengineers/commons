package com.appodus.commons.domain.contact.address;


import com.appodus.commons.domain.abstractservice.AbstractDao;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface AddressDao extends AbstractDao<Address> {
}
