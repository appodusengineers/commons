package com.appodus.commons.domain.contact.address;

import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.contact.address.lga.Lga;
import com.appodus.commons.domain.contact.address.lga.state.State;
import com.appodus.commons.domain.contact.address.lga.state.country.Country;
import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.PreUpdate;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Address extends AbstractEntity {
    @ManyToOne
    private Country country;
    @ManyToOne
    private State state;
    @ManyToOne
    private Lga lga;
    private String town;
    private String streetAddress;
    private String landMark;
    private String postalCode;
    private String type;

    public Address() {
        super("", "", "");
    }

    public Address(String streetAddress) {
        this();

        this.streetAddress = streetAddress;
    }

    @PrePersist
    public void prePersist() {

        if (this.country != null) {
            if (StringUtils.isEmpty(this.country.getId())) {
                this.country = null;
            }
        }

        if (this.state != null) {
            if (StringUtils.isEmpty(this.state.getId())) {
                this.state = null;
            }
        }

        if (this.lga != null) {
            if (StringUtils.isEmpty(this.lga.getId())) {
                this.lga = null;
            }
        }
    }
    @PreUpdate
    public void preUpdate()
    {
        prePersist();
    }
}
