package com.appodus.commons.domain.contact.address.lga.state.country.region.continent;

import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.contact.address.lga.state.country.Country;
import com.appodus.commons.domain.contact.address.lga.state.country.region.SubRegion;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Continent extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String name;
    @OneToMany
    @JoinTable(
            name = "continent_regions",
            joinColumns = @JoinColumn(name = "continent_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "region_id", referencedColumnName = "id"))
    private List<SubRegion> subRegionList;
    @OneToMany
    @JoinTable(
            name = "continent_countries",
            joinColumns = @JoinColumn(name = "continent_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "country_id", referencedColumnName = "id"))
    private List<Country> countryList;

    public Continent() {
        super("", "", "");
    }

    public Continent(String name) {
        this();

        this.name = name;
    }

    @PrePersist
    public void prePersist() {

        if (this.countryList != null) {
            if (StringUtils.isEmpty(this.countryList.get(0).getId())) {
                this.countryList = null;
            }
        }

        if (this.subRegionList != null) {
            if (StringUtils.isEmpty(this.subRegionList.get(0).getName())) {
                this.subRegionList = null;
            }
        }
    }
    @PreUpdate
    public void preUpdate()
    {
        prePersist();
    }
}
