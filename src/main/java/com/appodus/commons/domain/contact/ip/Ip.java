package com.appodus.commons.domain.contact.ip;



import com.appodus.commons.domain.abstractentity.AbstractEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Entity;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Ip extends AbstractEntity {
    private String value;
    private String description;

    public Ip() {
        super("", "", "");
    }
}
