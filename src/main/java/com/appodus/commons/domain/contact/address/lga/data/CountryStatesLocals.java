package com.appodus.commons.domain.contact.address.lga.data;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public class CountryStatesLocals {
    private String country;
    private List<DataStateGov> states;

    public CountryStatesLocals() {
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<DataStateGov> getStates() {
        return states;
    }

    public void setStates(List<DataStateGov> states) {
        this.states = states;
    }

    @Override
    public String toString() {
        return "CountryStatesLocals{" +
                "country='" + country + '\'' +
                ", states=" + states +
                '}';
    }
}
