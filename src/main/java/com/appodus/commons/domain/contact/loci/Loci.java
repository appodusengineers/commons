package com.appodus.commons.domain.contact.loci;


import com.appodus.commons.domain.abstractentity.AbstractEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Entity;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Loci extends AbstractEntity {
    private double lattitude;
    private double longitude;
    private String description;

    public Loci() {
        super("", "", "");
    }
}
