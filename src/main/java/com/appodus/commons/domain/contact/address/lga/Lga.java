package com.appodus.commons.domain.contact.address.lga;

import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.contact.address.lga.state.State;
import com.appodus.commons.domain.contact.address.lga.state.country.Country;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.persistence.*;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"name", "state_id"})
})
public class Lga extends AbstractEntity {
    @Column(nullable = false)
    private String name;
    @ManyToOne
    private State state;
    @ManyToOne
    private Country country;

    public Lga() {
        super("", "", "");
    }

    public Lga(String name) {
        this();

        this.name = name;
    }

    @PrePersist
    public void prePersist() {

        if (this.state != null) {
            if (StringUtils.isEmpty(this.state.getId())) {
                this.state = null;
            }
        }

        if (this.country != null) {
            if (StringUtils.isEmpty(this.country.getId())) {
                this.country = null;
            }
        }

    }
    @PreUpdate
    public void preUpdate()
    {
        prePersist();
    }
}
