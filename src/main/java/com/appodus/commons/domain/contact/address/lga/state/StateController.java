package com.appodus.commons.domain.contact.address.lga.state;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.contact.address.lga.Lga;
import com.appodus.commons.domain.contact.address.lga.state.country.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/states")
public class StateController {
    private final StateService stateService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public StateController(
            StateService stateService,
            ApplicationEventPublisher eventPublisher) {
        this.stateService = stateService;
        this.eventPublisher = eventPublisher;
    }

    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody State state, final HttpServletRequest request) {
        System.out.println("state: " + state);
        State _state = stateService.create(state);

//        eventPublisher.publishEvent(new OnStateModifiedEvent(new EventObj("CREATE"), this.getClass()));

        sResponse = new ServerResponse(_state);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }

    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<State> stateList = stateService.getAll();

        sResponse = new ServerResponse(stateList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{stateId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("stateId") String stateId, final HttpServletRequest request) {
        State state = stateService.getById(stateId);

        sResponse = new ServerResponse(state);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<State> stateList = stateService.getAllActive();

        sResponse = new ServerResponse(stateList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<State> stateList = stateService.getAllInactive();

        sResponse = new ServerResponse(stateList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-country")
    public ResponseEntity<ServerResponse> getStatesByCountry(@RequestBody Country country, final HttpServletRequest request) {
        List<State> stateList = stateService.getStatesByCountry(country);

        sResponse = new ServerResponse(stateList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @GetMapping("{stateId}/lgas")
    public ResponseEntity<ServerResponse> getStateLgas(@PathVariable("stateId") String stateId, final HttpServletRequest request) {
        List<Lga> lgaList = stateService.getStateLgas(stateId);

        sResponse = new ServerResponse(lgaList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{stateId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("stateId") String stateId,
            @RequestBody State state,
            final HttpServletRequest request) {

        State updatedState = stateService.update(stateId, state);

//        eventPublisher.publishEvent(new OnStateModifiedEvent(new EventObj("UPDATE"), this.getClass()));

        sResponse = new ServerResponse(updatedState);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{stateId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("stateId") String stateId,
            final HttpServletRequest request) {

        State updatedState = stateService.activate(stateId);

//        eventPublisher.publishEvent(new OnStateModifiedEvent(new EventObj("ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedState);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{stateId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("stateId") String stateId,
            final HttpServletRequest request) {

        State updatedState = stateService.inactivate(stateId);

//        eventPublisher.publishEvent(new OnStateModifiedEvent(new EventObj("IN_ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedState);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{stateId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("stateId") String stateId,
            final HttpServletRequest request) {

        State updatedState = stateService.delete(stateId);

//        eventPublisher.publishEvent(new OnStateModifiedEvent(new EventObj("DELETE"), this.getClass()));

        sResponse = new ServerResponse(updatedState);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{stateId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("stateId") String stateId,
            final HttpServletRequest request) {

        stateService.hardDelete(stateId);

//        eventPublisher.publishEvent(new OnStateModifiedEvent(new EventObj("HARD_DELETE"), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
