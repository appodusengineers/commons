package com.appodus.commons.domain.contact.loci;


import com.appodus.commons.domain.abstractservice.AbstractDao;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface LociDao extends AbstractDao<Loci> {
}
