package com.appodus.commons.domain.contact.address.lga.state;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.contact.address.lga.Lga;
import com.appodus.commons.domain.contact.address.lga.state.country.Country;
import com.appodus.commons.domain.contact.address.lga.state.country.CountryService;
import com.appodus.commons.domain.contact.address.lga.state.data.CountryStates;
import com.appodus.commons.domain.user.User;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class StateService extends AbstractService<State, StateDao> {

    private final StateDao stateDao;
    private final CountryService countryService;
    @Value("classpath:countryStates.json")
    private Resource countriesStatesJsonResource;

    @Autowired
    public StateService(StateDao stateDao, CountryService countryService) {
        this.stateDao = stateDao;
        this.countryService = countryService;
    }

    public List<State> getStatesByCountry(Country country) {
        return stateDao.findByCountry(country);
    }
    public List<Lga> getStateLgas(String stateId) {
        return super.getById(stateId).getLgaList();
    }

    public State update(String stateId, State state) {

        throw new NotImplementedException();

//        return super.update(_state);
    }
    public State getByName(String name) {
        return stateDao.findByName(name);
    }

    public State getByNameAndCountry(String name, Country country) {
        return stateDao.findByNameAndCountry(name, country);
    }

    @PostConstruct
    public void init() throws IOException {
        System.out.println("Started StateService.init()");

        if (super.count() < 1) {
//            Data from: https://gist.github.com/ebaranov/41bf38fdb1a2cb19a781#file-gistfile1-json
            String countriesStates = FileUtils.readFileToString(Paths.get(countriesStatesJsonResource.getURI()).toFile(), "ISO-8859-1");
            System.out.println("countriesStates: " + countriesStates);
            List<CountryStates> countryStatesList = Utils.jsonStringToCountriesStatesList(countriesStates);
            System.out.println("countryStatesList: " + countryStatesList);
            List<State> stateList = null;


            for(CountryStates _countryStates :countryStatesList) {
                Country country = countryService.getByName(_countryStates.getCountry());
                if (country != null) {
                    List<String> states = _countryStates.getStates();
                    stateList = new ArrayList<>();
                    for(String _state: states) {
                        State state = new State(_state);
                        state.setCountry(country);
                        state.setContinent(country.getContinent());
                        state.setSubRegion(country.getSubRegion());

                        stateList.add(state);
                    }

                    List<State> _stateList = super.createAll(stateList);

                    // Set States to country
//                    country.setStateList(_stateList);
//                    countryService.update(country);
                }

            }
        }


        System.out.println("Ended StateService.init()");
    }
}
