package com.appodus.commons.domain.abstractentity;

import com.appodus.commons.domain.organization.Organization;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


/**
 * Represents the base Entity.
 * <p>
 * <p>
 * AbstractEntity tries to abstract general entity concerns, letting Entities that extends it to focus on it's core.
 * <p>
 *
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@MappedSuperclass
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractEntity implements Serializable, Cloneable {

//    private static final long serialVersionUID = UUID.randomUUID().timestamp();

    @Transient
    @JsonIgnore
    public String EXISTED_MESSAGE = "This {0} already exists."; // A similar entity already existed
    @Transient
    @JsonIgnore
    public String NOT_EXISTED_MESSAGE = "This {0} don't exist."; // A requested entity does not exist
    @Transient
    @JsonIgnore
    public String LIST_NOT_FOUND_MESSAGE = "{0} List not found."; // No entity already existed

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, nullable = false)
    protected String id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(insertable = true, updatable = false)
    protected Date createdDate;
    protected String createdBy;
    protected String createdByIp;

    @Version
    @Column(nullable = false)
    protected int version = 0;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    protected Date lastModifiedDate;
    protected String lastModifiedBy;
    protected String lastModifiedByIp;


    @Column(nullable = false)
    protected boolean active = true;
    @Temporal(TemporalType.TIMESTAMP)
    protected Date activatedDate;
    protected String activatedBy;
    protected String activatedByIp;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date inactivatedDate;
    protected String inactivatedBy;
    protected String inactivatedByIp;


    protected boolean archived;
    @Temporal(TemporalType.TIMESTAMP)
    protected Date archivedDate;
    protected String archivedBy;
    protected String archivedByIp;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date unarchivedDate;
    protected String unarchivedBy;
    protected String unarchivedByIp;

    protected boolean deleted;
    @Temporal(TemporalType.TIMESTAMP)
    protected Date deletedDate;
    protected String deletedBy;
    protected String deletedByIp;

    @ManyToOne
    protected Organization organization;

    @Transient
    protected Map<String, String> status = new HashMap<>();

    public AbstractEntity() {
        this("", "", "");
    }

    public AbstractEntity(String existedMessage, String notExistedMessage, String listNotFoundMessage) {
        EXISTED_MESSAGE = StringUtils.hasText(existedMessage) ? existedMessage : String.format(EXISTED_MESSAGE, "");
        NOT_EXISTED_MESSAGE = StringUtils.hasText(notExistedMessage) ? notExistedMessage : String.format(NOT_EXISTED_MESSAGE, "");
        LIST_NOT_FOUND_MESSAGE = StringUtils.hasText(listNotFoundMessage) ? listNotFoundMessage : String.format(LIST_NOT_FOUND_MESSAGE, "");
    }

    @PreUpdate
    protected void onPreUpdate() {
        lastModifiedDate = new Date();
    }

    @PrePersist
    protected void onPrePersist() {
        id = UUID.randomUUID().toString();
        createdDate = new Date();
    }

    public String getId() {
        return id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedByIp() {
        return createdByIp;
    }

    public void setCreatedByIp(String createdByIp) {
        this.createdByIp = createdByIp;
    }

    public int getVersion() {
        return version;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getLastModifiedByIp() {
        return lastModifiedByIp;
    }

    public void setLastModifiedByIp(String lastModifiedByIp) {
        this.lastModifiedByIp = lastModifiedByIp;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getActivatedDate() {
        return activatedDate;
    }

    public void setActivatedDate(Date activatedDate) {
        this.activatedDate = activatedDate;
    }

    public String getActivatedBy() {
        return activatedBy;
    }

    public void setActivatedBy(String activatedBy) {
        this.activatedBy = activatedBy;
    }

    public String getActivatedByIp() {
        return activatedByIp;
    }

    public void setActivatedByIp(String activatedByIp) {
        this.activatedByIp = activatedByIp;
    }

    public Date getInactivatedDate() {
        return inactivatedDate;
    }

    public void setInactivatedDate(Date inactivatedDate) {
        this.inactivatedDate = inactivatedDate;
    }

    public String getInactivatedBy() {
        return inactivatedBy;
    }

    public void setInactivatedBy(String inactivatedBy) {
        this.inactivatedBy = inactivatedBy;
    }

    public String getInactivatedByIp() {
        return inactivatedByIp;
    }

    public void setInactivatedByIp(String inactivatedByIp) {
        this.inactivatedByIp = inactivatedByIp;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Date getArchivedDate() {
        return archivedDate;
    }

    public void setArchivedDate(Date archivedDate) {
        this.archivedDate = archivedDate;
    }

    public String getArchivedBy() {
        return archivedBy;
    }

    public void setArchivedBy(String archivedBy) {
        this.archivedBy = archivedBy;
    }

    public String getArchivedByIp() {
        return archivedByIp;
    }

    public void setArchivedByIp(String archivedByIp) {
        this.archivedByIp = archivedByIp;
    }

    public Date getUnarchivedDate() {
        return unarchivedDate;
    }

    public void setUnarchivedDate(Date unarchivedDate) {
        this.unarchivedDate = unarchivedDate;
    }

    public String getUnarchivedBy() {
        return unarchivedBy;
    }

    public void setUnarchivedBy(String unarchivedBy) {
        this.unarchivedBy = unarchivedBy;
    }

    public String getUnarchivedByIp() {
        return unarchivedByIp;
    }

    public void setUnarchivedByIp(String unarchivedByIp) {
        this.unarchivedByIp = unarchivedByIp;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public String getDeletedByIp() {
        return deletedByIp;
    }

    public void setDeletedByIp(String deletedByIp) {
        this.deletedByIp = deletedByIp;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Map getStatus() {
        return status;
    }

    @PostLoad
    public void postLoad() {

        // Set Status
        if (isDeleted()) {
            this.status.put("message", Status.DELETED.getStatusMessage());
            this.status.put("color", Status.DELETED.getStatusColorCode());

        } else if (isArchived()) {
            this.status.put("message", Status.ARCHIVED.getStatusMessage());
            this.status.put("color", Status.ARCHIVED.getStatusColorCode());
        } else if (isActive()) {
            this.status.put("message", Status.ACTIVE.getStatusMessage());
            this.status.put("color", Status.ACTIVE.getStatusColorCode());

        } else if (!isActive()) {
            this.status.put("message", Status.IN_ACTIVE.getStatusMessage());
            this.status.put("color", Status.IN_ACTIVE.getStatusColorCode());

        } else {
            this.status.put("message", Status.UNKNOWN.getStatusMessage());
            this.status.put("color", Status.UNKNOWN.getStatusColorCode());

        }

    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "AbstractEntity{" +
                "id='" + id + '\'' +
                ", createdDate=" + createdDate +
                ", createdBy='" + createdBy + '\'' +
                ", createdByIp='" + createdByIp + '\'' +
                ", version=" + version +
                ", lastModifiedDate=" + lastModifiedDate +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", lastModifiedByIp='" + lastModifiedByIp + '\'' +
                ", active=" + active +
                ", activatedDate=" + activatedDate +
                ", activatedBy='" + activatedBy + '\'' +
                ", activatedByIp='" + activatedByIp + '\'' +
                ", inactivatedDate=" + inactivatedDate +
                ", inactivatedBy='" + inactivatedBy + '\'' +
                ", inactivatedByIp='" + inactivatedByIp + '\'' +
                ", deleted=" + deleted +
                ", deletedDate=" + deletedDate +
                ", deletedBy='" + deletedBy + '\'' +
                ", deletedByIp='" + deletedByIp + '\'' +
                ", organization=" + organization +
                ", status=" + status +
                '}';
    }


    /**
     * @author Kingsley Ezenwere
     * @since 0.0.1
     */
    public enum Status {
        ACTIVE("ACTIVE", "success"),
        IN_ACTIVE("IN-ACTIVE", "danger"),
        LOCKED("LOCKED", "danger"),
        DELETED("DELETED", "danger"),
        UNKNOWN("STATUS UNKNOWN", "danger"),
        ARCHIVED("ARCHIVED", "danger"),
        SENT("SENT", "success"),
        DRAFTED("DRAFTED", "success"),
        SCHEDULED("SCHEDULED", "success");

        private String statusMessage;
        private String statusColorCode;

        Status(String statusMessage, String statusColorCode) {
            this.statusMessage = statusMessage;
            this.statusColorCode = statusColorCode;
        }

        public String getStatusMessage() {
            return statusMessage;
        }

        public String getStatusColorCode() {
            return statusColorCode;
        }

        @Override
        public String toString() {
            return "Status{" +
                    "statusMessage='" + statusMessage + '\'' +
                    ", statusColorCode='" + statusColorCode + '\'' +
                    '}';
        }
    }
}
