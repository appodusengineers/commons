package com.appodus.commons.domain.profile.school;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class SchoolService extends AbstractService<School, SchoolDao> {
    private final SchoolDao schoolDao;

    @Autowired
    public SchoolService(SchoolDao schoolDao) {
        this.schoolDao = schoolDao;
    }

    public School update(String schoolId, School school) {

        School _school = getById(schoolId);
        _school.setLevel(school.getLevel());
        _school.setName(school.getName());
        _school.setAttendedDate(school.getAttendedDate());

        return super.update(_school);
    }
}
