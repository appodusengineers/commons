package com.appodus.commons.domain.profile.school.educationlevel;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/educationLevels")
public class EducationLevelController {
    private final EducationLevelService educationLevelService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public EducationLevelController(
            EducationLevelService educationLevelService,
            ApplicationEventPublisher eventPublisher) {
        this.educationLevelService = educationLevelService;
        this.eventPublisher = eventPublisher;
    }
    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody EducationLevel educationLevel, final HttpServletRequest request) {
        System.out.println("educationLevel: " + educationLevel);
        EducationLevel _educationLevel = educationLevelService.create(educationLevel);

//        eventPublisher.publishEvent(new OnEducationLevelModifiedEvent(new EventObj("CREATE"), this.getClass()));

        sResponse = new ServerResponse(_educationLevel);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }
    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<EducationLevel> educationLevelList = educationLevelService.getAll();

        sResponse = new ServerResponse(educationLevelList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("{educationLevelId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("educationLevelId") String educationLevelId, final HttpServletRequest request) {
        EducationLevel educationLevel = educationLevelService.getById(educationLevelId);

        sResponse = new ServerResponse(educationLevel);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<EducationLevel> educationLevelList = educationLevelService.getAllActive();

        sResponse = new ServerResponse(educationLevelList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<EducationLevel> educationLevelList = educationLevelService.getAllInactive();

        sResponse = new ServerResponse(educationLevelList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-organization")
    public ResponseEntity<ServerResponse> getAllByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<EducationLevel> educationLevelList = educationLevelService.getAllByOrganization(organization);

        sResponse = new ServerResponse(educationLevelList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("active/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<EducationLevel> educationLevelList = educationLevelService.getAllActiveByOrganization(organization);

        sResponse = new ServerResponse(educationLevelList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("inactive/by-organization")
    public ResponseEntity<ServerResponse> getAllInactiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<EducationLevel> educationLevelList = educationLevelService.getAllInactiveByOrganization(organization);

        sResponse = new ServerResponse(educationLevelList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<EducationLevel> educationLevelList = educationLevelService.getAllDeletedByOrganization(organization);

        sResponse = new ServerResponse(educationLevelList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("non-deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllNonDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<EducationLevel> educationLevelList = educationLevelService.getAllNonDeletedByOrganization(organization);

        sResponse = new ServerResponse(educationLevelList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{educationLevelId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("educationLevelId") String educationLevelId,
            @RequestBody EducationLevel educationLevel,
            final HttpServletRequest request) {

        EducationLevel updatedEducationLevel = educationLevelService.update(educationLevelId, educationLevel);

//        eventPublisher.publishEvent(new OnEducationLevelModifiedEvent(new EventObj("UPDATE"), this.getClass()));

        sResponse = new ServerResponse(updatedEducationLevel);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{educationLevelId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("educationLevelId") String educationLevelId,
            final HttpServletRequest request) {

        EducationLevel updatedEducationLevel = educationLevelService.activate(educationLevelId);

//        eventPublisher.publishEvent(new OnEducationLevelModifiedEvent(new EventObj("ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedEducationLevel);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{educationLevelId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("educationLevelId") String educationLevelId,
            final HttpServletRequest request) {

        EducationLevel updatedEducationLevel = educationLevelService.inactivate(educationLevelId);

//        eventPublisher.publishEvent(new OnEducationLevelModifiedEvent(new EventObj("IN_ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedEducationLevel);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{educationLevelId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("educationLevelId") String educationLevelId,
            final HttpServletRequest request) {

        EducationLevel updatedEducationLevel = educationLevelService.delete(educationLevelId);

//        eventPublisher.publishEvent(new OnEducationLevelModifiedEvent(new EventObj("DELETE"), this.getClass()));

        sResponse = new ServerResponse(updatedEducationLevel);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{educationLevelId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("educationLevelId") String educationLevelId,
            final HttpServletRequest request) {

        educationLevelService.hardDelete(educationLevelId);

//        eventPublisher.publishEvent(new OnEducationLevelModifiedEvent(new EventObj("HARD_DELETE"), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
