package com.appodus.commons.domain.profile;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.profile.careerindustry.CareerIndustryService;
import com.appodus.commons.domain.profile.school.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class ProfileService extends AbstractService<Profile, ProfileDao> {

    private final ProfileDao profileDao;
    private final SchoolService schoolService;
    private final CareerIndustryService careerIndustryService;

    @Autowired
    public ProfileService(ProfileDao profileDao, SchoolService schoolService, CareerIndustryService careerIndustryService) {
        this.profileDao = profileDao;
        this.schoolService = schoolService;
        this.careerIndustryService = careerIndustryService;
    }

    public Profile create(Profile profile) {


        if (profile.getSchoolList() != null) {
            if (StringUtils.hasText(profile.getSchoolList().get(0).getName())) {
                profile.setSchoolList(schoolService.createAll(profile.getSchoolList()));
            }else {
                profile.setSchoolList(null);
            }
        }

        return super.create(profile);
    }
}
