package com.appodus.commons.domain.profile;


import com.appodus.commons.domain.abstractservice.AbstractDao;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface ProfileDao extends AbstractDao<Profile> {
}
