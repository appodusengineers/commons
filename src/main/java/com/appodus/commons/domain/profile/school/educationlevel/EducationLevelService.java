package com.appodus.commons.domain.profile.school.educationlevel;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class EducationLevelService extends AbstractService<EducationLevel, EducationLevelDao> {
    private final EducationLevelDao educationLevelDao;
    private final ActiveAuditor activeAuditor;

    @Autowired
    public EducationLevelService(EducationLevelDao educationLevelDao, ActiveAuditor activeAuditor) {
        this.educationLevelDao = educationLevelDao;
        this.activeAuditor = activeAuditor;
    }

    public EducationLevel update(String educationLevelId, EducationLevel educationLevel) {

        EducationLevel _educationLevel = getById(educationLevelId);
        _educationLevel.setName(educationLevel.getName());
        _educationLevel.setDescription(educationLevel.getDescription());

        return super.update(_educationLevel);
    }

    //    public EducationLevel getByName(String name) {
//        return educationLevelDao.findByName(name);
//    }
    public EducationLevel getByNameAndOrganization(String name, Organization organization) {
        return educationLevelDao.findByNameAndOrganization(name, organization);
    }

    /**
     * Returns List of All EducationLevels in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<EducationLevel> getAllByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return educationLevelDao.findAllByOrganization(_organization);
    }


    /**
     * Returns List of All Active EducationLevels in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<EducationLevel> getAllActiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return educationLevelDao.findAllByOrganizationAndActive(_organization, true);
    }


    /**
     * Returns List of All Inactive EducationLevels in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<EducationLevel> getAllInactiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return educationLevelDao.findAllByOrganizationAndActive(_organization, false);
    }


    /**
     * Returns List of All Deleted EducationLevels in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<EducationLevel> getAllDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return educationLevelDao.findAllByOrganizationAndDeleted(_organization, true);
    }


    /**
     * Returns List of All Non Deleted EducationLevels in an Organization
     *
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<EducationLevel> getAllNonDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))) {
            _organization = activeAuditor.getOrganization();
        }

        return educationLevelDao.findAllByOrganizationAndDeleted(_organization, false);
    }

    public void initOrganizationEducationLevels(Organization organization) {
        if (getAllByOrganization(organization).size() < 1) {
            super.createAll(Arrays.asList(
                    new EducationLevel("Creche", "Day care", 1, organization),
                    new EducationLevel("Preschool", "Nursery", 2, organization),
                    new EducationLevel("Primary", "Primary school", 3, organization),
                    new EducationLevel("Secondary", "Secondary school", 4, organization),
                    new EducationLevel("OND", "Ordinary National Diploma", 5, organization),
                    new EducationLevel("HND", "Higher National Diploma", 6, organization),
                    new EducationLevel("Bachelor degree", "Bachelor degree", 7, organization),
                    new EducationLevel("Master degree", "Master degree", 8, organization),
                    new EducationLevel("Doctorate degree", "Doctorate degree", 9, organization)
            ));
        }
    }

}
