package com.appodus.commons.domain.profile.careerindustry;



import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.organization.Organization;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(name = "careerindustry")
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"name", "organization_id"})
})
public class CareerIndustry extends AbstractEntity {
    @Column(nullable = false)
    private String name;
    private String description;

    public CareerIndustry() {
        super("", "", "");
    }

    public CareerIndustry(String name, String description, Organization organization) {
        this();

        this.name = name;
        this.description = description;
        this.organization = organization;
    }
}
