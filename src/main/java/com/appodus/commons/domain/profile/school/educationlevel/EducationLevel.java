package com.appodus.commons.domain.profile.school.educationlevel;


import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.organization.Organization;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.*;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(name = "educationlevel")
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"name", "organization_id"})
})
public class EducationLevel extends AbstractEntity {
    @Column(nullable = false)
    private String name;
    private String description;
    private int grade;

    public EducationLevel() {
        super("", "", "");
    }

    public EducationLevel(String name, String description, int grade, Organization organization) {
        this();

        this.name = name;
        this.description = description;
        this.grade = grade;
        this.organization = organization;
    }
}
