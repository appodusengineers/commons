package com.appodus.commons.domain.profile.careerindustry;

import com.appodus.commons.Util.Utils;
import com.appodus.commons.domain.ServerResponse;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("v1/careerIndustries")
public class CareerIndustryController {
    private final CareerIndustryService careerIndustryService;
    private final ApplicationEventPublisher eventPublisher;
    private ServerResponse sResponse;

    @Autowired
    public CareerIndustryController(
            CareerIndustryService careerIndustryService,
            ApplicationEventPublisher eventPublisher) {
        this.careerIndustryService = careerIndustryService;
        this.eventPublisher = eventPublisher;
    }
    ///////////// CREATE ///////////////////////////////////////////////////////////////////////////////////
    @PostMapping()
    public ResponseEntity<ServerResponse> create(@RequestBody CareerIndustry careerIndustry, final HttpServletRequest request) {
        System.out.println("careerIndustry: " + careerIndustry);
        CareerIndustry _careerIndustry = careerIndustryService.create(careerIndustry);

//        eventPublisher.publishEvent(new OnCareerIndustryModifiedEvent(new EventObj("CREATE"), this.getClass()));

        sResponse = new ServerResponse(_careerIndustry);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.CREATED);
    }
    ///////////// READ   ///////////////////////////////////////////////////////////////////////////////////
    @GetMapping()
    public ResponseEntity<ServerResponse> getAll(final HttpServletRequest request) {
        List<CareerIndustry> careerIndustryList = careerIndustryService.getAll();

        sResponse = new ServerResponse(careerIndustryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("{careerIndustryId}")
    public ResponseEntity<ServerResponse> getById(@PathVariable("careerIndustryId") String careerIndustryId, final HttpServletRequest request) {
        CareerIndustry careerIndustry = careerIndustryService.getById(careerIndustryId);

        sResponse = new ServerResponse(careerIndustry);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("active")
    public ResponseEntity<ServerResponse> getAllActive(final HttpServletRequest request) {
        List<CareerIndustry> careerIndustryList = careerIndustryService.getAllActive();

        sResponse = new ServerResponse(careerIndustryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @GetMapping("inactive")
    public ResponseEntity<ServerResponse> getAllInactive(final HttpServletRequest request) {
        List<CareerIndustry> careerIndustryList = careerIndustryService.getAllInactive();

        sResponse = new ServerResponse(careerIndustryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PostMapping("by-organization")
    public ResponseEntity<ServerResponse> getAllByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<CareerIndustry> careerIndustryList = careerIndustryService.getAllByOrganization(organization);

        sResponse = new ServerResponse(careerIndustryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }   
    @PostMapping("active/by-organization")
    public ResponseEntity<ServerResponse> getAllActiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<CareerIndustry> careerIndustryList = careerIndustryService.getAllActiveByOrganization(organization);

        sResponse = new ServerResponse(careerIndustryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("inactive/by-organization")
    public ResponseEntity<ServerResponse> getAllInactiveByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<CareerIndustry> careerIndustryList = careerIndustryService.getAllInactiveByOrganization(organization);

        sResponse = new ServerResponse(careerIndustryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<CareerIndustry> careerIndustryList = careerIndustryService.getAllDeletedByOrganization(organization);

        sResponse = new ServerResponse(careerIndustryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    @PostMapping("non-deleted/by-organization")
    public ResponseEntity<ServerResponse> getAllNonDeletedByOrganization(@RequestBody Organization organization, final HttpServletRequest request) {
        List<CareerIndustry> careerIndustryList = careerIndustryService.getAllNonDeletedByOrganization(organization);

        sResponse = new ServerResponse(careerIndustryList);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
    ///////////// UPDATE ///////////////////////////////////////////////////////////////////////////////////
    @PutMapping("{careerIndustryId}")
    public ResponseEntity<ServerResponse> update(
            @PathVariable("careerIndustryId") String careerIndustryId,
            @RequestBody CareerIndustry careerIndustry,
            final HttpServletRequest request) {

        CareerIndustry updatedCareerIndustry = careerIndustryService.update(careerIndustryId, careerIndustry);

//        eventPublisher.publishEvent(new OnCareerIndustryModifiedEvent(new EventObj("UPDATE"), this.getClass()));

        sResponse = new ServerResponse(updatedCareerIndustry);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{careerIndustryId}/activate")
    public ResponseEntity<ServerResponse> activate(
            @PathVariable("careerIndustryId") String careerIndustryId,
            final HttpServletRequest request) {

        CareerIndustry updatedCareerIndustry = careerIndustryService.activate(careerIndustryId);

//        eventPublisher.publishEvent(new OnCareerIndustryModifiedEvent(new EventObj("ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedCareerIndustry);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @PutMapping("{careerIndustryId}/inactivate")
    public ResponseEntity<ServerResponse> inactivate(
            @PathVariable("careerIndustryId") String careerIndustryId,
            final HttpServletRequest request) {

        CareerIndustry updatedCareerIndustry = careerIndustryService.inactivate(careerIndustryId);

//        eventPublisher.publishEvent(new OnCareerIndustryModifiedEvent(new EventObj("IN_ACTIVATE"), this.getClass()));

        sResponse = new ServerResponse(updatedCareerIndustry);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    ///////////// DELETE ///////////////////////////////////////////////////////////////////////////////////
    @DeleteMapping("{careerIndustryId}")
    public ResponseEntity<ServerResponse> delete(
            @PathVariable("careerIndustryId") String careerIndustryId,
            final HttpServletRequest request) {

        CareerIndustry updatedCareerIndustry = careerIndustryService.delete(careerIndustryId);

//        eventPublisher.publishEvent(new OnCareerIndustryModifiedEvent(new EventObj("DELETE"), this.getClass()));

        sResponse = new ServerResponse(updatedCareerIndustry);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }

    @DeleteMapping("{careerIndustryId}/hard")
    public ResponseEntity<ServerResponse> hardDelete(
            @PathVariable("careerIndustryId") String careerIndustryId,
            final HttpServletRequest request) {

        careerIndustryService.hardDelete(careerIndustryId);

//        eventPublisher.publishEvent(new OnCareerIndustryModifiedEvent(new EventObj("HARD_DELETE"), this.getClass()));

        sResponse = new ServerResponse(null);
        sResponse.add(new Link(Utils.getRequestUrl(request), "self"));

        return new ResponseEntity<>(sResponse, HttpStatus.OK);
    }
}
