package com.appodus.commons.domain.profile;

import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.designation.Designation;
import com.appodus.commons.domain.miscellaneous.agecategory.AgeCategory;
import com.appodus.commons.domain.miscellaneous.gender.Gender;
import com.appodus.commons.domain.miscellaneous.maritalstatus.MaritalStatus;
import com.appodus.commons.domain.miscellaneous.marriagetype.MarriageType;
import com.appodus.commons.domain.miscellaneous.membershipstatus.MembershipStatus;
import com.appodus.commons.domain.miscellaneous.months.Month;
import com.appodus.commons.domain.profile.careerindustry.CareerIndustry;
import com.appodus.commons.domain.profile.school.School;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;
import org.springframework.util.StringUtils;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Profile extends AbstractEntity
{
    @ManyToOne
    private Designation salutation;
    private String passportBase64;
    private String passportFile;
    private String firstName;
    private String lastName;
    private String otherNames;
    @Transient
    private String fullName;
    @ManyToOne
    private Gender gender;
    //    Marriage
    @ManyToOne
    private MaritalStatus maritalStatus;
    @ManyToOne
    private MarriageType marriageType;
    private Date marriageDate;
    private int childrenCount;
    //    DoB
    private int birthDay;
    @ManyToOne
    private Month birthMonth;
    private int birthYear;
    @ManyToOne
    private AgeCategory ageCategory;
    //    Education
    @OneToMany
    @JoinTable(
        name = "profile_schools",
        joinColumns = @JoinColumn(name = "profile_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "school_id", referencedColumnName = "id"))
    private List<School> schoolList;
    //    Career
    @ManyToOne
    private CareerIndustry careerIndustry;
    private String careerCompany;
    private String careerJobTitle;
    //    Church membership
    @Temporal(TemporalType.TIMESTAMP)
    private Date decisionDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date membershipDate;
    //    @ManyToMany(fetch = FetchType.LAZY)
    //    @JoinTable(
    //            name = "profile_departments",
    //            joinColumns = @JoinColumn(name = "profile_id", referencedColumnName = "id"),
    //            inverseJoinColumns = @JoinColumn(name = "department_id", referencedColumnName = "id"))
    //    private List<GroupDepartment> departmentList;
    //    @ManyToMany(fetch = FetchType.LAZY)
    //    @JoinTable(
    //            name = "profile_groups",
    //            joinColumns = @JoinColumn(name = "profile_id", referencedColumnName = "id"),
    //            inverseJoinColumns = @JoinColumn(name = "group_id", referencedColumnName = "id"))
    //    private List<Group> groupList;
    @ManyToOne
    private MembershipStatus membershipStatus;
    //    Next of Kin
    private String nokFullName;
    private String nokAddress;
    @ManyToOne
    private Designation nokRelationship;


    public Profile()
    {
        super();

        this.schoolList = Arrays.asList(new School());
    }


    @PrePersist
    public void prePersist()
    {

        /// SET TO NULL IF JUST AN OBJECT INIT
        if (this.salutation != null)
        {
            if (StringUtils.isEmpty(this.salutation.getId()))
            {
                this.salutation = null;
            }
        }

        if (this.gender != null)
        {
            if (StringUtils.isEmpty(this.gender.getId()))
            {
                this.gender = null;
            }
        }

        if (this.maritalStatus != null)
        {
            if (StringUtils.isEmpty(this.maritalStatus.getId()))
            {
                this.maritalStatus = null;
            }
        }

        if (this.marriageType != null)
        {
            if (StringUtils.isEmpty(this.marriageType.getId()))
            {
                this.marriageType = null;
            }
        }

        if (this.birthMonth != null)
        {
            if (StringUtils.isEmpty(this.birthMonth.getId()))
            {
                this.birthMonth = null;
            }
        }

        if (this.ageCategory != null)
        {
            if (StringUtils.isEmpty(this.ageCategory.getId()))
            {
                this.ageCategory = null;
            }
        }

        if (this.schoolList != null)
        {
            if (StringUtils.isEmpty(this.schoolList.get(0).getId()))
            {
                this.schoolList = null;
            }
        }

        if (this.careerIndustry != null)
        {
            if (StringUtils.isEmpty(this.careerIndustry.getId()))
            {
                this.careerIndustry = null;
            }
        }

        if (this.membershipStatus != null)
        {
            if (StringUtils.isEmpty(this.membershipStatus.getId()))
            {
                this.membershipStatus = null;
            }
        }

        if (this.nokRelationship != null)
        {
            if (StringUtils.isEmpty(this.nokRelationship.getId()))
            {
                this.nokRelationship = null;
            }
        }
    }
    @PreUpdate
    public void preUpdate()
    {
        prePersist();
    }


    @PostLoad
    public void postLoad()
    {
        if (!StringUtils.hasText(fullName))
        {// Set FullName
            fullName = (StringUtils.hasText(firstName) ? firstName + " " : "") + (StringUtils.hasText(lastName) ? lastName : "");
        }
    }
}
