package com.appodus.commons.domain.profile.careerindustry;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.miscellaneous.membershipstatus.MembershipStatus;
import com.appodus.commons.domain.organization.Organization;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface CareerIndustryDao extends AbstractDao<CareerIndustry> {
//    CareerIndustry findByName(String name);
    CareerIndustry findByNameAndOrganization(String name, Organization organization);

    List<CareerIndustry> findAllByOrganization(Organization organization);
    List<CareerIndustry> findAllByOrganizationAndActive(Organization organization, boolean isActive);
    List<CareerIndustry> findAllByOrganizationAndDeleted(Organization organization, boolean isDeleted);
}
