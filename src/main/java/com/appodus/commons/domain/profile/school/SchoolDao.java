package com.appodus.commons.domain.profile.school;


import com.appodus.commons.domain.abstractservice.AbstractDao;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface SchoolDao extends AbstractDao<School> {
}
