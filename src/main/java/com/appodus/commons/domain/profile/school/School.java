package com.appodus.commons.domain.profile.school;



import com.appodus.commons.domain.abstractentity.AbstractEntity;
import com.appodus.commons.domain.profile.school.educationlevel.EducationLevel;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.*;
import java.util.Date;
import lombok.Data;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class School extends AbstractEntity {

    @ManyToOne
    private EducationLevel level;
    private String name;
    @Temporal(TemporalType.TIMESTAMP)
    private Date attendedDate;

    public School() {
        super("", "", "");
    }
}
