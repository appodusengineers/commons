package com.appodus.commons.domain.profile.careerindustry;

import com.appodus.commons.domain.abstractservice.AbstractService;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.security.ActiveAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
@Service
public class CareerIndustryService extends AbstractService<CareerIndustry, CareerIndustryDao> {
    private final CareerIndustryDao careerIndustryDao;
    private final ActiveAuditor activeAuditor;

    @Autowired
    public CareerIndustryService(CareerIndustryDao careerIndustryDao, ActiveAuditor activeAuditor) {
        this.careerIndustryDao = careerIndustryDao;
        this.activeAuditor = activeAuditor;
    }

    public CareerIndustry update(String careerIndustryId, CareerIndustry careerIndustry) {

        CareerIndustry _careerIndustry = getById(careerIndustryId);
        _careerIndustry.setName(careerIndustry.getName());
        _careerIndustry.setDescription(careerIndustry.getDescription());

        return super.update(_careerIndustry);
    }

//    public CareerIndustry getByName(String name) {
//        return careerIndustryDao.findByName(name);
//    }

    public CareerIndustry getByNameAndOrganization(String name, Organization organization) {
        return careerIndustryDao.findByNameAndOrganization(name, organization);
    }

    /**
     * Returns List of All CareerIndustries in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<CareerIndustry> getAllByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return careerIndustryDao.findAllByOrganization(_organization);
    }


    /**
     * Returns List of All Active CareerIndustries in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<CareerIndustry> getAllActiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return careerIndustryDao.findAllByOrganizationAndActive(_organization, true);
    }


    /**
     * Returns List of All Inactive CareerIndustries in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<CareerIndustry> getAllInactiveByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return careerIndustryDao.findAllByOrganizationAndActive(_organization, false);
    }


    /**
     * Returns List of All Deleted CareerIndustries in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<CareerIndustry> getAllDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return careerIndustryDao.findAllByOrganizationAndDeleted(_organization, true);
    }


    /**
     * Returns List of All Non Deleted CareerIndustries in an Organization
     * @param organization "Given Org; can be null, on which case, Org from ActiveAuditor is used"
     * @return "List of all in the given Org"
     */
    public List<CareerIndustry> getAllNonDeletedByOrganization(Organization organization) {
        Organization _organization = organization;

        // Use from ActiveAuditor if null or non persisted was passed
        if (_organization == null || (_organization != null && StringUtils.isEmpty(_organization.getId()))){
            _organization = activeAuditor.getOrganization();
        }

        return careerIndustryDao.findAllByOrganizationAndDeleted(_organization, false);
    }

    public void initOrganizationCareerIndustries(Organization organization) {
        if (getAllByOrganization(organization).size() < 1) {
            super.createAll(Arrays.asList(
                    new CareerIndustry("IT-Software", "Never married", organization),
                    new CareerIndustry("IT-Database", "In a marriage", organization),
                    new CareerIndustry("IT-Hardware", "Married, but separated", organization),
                    new CareerIndustry("IT-Networking", "In a marriage", organization)
            ));
        }
    }
}
