package com.appodus.commons.domain.profile.school.educationlevel;


import com.appodus.commons.domain.abstractservice.AbstractDao;
import com.appodus.commons.domain.organization.Organization;
import com.appodus.commons.domain.profile.careerindustry.CareerIndustry;

import java.util.List;

/**
 * @author Kingsley Ezenwere
 * @since 0.0.1
 */
public interface EducationLevelDao extends AbstractDao<EducationLevel> {
//    EducationLevel findByName(String name);
    EducationLevel findByNameAndOrganization(String name, Organization organization);

    List<EducationLevel> findAllByOrganization(Organization organization);
    List<EducationLevel> findAllByOrganizationAndActive(Organization organization, boolean isActive);
    List<EducationLevel> findAllByOrganizationAndDeleted(Organization organization, boolean isDeleted);
//    @Query("SELECT d FROM EducationLevel d WHERE d.active = true ORDER BY d.createdDate DESC")
//    List<EducationLevel> finAllActive();
//
//    @Query("SELECT d FROM EducationLevel d WHERE d.active = false ORDER BY d.createdDate DESC")
//    List<EducationLevel> findAllInactive();
}
