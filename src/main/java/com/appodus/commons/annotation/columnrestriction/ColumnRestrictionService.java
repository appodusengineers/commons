//package com.appodus.commons.annotation.columnrestriction;
//
//import com.appodus.commons.domain.abstractentity.AbstractEntity;
//import com.appodus.commons.domain.abstractservice.AbstractService;
//import com.appodus.commons.exception.EntityConflictException;
//import org.springframework.beans.factory.config.BeanDefinition;
//import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
//import org.springframework.core.type.filter.AnnotationTypeFilter;
//import org.springframework.stereotype.Component;
//
//import javax.persistence.Entity;
//import java.lang.reflect.Field;
//import java.util.List;
//
///**
// * @author Kingsley Ezenwere
// * @since 0.0.1
// */
//@Component
//public class ColumnRestrictionService {
//    private List<AbstractEntity> entityList;
//    private AbstractService abstractService;
//
//    public void performColumnRestriction(List<AbstractEntity> entityList, AbstractService abstractService) {
//        this.entityList = entityList;
//        this.abstractService = abstractService;
//        String basePackage = entityList.get(0).getClass().getPackage().getName();
//
//        ClassPathScanningCandidateComponentProvider provider = createComponentScanner();
//        provider.findCandidateComponents(basePackage).forEach((BeanDefinition beanDef) -> {
//            saveMetadata(beanDef);
//        });
//
//    }
//
//
//    private ClassPathScanningCandidateComponentProvider createComponentScanner() {
//        // Don't pull default filters (@Component, etc.):
//        ClassPathScanningCandidateComponentProvider provider
//                = new ClassPathScanningCandidateComponentProvider(false);
//        provider.addIncludeFilter(new AnnotationTypeFilter(Entity.class));
//        return provider;
//    }
//
//    private void saveMetadata(BeanDefinition beanDef) {
//        try {
//            Class<?> cl = Class.forName(beanDef.getBeanClassName());
//            for (Field f : cl.getFields()) {
//
//                ColumnRestriction cr = f.getAnnotation(ColumnRestriction.class);
//                boolean uniqueByOrganization = false;
//
//                if (cr != null) {
//                    uniqueByOrganization = cr.uniqueByOrganization();
//                }
//
//                if (uniqueByOrganization) {
//                    entityList.forEach(entity -> {
//                        List<Object> objList = abstractService.getAllByOrganization(entity.getOrganization());
//                        objList.forEach(obj -> {
//                            for (Field _f : obj.getClass().getFields()) {
//                                try {
//                                    if (_f.getName().equals(f.getName()) && (_f.get(obj) + "").equalsIgnoreCase(f.get(obj) + "")) {
//                                        throw new EntityConflictException(String.format("The value '%s' already exists, uniqueByOrganization.", _f.get(obj)));
//                                    }
//                                } catch (IllegalAccessException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        });
//                    });
//                    continue;
//                }
//
//            }
//
//        } catch (ClassNotFoundException | SecurityException e) {
//            throw new RuntimeException(e.getMessage());
//        }
//    }
//}
