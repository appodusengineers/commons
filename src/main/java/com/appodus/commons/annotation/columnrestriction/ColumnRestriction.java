//package com.appodus.commons.annotation.columnrestriction;
//
//import java.lang.annotation.ElementType;
//import java.lang.annotation.Retention;
//import java.lang.annotation.RetentionPolicy;
//import java.lang.annotation.Target;
//
///**
// *
// * @author Kingsley Ezenwere
// * @since 0.0.1
// */
//@Target(value = {ElementType.FIELD})
//@Retention(RetentionPolicy.RUNTIME)
//public @interface ColumnRestriction {
//    boolean uniqueByOrganization() default false;
//}